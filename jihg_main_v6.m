%% MAIN v1.0
% PC: added python environment config
% Run once at beginning of session
% pyenv('Version','/usr/local/bin/python')
% pyenv('Version','/System/Library/Frameworks/Python.framework/Versions/3.7/bin/python')
% /Users/pconroy/opt/anaconda3/bin/python

clear 
clc
% dbstop if error

addpath(genpath('/Users/pconroy/phd/code/floris/JIHG_v6'))
addpath(genpath('/Users/pconroy/phd/projects/delftland'))

mfile_name          = mfilename('fullpath');
% [pathstr,name,ext]  = fileparts(mfile_name);
% cd(pathstr);
cwd = pwd;
%% user defined settings
input_folder  = '/Users/pconroy/phd/projects/delftland/input/';
overwrite_check = 0;
max_mem = 2*1e9;
run_name = 'delftland_joint_run2';

% PC NOTE: DO NOT INCLUDE DATES OUTSIDE THE RANGE OF METEO_DATA FILE
% the number of used mages can be smaller than the stack.
start_date = '20170103';
end_date   = '20191231';
exclude_dates   = {''}; % slcs that give problems can be excluded


ref_period = {'20170101','20200101'}; % used to reference all tsrs to same time (window) %what is this?

%% Joint Estimation of Stacks 
joint = 1;
joint_output_folder = ['/Users/pconroy/phd/projects/delftland/output/' run_name '/'];

%% stack parameters
% path: can do multiple stacks, separate with comma
stack_path        = {'/Users/pconroy/phd/projects/delftland/stitched_ifgs_asc_t088', ...
                     '/Users/pconroy/phd/projects/delftland/stitched_ifgs_asc_t161', ...
                     '/Users/pconroy/phd/projects/delftland/stitched_ifgs_dsc_t110'};
stack_id          = 'delft';
master_date       = {'20190106', '20170103', '20170106'};
output_folder     = {[joint_output_folder 'output_asc_t088/'], ...
                     [joint_output_folder 'output_asc_t161/'], ...
                     [joint_output_folder 'output_dsc_t110/']};
lambda            = 0.0556;

%% identifying pixels

% id PS pixels
id_ps_flag        = 1;
D_thres           = 0.2; % Threshold for amplitude dispersion

% id pixels in polygons
poly_flag         = 1;
shape_path        = '/Users/pconroy/phd/projects/delftland/shapefiles/test_selection.shp';

% include pixels that are not PS and outside polygons
inc_rem           = 1; %what is this? I thought the whole point is to process DS

%% multilooking
standard          = 0; % not implemented (this would be with line pixel coordinates. Joining stacks would then get complicated).
convert2rd        = 'no'; %convert latlon to rd

% what is this?
geocoords         = 1;
polycoords        = 0; % Not default
win_size          = 100; %[m]

%corners of area of interest in latitude longitude
ll                = [51.94,4.3]; % lower-left
ur                = [52.02,4.4]; % upper-right
overlapping       = 0; % not implemented (windows would overlap)

% select statistically homogenous pixels
shp_flag          = 1;
mean_test         = 1;
alpha             = 0.05; % level of significance used in test.
std_output        = 0;

%% PS input
ps_flag           = 0; % 1 if you PS results should be used
ps_software       = 'depsi';
depsi_atmosphere  = 1; % subtract atmosphere
depsi_path        = {'/Users/pconroy/phd/projects/delft_asc_t088_philip/depsi'};
depsi_project_id  = {'delft_asc_t088'};

%% phase-linking

pl_method         = 'EMI'; % options: EVD,PTA,EMI;
pl_ESM_EDC        = 'EDC'; % Equivalent Single-Master or Equivalent Daisy-Chain
CRB               = 'var'; % 'var','full', calculate Cramer-Rao Bound Covariance matrix. Full creates the full covariance matrix (nslcs x nslcs) per window.

%% unwrapping
unwrap_method     = 'MCF';
ref_ll            = [52.00,4.365]; % Delft
ref_rad           = 500; % radius around ref [m];
rain_unw          = 0; % Currently only works in combination with CRFS RCR and experimental
snaphu_path       = '/Users/pconroy/phd/snaphu-v2.0.4/bin/'; %PC: added

%% RCR
RCR               = 1;
model             = {'CRFS'}; % only use CRFS, extensometer and wl are not easily used yet.
CRFS_v2           = 0; % increases contribution of rain.

% CRFS data
fname_wd          = 'etmgeg_344.txt'; % file containing daily weather data from a KNMI station, should be placed in Input folder;

% waterlevel data
fname_wl          = {'data_peilbuizen.csv','data_peilbuizen_menyanthes.csv'};
fname_wlxy        = {'locatie_peilbuizen.csv','locatie_peilbuizen_menyanthes.csv'};

% extensometer data
extensometer      = 0;
fname_ext         = 'WDOD-05B-ref.xlsm';
rcr_types         = 'poly'; % or all windows, poly == 3;

%% Simulate subsidence based on coherence matrix calculated during jihg_cpxcoh
sim               = 0;
sim_joint         = 0;

sim_ps            = 0;
sim_signal        = 0;
sim_max_shrink    = 10;
sim_sec_sub       = 0;
sim_model         = 'extensometer'; % or extensometer or groundwaterlevel;
fit_sim_model     = 'no';

sim_polygon       = 0;
sim_poly_var      = 0.1; % fraction of max shrinkage
sim_poly_decorr_t = 100; % days

sim_temporal_var  = 0;

sim_noise         = 0;


%% run

main_dir = cwd;
if joint == 1
    save([joint_output_folder 'jihg_J']);
    jihg_steps_joint(joint_output_folder)
else
    
    save('jihg_S');
    jihg_steps_single
end



