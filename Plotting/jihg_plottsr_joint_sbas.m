function jihg_plottsr_joint_sbas(~,~,TSR,COH,VAR,T_ext)

    [x_get,y_get,~]=ginput(1);
    
    win_lon = VAR.win_lon;
    win_lat = VAR.win_lat;
    win_p   = VAR.win_p;
    win_l   = VAR.win_l;
    win_type = VAR.win_type;
    
    nlines = max(win_l);
    npixels = max(win_p);
    min_lon = min(win_lon);
    max_lon = max(win_lon);
    min_lat = min(win_lat);
    max_lat = max(win_lat);
    
    ix = find(diff(win_lon)>0,1);
    dlon = (win_lon(ix+1)-win_lon(1));
    dlat = (win_lat(2)-win_lat(1));
    
    lon_range = min_lon:dlon:max_lon+dlon/2;
    lat_range = min_lat:dlat:max_lat+dlat/2;   
    
%     ix_wrt_data = zeros(size(VAR.chk));
%     ix_wrt_data(VAR.chk==1) = 1:size(TSR,2);
    
    ix1 = find(lat_range < y_get + dlat/2,1,'last');
    ix2 = find(lon_range < x_get + dlon/2,1,'last');
    ix_wrt_grid = sub2ind([nlines,npixels],ix1,ix2);
    ix = ix_wrt_grid;
%     if VAR.chk(ix_wrt_grid) ==1
% 
%         ix = ix_wrt_data(ix_wrt_grid);
%     else
%         return
%     end
    

    nf = max(TSR.stack);
    date_v = COH.Time;
    tsr_crb = COH.(['win',num2str(ix_wrt_grid)]);
%     est_p = EST.(['win',num2str(ix_wrt_grid)]);
%     est_a = POLY.(['win',num2str(ix_wrt_grid)]);
    
    h = figure;
    set(gcf,'position', [158*2 164*2 1200/2 600])
    subplot(3,1,1:2)   
    plot_configs = {'ok','xk','+k','sk'};
    plot_configs1 = {'r','g','k','b'};
    legend_config = {'stack1','stack2','stack3','stack4'};
    for i = 1:nf
        sel = TSR.stack == i;
        date_p = TSR.Time(sel);
        tsr_p = TSR.(['win',num2str(ix_wrt_grid)])(sel);  
        plot(date_p,tsr_p,plot_configs{i})
        hold on
    end
    hold off
    title({[num2str(ix1) ',' num2str(ix2)],['window ',num2str(ix_wrt_grid),' type ',num2str(win_type(ix))]})
    grid on;
    grid minor;
    ylabel('[mm]');
    subplot(3,1,3)   
    for i = 1:nf
        sel = TSR.stack == i;
        date_p = COH.Time(sel);
        coh_p = COH.(['win',num2str(ix_wrt_grid)])(sel);  
        h1=plot(date_p,coh_p,'-o');
        set(h1,'MarkerFaceColor',get(h1,'color'),'MarkerSize',5);
        hold on
    end
    grid on
    yyaxis right
    time_range = timerange(date_p(1),date_p(end));
    T_plot = T_ext(time_range,:);
    plot(T_plot.t,T_plot.data,'r','LineWidth',2);
    title('Avg Coherence')
    xlim([min(date_p) max(date_p)])
    legend(legend_config{1:nf},'extensometer')

    
    number = h.Number;
    txts = findobj('Type','Text');
    scat = findobj('Type','Scat');    
    if ~isempty(txts)
        strings = [txts.String];
        chk = strcmp(strings,num2str(number)); 
        delete(txts(chk));
        delete(scat(chk));
    end
    
    figure(100)
    scatter(x_get,y_get,50,'Filled','k','s')
    text(x_get,y_get,num2cell(number),'FontSize',12,'FontWeight','bold');
    figure(number);
    
    
    

end