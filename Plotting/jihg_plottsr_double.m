function jihg_plottsr_double(~,~,S,results,rcr,win)

    [x_get,y_get,~]=ginput(1);
    
    nlines = S.win.nlines;
    npixels = S.win.npixels;
    dates_num = S.stackinfo.dates_num;
    ifg_ix = S.stackinfo.ifg_ix;
    dlat = win.dlat;
    dlon = win.dlon;
    lat_range = win.lat_range;
    lon_range = win.lon_range;    
    tsr_unw = results.tsr_unw;
    tsr_est = results.tsr_est;
    tsr_unw_rcr = rcr.tsr_unw_rcr;
    tsr_est_rcr = rcr.tsr_est;
    
    
    ix1 = find(lat_range < y_get + dlat/2,1,'last');
    ix2 = find(lon_range < x_get + dlon/2,1,'last');
    ix = sub2ind([nlines,npixels],ix1,ix2);
    
    date_p = datetime(datestr(dates_num(ifg_ix(:,2))));
    tsr_p = -tsr_unw(ix,:)/2/pi;
    est_p = -tsr_est(ix,:)/2/pi;
    

    h = figure;
    set(gcf,'position', [158*2 164*2 1200 600/2]) 
    subplot(1,2,1)

    plot(date_p,tsr_p,'ok');
    hold on
    plot(date_p,est_p,'r','LineWidth',2);
    hold off
    title({[num2str(ix1) ',' num2str(ix2)],num2str(ix)})
    grid on;
    grid minor;
    
    tsr_p = -tsr_unw_rcr(ix,:)/2/pi;
    est_p = -tsr_est_rcr(ix,:)/2/pi;
    subplot(1,2,2);
    plot(date_p,tsr_p,'ok');
    hold on
    plot(date_p,est_p,'r','LineWidth',2);
    hold off
    title({[num2str(ix1) ',' num2str(ix2)],num2str(ix)})
    grid on;
    grid minor;
    
    number = h.Number;
    txts = findobj('Type','Text');
    scat = findobj('Type','Scat');    
    if ~isempty(txts)
        strings = [txts.String];
        chk = strcmp(strings,num2str(number)); 
        delete(txts(chk));
        delete(scat(chk));
    end
    
    figure(100)
    scatter(x_get,y_get,50,'Filled','k','s')
    text(x_get,y_get,num2cell(number),'FontSize',12,'FontWeight','bold');
    figure(number);
    

end