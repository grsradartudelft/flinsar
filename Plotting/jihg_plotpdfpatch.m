function jihg_plotpdfpatch(src,~,h,date_p,coh_p,est_p,nshp)

    
    ylim1 = ylim(gca);
    

    j_ind = floor(ylim1(1)-1:1:ylim1(2)+1);

    N = 100;
    dwn_samp = round(linspace(1,629,N));  
    diff_date = diff(date_p);
    diff_date = [6;diff_date;6];
    pdf_atmo = normpdf([-pi:0.01:pi],0,sqrt(2)*(5/56*4*pi));
    for i = 1:length(date_p)
        [std,pdf] = coh2std(coh_p(i),'ds',nshp); 
%         pdf = normpdf([-pi:0.01:pi],0,deg2rad(std)+sqrt(2)*(5/56*4*pi));
        pdf = normpdf([-pi:0.01:pi],0,deg2rad(std));
%         pdf = pdf.*pdf_atmo;
        pdf = pdf(dwn_samp);
        
        
        
        y1 = ones(N,1)*(date_p(i)-diff_date(i)/2);
        y2 = ones(N,1)*(date_p(i)+diff_date(i+1)/2);
        for j = j_ind
            x = linspace(date_p(i)-3,date_p(i)+3,629)';
%             y = ones(629,1)*tsr_p(i);

            x = linspace(-0.5,0.5,N)' + mod(est_p(i)+0.5,1) - 0.5 +j;

            verts = [y1, x(:);y2,x(:)];
%             N = length(x);

%             pdf = normpdf([-pi:0.01:pi],0,deg2rad(std));
            % Define the faces to connect each adjacent f(x) and the corresponding points at y = 0.
            q = (1:N-1)';
            faces = [q, q+1, q+N+1, q+N];
    %         figure;
            patch('Faces', faces, 'Vertices', verts,'FaceVertexCData',...
                [pdf(:); pdf(:)], 'FaceColor', 'interp', 'EdgeColor', 'none','FaceAlpha',0.5);
        end
    end
   
%    set(p,'Alpha','none')
%    set(p,'FaceAlpha',0.5)
%    set(p,'EdgeAlpha',0.5)
%     chH = get(gca,'Children');
%     set(gca,'Children',[chH(end-1:end);chH(1:end-2)]);
end