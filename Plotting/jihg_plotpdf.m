function jihg_plotpdf(src,~,h,date_p,coh_p,est_p,nshp)

    ylim1 = ylim(gca);  
    j_ind = floor(ylim1(1)):1:ceil(ylim1(2));
    dphi=0.001; 
    phi=[j_ind(1):dphi:j_ind(end)-dphi]';
    for i = 2:length(date_p)
         [std,pdf] = coh2std(coh_p(i),'ds',nshp); 
         if std == 0;
             std = 0.0001;
         end
         std = deg2rad(std)/(2*pi);
         phi_prob = zeros(size(phi)); 
        for j = j_ind
            mu = est_p(i) + j;
            a = 1/(std.*sqrt(2*pi));
            b = ((phi-mu)/std).^2;
            phi_prob = phi_prob + a.*exp(-0.5.*b);
        end
        
%         phi_prob=phi_prob-min(phi_prob);
%         phi_prob = phi_prob./max(phi_prob);
        pp(:,i) = phi_prob;
    end
    
    figure; imagesc(date_p,phi,pp)
    set(gca,'ydir','normal')
end
