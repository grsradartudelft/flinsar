function jihg_plottsr_joint_errorbar(~,~,TSR,CRB,EST,VAR,UNW_E,STD)

    [x_get,y_get,~]=ginput(1);
    
    win_lon = VAR.win_lon;
    win_lat = VAR.win_lat;
    win_p   = VAR.win_p;
    win_l   = VAR.win_l;
    
    nlines = max(win_l);
    npixels = max(win_p);
    min_lon = min(win_lon);
    max_lon = max(win_lon);
    min_lat = min(win_lat);
    max_lat = max(win_lat);
    
    ix = find(diff(win_lon)>0,1);
    dlon = (win_lon(ix+1)-win_lon(1));
    dlat = (win_lat(2)-win_lat(1));
    
    lon_range = min_lon:dlon:max_lon+dlon/2;
    lat_range = min_lat:dlat:max_lat+dlat/2;   
    
%     ix_wrt_data = zeros(size(VAR.chk));
%     ix_wrt_data(VAR.chk==1) = 1:size(TSR,2);
    
    ix1 = find(lat_range < y_get + dlat/2,1,'last');
    ix2 = find(lon_range < x_get + dlon/2,1,'last');
    ix_wrt_grid = sub2ind([nlines,npixels],ix1,ix2);
    ix = ix_wrt_grid;
%     if VAR.chk(ix_wrt_grid) ==1
% 
%         ix = ix_wrt_data(ix_wrt_grid);
%     else
%         return
%     end
    
    date_p = datenum(TSR.Time);
    tsr_p = TSR.(['win',num2str(ix_wrt_grid)]);
    std_p = STD.(['win',num2str(ix_wrt_grid)]);
    date_v = CRB.Time;
    crb_p = CRB.(['win',num2str(ix_wrt_grid)]);
    date_est = datenum(EST.Time);
    est_p = EST.(['win',num2str(ix_wrt_grid)]);
%     unw_date = UNW_E.Time;
    unw_e_p = UNW_E.(['win',num2str(ix_wrt_grid)]);
    los2vert = VAR.los2vert(ix);
    amb = los2vert*0.0556*1000/2;
    
    h = figure;
    set(gcf,'position', [158*2 164*2 1200/2 600])
    subplot(3,1,1:2)    
    h1=errorbar(date_p,tsr_p,std_p,'.k','MarkerSize',15);
    hold on
    h2=plot(date_p,tsr_p + amb,'.k');
    h3=plot(date_p,tsr_p - amb,'.k');
    h4=plot(date_est,est_p,'r','LineWidth',2);
    
    title({[num2str(ix1) ',' num2str(ix2)],['window ',num2str(ix_wrt_grid)]})
    grid on;
    grid minor;
    ylabel('[mm]');
%     subplot(3,1,3)   
%     plot(date_v,tsr_var/max(abs(tsr_var)),'.k')
%     grid on;
%     title('Cramer-Rao Bound (normalized) [-]')
%     ax1 = axes;

    grid_date = repmat(date_est,1,4) + repmat([-3,-3,3,3],length(date_est),1);
    Ys = ylim;
	grid_y = repmat([Ys(1) Ys(2) Ys(2) Ys(1)],length(date_est),1);
%     hall=[];
    for i = 1:length(date_est)
        ht = fill(grid_date(i,:),grid_y(i,:),[1 1 1]-unw_e_p(i),'FaceAlpha',1,'EdgeColor', 'none');
%         hall= [hall;ht];
    end
    h1 = get(gca,'Children');
    set(gca,'Children',flipud(h1))
    xlim([min(date_est) max(date_est)]) 
    set(gca, 'Layer', 'top')
    hold off
    
    datetick
    dcm_obj = datacursormode(gcf);
    set(dcm_obj, 'UpdateFcn',@myfunction);
    
%     uistack(hall,'bottom');
%     set(gca, 'Layer', 'top')

    
    
    subplot(3,1,3)
    plot(date_p,std_p);
    grid on;
    hold on
    plot(date_p,crb_p);
%     [date_v_sort,sort_ix] = sort(datenum(date_v));
%     plot(date_v_sort,tsr_var(sort_ix),'-k')
%     grid on;
%     hold on;    
%     yyaxis right
%     time_range = timerange(EST.Time(1),EST.Time(end));
%     T_plot = T_ext(time_range,:);
%     plot(datenum(T_plot.t),T_plot.data,'r','LineWidth',2);
%     title('Avg Coherence')
%     xlim([datenum(min(EST.Time)) datenum(max(EST.Time))])
%     legend('avg coherence','extensometer')
%     datetick
    
    
    number = h.Number;
    txts = findobj('Type','Text');
    scat = findobj('Type','Scat');    
    if ~isempty(txts)
        strings = [txts.String];
        chk = strcmp(strings,num2str(number)); 
        delete(txts(chk));
        delete(scat(chk));
    end
    
    figure(100)
    scatter(x_get,y_get,50,'Filled','k','s')
    text(x_get,y_get,num2cell(number),'FontSize',12,'FontWeight','bold');
    figure(number);
    
    function output_txt = myfunction(obj,event_obj)
        % Display the position of the data cursor
        % obj          Currently not used (empty)
        % event_obj    Handle to event object
        % output_txt   Data cursor text string (string or cell array of strings).
        pos = get(event_obj,'Position');
        data_ix = get(event_obj, 'DataIndex');
        output_txt = {['X: ', datestr(pos(1))],...                            % Notice the X coordinate value has been changed to DATESTR
            ['Y: ',num2str(pos(2),4)]};
        % If there is a Z-coordinate in the position, display it as well
        
        if length(pos) > 2
            output_txt{end+1} = ['Z: ',num2str(pos(3),4)];
        end
        
        fig_type = get(event_obj.Target,'Type');
        if strcmp(fig_type,'errorbar')
            output_txt{end+1} = ['std: ',num2str(abs(event_obj.Target.YNegativeDelta(data_ix)),4)];
        end
        
    end

    

end