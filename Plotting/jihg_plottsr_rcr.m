function jihg_plottsr_rcr(~,~,TSR,CRB,EST,VAR,POLY)

    [x_get,y_get,~]=ginput(1);
    
    win_lon = VAR.win_lon;
    win_lat = VAR.win_lat;
    win_p   = VAR.win_p;
    win_l   = VAR.win_l;
    win_type = VAR.win_type;
    
    nlines = max(win_l);
    npixels = max(win_p);
    min_lon = min(win_lon);
    max_lon = max(win_lon);
    min_lat = min(win_lat);
    max_lat = max(win_lat);
    
    ix = find(diff(win_lon)>0,1);
    dlon = (win_lon(ix+1)-win_lon(1));
    dlat = (win_lat(2)-win_lat(1));
    
    lon_range = min_lon:dlon:max_lon+dlon/2;
    lat_range = min_lat:dlat:max_lat+dlat/2;   
    
%     ix_wrt_data = zeros(size(VAR.chk));
%     ix_wrt_data(VAR.chk==1) = 1:size(TSR,2);
    
    ix1 = find(lat_range < y_get + dlat/2,1,'last');
    ix2 = find(lon_range < x_get + dlon/2,1,'last');
    ix_wrt_grid = sub2ind([nlines,npixels],ix1,ix2);
    ix = ix_wrt_grid;
%     if VAR.chk(ix_wrt_grid) ==1
% 
%         ix = ix_wrt_data(ix_wrt_grid);
%     else
%         return
%     end
    

    nf = max(TSR.stack);
    date_v = CRB.Time;
    tsr_crb = CRB.(['win',num2str(ix_wrt_grid)]);
    date_est = EST.Time;
    est_p = EST.(['win',num2str(ix_wrt_grid)]);
%     est_a = POLY.(['win',num2str(ix_wrt_grid)]);


    
    h = figure;
    set(gcf,'position', [158*2 164*2 1200/2 600])
    subplot(3,1,1:2)   
    plot_configs = {'ok','xk','+k','sk'};
    plot_configs1 = {'r','g','k','b'};
    for i = 1:nf
        sel = TSR.stack == i;
        date_p = TSR.Time(sel);
        tsr_p = TSR.(['win',num2str(ix_wrt_grid)])(sel);  
        est_a = POLY.(['win',num2str(ix_wrt_grid)])(date_p);
        plot(date_p,tsr_p,plot_configs{i})
        hold on
        plot(date_p,est_a)
    end
    
    grid_date = repmat(date_est,1,4) + repmat([-3,-3,3,3],length(date_est),1);
    Ys = ylim;
	grid_y = repmat([Ys(1) Ys(2) Ys(2) Ys(1)],length(date_est),1);
    
    for i = 1:length(date_est)
        fill(grid_date(i,:),grid_y(i,:),[1 1 1]-tsr_crb(i),'FaceAlpha',0.50,'EdgeColor', 'none')
    end

%     amb = los2vert*0.0556*1000/4; 
%     plot(date_est,est_p + amb,'.r');
%     plot(date_est,est_p - amb,'.r');
    plot(date_est,est_p,'r','LineWidth',2);
    hold off
    title({[num2str(ix1) ',' num2str(ix2)],['window ',num2str(ix_wrt_grid),' type ',num2str(win_type(ix))]})
    grid on;
    grid minor;
    ylabel('[mm]');
    h1 = get(gca,'Children');
    set(gca,'Children',flipud(h1))
    xlim([min(date_est) max(date_est)])
    subplot(3,1,3)   
    plot(date_v,tsr_crb/max(abs(tsr_crb)),'.k')
    grid on;
    hold on
    title('Cramer-Rao Bound (normalized) [-]')
    xlim([min(date_v) max(date_v)])


    
    number = h.Number;
    txts = findobj('Type','Text');
    scat = findobj('Type','Scat');    
    if ~isempty(txts)
        strings = [txts.String];
        chk = strcmp(strings,num2str(number)); 
        delete(txts(chk));
        delete(scat(chk));
    end
    
    figure(100)
    scatter(x_get,y_get,50,'Filled','k','s')
    text(x_get,y_get,num2cell(number),'FontSize',12,'FontWeight','bold');
    figure(number);
    
    
    

end