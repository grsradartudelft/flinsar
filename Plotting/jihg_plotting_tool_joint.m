clear all;
cwd = pwd;
addpath(genpath([cwd,'/..']));




% results
try
    load('results_delft.mat')
catch
%     tsrs = readtimetable('staphorst_deltares_tsr.csv');
%     tsr_vars = readtimetable('staphorst_deltares_var.csv');
%     info = readtable('staphorst_deltares_win_info.csv');
%     VAR = readtable('staphorst_deltares_plot_config.csv');
%     model = readtable('staphorst_deltares_model.csv');
%     save('staphorst_deltares.mat','tsrs','tsr_vars','info','VAR','model');    
end

im = imread('E:\Dropbox\PhD\Pictures\area_of_interest_referenced.png');


%% MAIN SCRIPT

win_lon = VAR.win_lon;
win_lat = VAR.win_lat;

ix = find(diff(win_lon)>0,1);
dlon = (win_lon(ix+1)-win_lon(1))/2;
dlat = (win_lat(2)-win_lat(1))/2;
lon_grid = repmat(win_lon,1,4) + [-dlon,-dlon,dlon,dlon];
lat_grid = repmat(win_lat,1,4) + [-dlat,dlat,dlat,-dlat];
g_faces = reshape(1:length(lon_grid(:)),4,[])';

% x_grid = nan(size(VAR(:,1)));
x_grid = VAR.model_amp;
% x_grid1 = nan(size(VAR(:,1)));
x_grid1= VAR.lin_sub;
% x_grid2 = nan(size(VAR(:,1)));
x_grid2 = VAR.gamma_pta;
title_flag = 2;

g.Faces = g_faces;
g.Vertices = [reshape(lon_grid',[],1) reshape(lat_grid',[],1)];
g.FaceVertexCData = x_grid;
% g.FaceVertexAlphaData  = alphas;
g.FaceAlpha = 0.5;
g.FaceColor='flat';
g.EdgeColor='none';



f=figure(100);
set(f,'Visible','off')
hold on
h2 = surf([win_lon(1)-dlon win_lon(end)+dlon; win_lon(1)-dlon win_lon(end)+dlon],...
    [win_lat(1)-dlat win_lat(1)-dlat; win_lat(end)+dlat win_lat(end)+dlat],...
    [0 0; 0 0],flipud(im),'facecolor','texture');
view(0,90);
h1=patch(g);
cl1 = prctile(x_grid,95);
cl2 = prctile(x_grid,5);
climit = max(abs([cl1,cl2]));
caxis([-climit,climit]);
axis tight

colormap(flipud(redblue))
if title_flag == 1
    title('displacement mm/yr');
else
    title('amplitude of CRFS model mm/mm')
end
colorbar

btn = uicontrol('Style', 'pushbutton', 'String', 'Select',...
    'Units','normalized','Position', [0.01 0.01 0.09 0.05],...
    'Callback', {@jihg_plottsr_joint,TSR_6d,CRB_6d,EST_6d,VAR});

btn1 = uicontrol('Style', 'pushbutton', 'String', 'Displacement',...
    'Units','normalized','Position', [0.1 0.01 0.1 0.05],...
    'Callback', {@plotnew1,x_grid1,h1});
btn2 = uicontrol('Style', 'pushbutton', 'String', 'Amplitude',...
    'Units','normalized','Position', [0.2 0.01 0.1 0.05],...
    'Callback', {@plotnew2,x_grid,h1});
btn3 = uicontrol('Style', 'pushbutton', 'String', 'Temporal Coherence',...
    'Units','normalized','Position', [0.3 0.01 0.1 0.05],...
    'Callback', {@plotnew3,x_grid2,h1});

btn4 = uicontrol('Style', 'pushbutton', 'String', 'Reset',...
    'Units','normalized','Position', [0.4 0.01 0.1 0.05],...
    'Callback', {@reset,f});

sld1=uicontrol('Style','slider',...
    'Min',0,'Max',1,'Value',0.5,'SliderStep',[0.1,0.2],...
    'Units','normalized','Position',[0.8 0.01 0.1 0.03] ,...
    'Callback', {@set_transparency,h1}); 

f.Visible= 'on';

function reset(~,~,f)
    scat = findobj('Type','Scatter');
    texts = findobj('Type','Text');
    delete(scat);
    delete(texts);
    set(f, 'HandleVisibility', 'off');
    close all
    set(f, 'HandleVisibility', 'on');
    jihg_plotting_tool_joint
end

function set_transparency(source,~,h1)
    num=source.Value;
    set(h1,'Alpha','none')
    set(h1,'FaceAlpha',num)
    set(h1,'EdgeAlpha',num)
    
end

   


function plotnew1(source,event,x_grid,h)
    set(h,'CData',x_grid);
    set(h,'CData',x_grid);
    cl1 = prctile(x_grid,95);
    cl2 = prctile(x_grid,5);
    climit = max(abs([cl1,cl2]));
    caxis([-climit,climit]);
    title('displacement mm/yr');
    colorbar

   end
   
function plotnew2(source,event,x_grid,h)
    set(h,'CData',x_grid);
    set(h,'CData',x_grid);
    cl1 = prctile(x_grid,95);
    cl2 = prctile(x_grid,5);
    climit = max(abs([cl1,cl2]));
    caxis([-climit,climit]);
    title('model amplitude');
    colorbar

end
   
function plotnew3(source,event,x_grid,h)
    x_grid(x_grid == 0) = NaN;
    set(h,'CData',x_grid);
    caxis([0,1]);
    title('temporal coherence');
    colorbar

end



