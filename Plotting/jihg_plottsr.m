function jihg_plottsr(~,~,S,results,CRB,win)

    [x_get,y_get,~]=ginput(1);
    
    nlines = S.win.nlines;
    npixels = S.win.npixels;
    dates_num = S.stackinfo.dates_num;
    win_f = S.win.win_f;
    dlat = win.dlat;
    dlon = win.dlon;
    lat_range = win.lat_range;
    lon_range = win.lon_range;    
    tsr_unw = results.tsr_unw;
    tsr_est = results.est_EP2;
    v_est  = results.v_est;

%     ifgs_coh = results.ifgs_coh;
    
    ix1 = find(lat_range < y_get + dlat/2,1,'last');
    ix2 = find(lon_range < x_get + dlon/2,1,'last');
    ix = sub2ind([nlines,npixels],ix1,ix2);
    
    date_p = datetime(datestr(dates_num(2:end)));
%     date_p = dates_num(2:end);
    tsr_p = -tsr_unw(ix,:)/2/pi; %*S.options.lambda*1000/4/pi;
    est_p = -tsr_est(ix,:)/2/pi; %*S.options.lambda*1000/4/pi;
    v_p = -v_est(ix,:)/2/pi;
    tsr_var = CRB(ix,:);
%     coh_p =  ifgs_coh(ix,:);
    if S.options.ps_flag == 1
        load('jihg_depsi_atmosphere_output.mat');
        atmos_p = -win_atmos_sm(ix,:)/2/pi;%*S.options.lambda*1000/4/pi;
        atmos_p2 = -win_atmos_dc(ix,:)/2/pi;%*S.options.lambda*1000/4/pi;
        atmos_p2 = [atmos_p2(1) cumsum(atmos_p2)];
    end

    amb = 1; %2*pi*S.options.lambda*1000/4/pi;
% 

    h = figure;
    set(gcf,'position', [158*2 164*2 1200/2 600])
    subplot(3,1,1:2)    
    plot(date_p,tsr_p,'.k','MarkerSize',15)
    hold on
    plot(date_p,tsr_p + amb,'.k');
    plot(date_p,tsr_p - amb,'.k');
    plot(date_p,est_p,'r','LineWidth',2);
     plot(date_p,v_p,'b','LineWidth',2);
    if S.options.ps_flag == 1
        plot(date_p,tsr_p+atmos_p,'ob');
    end
    hold off
    title({[num2str(ix1) ',' num2str(ix2)],['window ',num2str(ix),' type ',num2str(win_f(ix))]})
    grid on;
    grid minor;
    ylabel('cycles');
    subplot(3,1,3)   
    plot(date_p,tsr_var/max(abs(tsr_var)),'.k')
    grid on;
    title('Cramer-Rao Bound (normalized) [-]')

%     colormap(jet)
%     hold off
%     title({[num2str(ix1) ',' num2str(ix2)],num2str(ix),num2str(S.win.win_f(ix))})
%     grid on;
%     grid minor;
% %     datetick
%     
%     ylim(ylim1);

    
% %     btn = uicontrol(h,'Style', 'pushbutton', 'String', 'pdf',...
%     'Units','normalized','Position', [0.01 0.01 0.09 0.05],...
%     'Callback', {@jihg_plotpdfpatch,h,date_p,coh_p,tsr_p,nshp});
%     
% %     btn2 = uicontrol(h,'Style', 'pushbutton', 'String', 'reset',...
%     'Units','normalized','Position', [0.11 0.01 0.09 0.05],...
%     'Callback', {@delete_patch,h});
% 
% %     btn = uicontrol(h,'Style', 'pushbutton', 'String', 'pdf2',...
%     'Units','normalized','Position', [0.21 0.01 0.09 0.05],...
%     'Callback', {@jihg_plotpdf,h,date_p,coh_p,tsr_p,nshp});
% 
% %     sld1=uicontrol('Style','slider',...
%     'Min',0,'Max',1,'Value',0.5,'SliderStep',[0.1,0.2],...
%     'Units','normalized','Position',[0.8 0.01 0.1 0.03] ,...
%     'Callback', {@set_transparency,h}); 



% 
%     figure;
%     plot(date_p,coh_p)
%     datetick
%     grid on
    
    number = h.Number;
    txts = findobj('Type','Text');
    scat = findobj('Type','Scat');    
    if ~isempty(txts)
        strings = [txts.String];
        chk = strcmp(strings,num2str(number)); 
        delete(txts(chk));
        delete(scat(chk));
    end
    

    
    figure(100)
    scatter(x_get,y_get,50,'Filled','k','s')
    text(x_get,y_get,num2cell(number),'FontSize',12,'FontWeight','bold');
    figure(number);
        

end

function delete_patch(src,~,h)

t=findobj(h,'Type','Patch');
delete(t)
end

function set_transparency(source,~,h)
    num=source.Value;
    h1=findobj(h,'Type','Patch');
    set(h1,'Alpha','none')
    set(h1,'FaceAlpha',num)
    set(h1,'EdgeAlpha',num)
    
end