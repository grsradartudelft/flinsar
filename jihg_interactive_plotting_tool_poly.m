% Run this function inside an individual stack. Make sure the plotting
% function folder is in the path.



clear all;
S = load('S');
cwd = pwd;
addpath(genpath(cwd));

im = imread('E:\Dropbox\PhD\Processing\Interferometry\jihg\delft\input\delft_aoi.png'); % make sure this is an image that can be used as a background plot. Meaning the corners need to be georeferenced. (Can be done by eye).

results = [];
results_sbas = [];
% results SBAS
results_sbas = load('jihg_est_tsr_poly_output');
results_CRB = results_sbas.CRB_sm;
% results RCR (all stacks)
% results = load('jihg_rcr_output1');

win = load('jihg_aw_geocoords_output');
win_lon = win.win_lon;
win_lat = win.win_lat;


% %%
mask =S.win.win_f ~= 0;
in_p = S.win.win_f == 3;

%% comment out 
% linear subsidence
x_grid = results_sbas.est_param_tsr(:,2)/(4*pi)*0.056*1000;%.*mask;
x_grid(~in_p) = NaN;
title_flag = 1;

%  % AMPLITUDE ESTIMATE
% x_grid = results.EP_final(:,3)/(2*pi);%
% x_grid = results.v_res/(2*pi);%
% title_flag = 2;

%%



% [ref_ind] = jihg_unw_setref(S);
% ref_lon = win_lon(ref_ind);
% ref_lat = win_lat(ref_ind);
ix = find(diff(win_lon)>0,1);
dlon = (win_lon(ix+1)-win_lon(1))/2;
dlat = (win_lat(2)-win_lat(1))/2;
lon_grid = repmat(win_lon,1,4) + [-dlon,-dlon,dlon,dlon];
lat_grid = repmat(win_lat,1,4) + [-dlat,dlat,dlat,-dlat];
g_faces = reshape(1:length(lon_grid(:)),4,[])';


g.Faces = g_faces;
g.Vertices = [reshape(lon_grid',[],1) reshape(lat_grid',[],1)];
g.FaceVertexCData = x_grid;
% g.FaceVertexAlphaData  = alphas;
g.FaceAlpha = 0.5;
g.FaceColor='flat';
g.EdgeColor='none';




f=figure(100);
set(f,'Visible','off')
hold on
h2 = surf([win_lon(1)-dlon win_lon(end)+dlon; win_lon(1)-dlon win_lon(end)+dlon],...
    [win_lat(1)-dlat win_lat(1)-dlat; win_lat(end)+dlat win_lat(end)+dlat],...
    [0 0; 0 0],flipud(im),'facecolor','texture');
view(0,90);
h1=patch(g);
% scatter(6.0884845,52.6332045,30,'w','filled');
axis tight
cl1 = prctile(x_grid,95);
cl2 = prctile(x_grid,5);
climit = max(abs([cl1,cl2]));
caxis([-climit,climit]);
% colormap(flipud(redblue))
if title_flag == 1
    title('displacement LOS mm/yr');
else
    title('amplitude of CRFS model cycles/mm')
end
colorbar

btn = uicontrol('Style', 'pushbutton', 'String', 'Select',...
    'Units','normalized','Position', [0.01 0.01 0.09 0.05],...
    'Callback', {@jihg_plottsr,S,results,results_CRB,win});
btn2 = uicontrol('Style', 'pushbutton', 'String', 'Select sbas',...
    'Units','normalized','Position', [0.2 0.01 0.09 0.05],...
    'Callback', {@jihg_plottsr_sbas,S,results_sbas,results_CRB,win});
btn4 = uicontrol('Style', 'pushbutton', 'String', 'Reset',...
    'Units','normalized','Position', [0.4 0.01 0.1 0.05],...
    'Callback', {@reset,f});
btn5 = uicontrol('Style', 'pushbutton', 'String', 'User Input',...
    'Units','normalized','Position', [0.3 0.01 0.1 0.05],...
    'Callback',{@jihg_plottsr_sbas_user_input,S,results_sbas,results_CRB,win});

sld1=uicontrol('Style','slider',...
    'Min',0,'Max',1,'Value',0.5,'SliderStep',[0.1,0.2],...
    'Units','normalized','Position',[0.8 0.01 0.1 0.03] ,...
    'Callback', {@set_transparency,h1}); 

f.Visible= 'on';

function reset(~,~,f)
    scat = findobj('Type','Scatter');
    texts = findobj('Type','Text');
    delete(scat);
    delete(texts);
    set(f, 'HandleVisibility', 'off');
    close all
    set(f, 'HandleVisibility', 'on');
    jihg_interactive_plotting_tool
end

function set_transparency(source,~,h1)
    num=source.Value;
    set(h1,'Alpha','none')
    set(h1,'FaceAlpha',num)
    set(h1,'EdgeAlpha',num)
    
end






