function jihg_joint_rcr(sim_flag)

S.options = load('jihg_J');
main_dir = S.options.main_dir;
joint_output_folder = S.options.joint_output_folder;
S.options.output_path = [main_dir,'/',joint_output_folder,'/'];
jihg_init(S);
folders = S.options.output_folder;

    db               = dbstack;
    if length(db) == 1
        sim_flag = 0;
    end


    nf = length(folders);
    

    for i = 1:nf
        input_folder = [main_dir,'/',folders{i},'/'];
        S = load([input_folder,'S.mat']);
        S.sim_flag = sim_flag;
        cpxcoh_output = load([input_folder,'jihg_cpxcoh_output.mat'],'gamma_pta');
        
        % check which output to load
        
        if sim_flag == 1
            output_tsr = load([input_folder,'Simulation/jihg_est_tsr_sim_output']);
        else
            output_tsr = load([input_folder,'jihg_est_tsr_output.mat']);
        end

    
    %% estimate model parameters
        win_model = ones(S.win.n,1); win_model(S.win.win_f == 3) = 2;
            % temporal unwrap is done to correct possible unwrapping errors in,
            % which might still be present after the sbas+ step. This step is
            % not done on windows with pixels labeled as polygon.       

        [S,output_t_unw]      = jihg_temporal_unwrap(S,output_tsr); 
        % estimate the scaling factor of the provided deformation model for
        % windows with pixels labeled as polygon.
        [S,output_model]      = jihg_poly_model_v2(S,output_tsr);   
        % results in 2 functional models, which we are kept seperately.
        % This is done, because of uncertainties in joint estimation of
        % linear trend and deformation model in the RCR step (see later).

        EP1             = output_t_unw.est_param;
        EP2             = output_model.est_param;        
        DM1             = output_t_unw.design_matrix;
        DM2             = output_model.design_matrix;         
        
        output = output_tsr;
        output.win = S.win;
        output.gamma_pta = cpxcoh_output.gamma_pta;
        output.EP1 = EP1;
        output.EP2 = EP2;
        output.DM1 = DM1;
        output.DM2 = DM2;
        output.win_model = win_model;
        output.los2vert  =  1./cosd(S.win.inc);
        
        joint.(['stack',num2str(i)]) =  output;
    end
    
    %% Check consistency between stacks
    % Currently excluding a window if not consistent across stacks. This could be
    % changed. 
    
    gamma_pta = zeros(S.win.n,nf);
    win_f_all = zeros(S.win.n,nf);
    win_nshp = zeros(S.win.n,nf);
    los2vert = zeros(S.win.n,nf);
    for i = 1:nf        
        gamma_pta(:,i) = joint.(['stack',num2str(i)]).gamma_pta;
        win_f_all(:,i) = joint.(['stack',num2str(i)]).win.win_f;
        win_nshp(:,i)  = joint.(['stack',num2str(i)]).win.nshp;
        los2vert(:,i)  = joint.(['stack',num2str(i)]).los2vert;
    end
    
    win_f_new = win_f_all(:,1);
    win_f_new(~all(win_f_all == win_f_all(:,1),2)) = -1;
    

    
    %% average parameters
    % keeping models seperated for possible comparison between default and
    % used model. 
    npar1 = size(DM1,2);
    EP1_avg = zeros(S.win.n,npar1);
    for j = 1:npar1
        est_param=zeros(S.win.n,nf);
        for i = 1:nf        
            est_param(:,i) = joint.(['stack',num2str(i)]).EP1(:,j);
        end 
        
        % average weighted by gamma_pta
        weights = gamma_pta./sum(gamma_pta,2);
        EP1_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical      
    end
    
    % set windows values to 0 for clarity
    EP1_avg(win_f_new < 0,:) = 0;
    EP1_avg(win_f_new == 3,:)  = 0;    
    
    
    % now model for polygons
    npar2 = size(DM2,2);
    EP2_avg = zeros(S.win.n,npar1);
    for j = 1:npar2
        est_param=zeros(S.win.n,nf);
        for i = 1:nf        
            est_param(:,i) = joint.(['stack',num2str(i)]).EP2(:,j);
        end 
        
        % average weighted by gamma_pta
        weights = gamma_pta./sum(gamma_pta,2);
        EP2_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical            
    end
    
    % set windows values to 0 for clarity
    
    EP2_avg(win_f_new < 0,:) = 0;
    EP2_avg(win_f_new == 1,:)  = 0;
    EP2_avg(win_f_new == 2,:)  = 0;
    
    
    %% Remove-compute-restore based on combined estimate of parameters
    
    % Need to do it per stack, as the stacks are independent
    
    % only 2 models being used. 
    win_f_iter = win_f_new;
    for k = 1:2
    %% number of iterations
        iter_step = k;
        for i = 1:nf
            
            
            %% per stack            
            input_folder = [main_dir,'/',folders{i},'/'];
            S = load([input_folder,'S.mat']);
            S.sim_flag = sim_flag;
            % input data
            if sim_flag == 1
                input = load([input_folder,'Simulation/jihg_est_tsr_sim_output']);
            else
                input = load([input_folder,'jihg_est_tsr_output.mat']);
            end
            
            input.DM1 = joint.(['stack',num2str(i)]).DM1;
            input.DM2 = joint.(['stack',num2str(i)]).DM2;            
            
            input.EP1 = joint.(['stack',num2str(i)]).EP1;
            input.EP2 = joint.(['stack',num2str(i)]).EP2;

            
            input.EP1_avg = EP1_avg./los2vert(:,i); % back to LOS
            input.EP2_avg = EP2_avg./los2vert(:,i);
            
            input.iter_step = iter_step;
            input.gamma_pta = gamma_pta(:,i);
            input.win_f_joint = win_f_iter;
            
            input.iter_step = iter_step;
            
            
            [S,output_rcr] = jihg_rcr(S,input); 
            
            joint_rcr.(['stack',num2str(i)]) =  output_rcr;
            joint_rcr.(['stack',num2str(i)]).CRB = joint.(['stack',num2str(i)]).CRB_sm;
        end
        
        
            %% average parameters
        npar1 = size(DM1,2);
        EP1_avg = zeros(S.win.n,npar1);
        for j = 1:npar1
            est_param=zeros(S.win.n,nf);
            for i = 1:nf        
                est_param(:,i) = joint_rcr.(['stack',num2str(i)]).EP1(:,j);
            end 

            % average weighted by gamma_pta
            weights = gamma_pta./sum(gamma_pta,2);
            EP1_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical      
        end

        % set windows values to 0 for clarity
        EP1_avg(win_f_new < 0,:) = 0;
        EP1_avg(win_f_new == 3,:)  = 0;    


        % now model for polygons
        npar2 = size(DM2,2);
        EP2_avg = zeros(S.win.n,npar1);
        for j = 1:npar2
            est_param=zeros(S.win.n,nf);
            for i = 1:nf        
                est_param(:,i) = joint_rcr.(['stack',num2str(i)]).EP2(:,j);
            end 
            
            % any positive shrinkage values?
            is_pos = false(size(est_param));
            if j == 2
                is_pos = est_param > 0;
            end
                
            est_param(is_pos) = 0;

            % average weighted by gamma_pta
            weights = gamma_pta./sum(gamma_pta,2);
            weights(is_pos) = 0;
            EP2_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical            
        end

        % set windows values to 0 for clarity
        EP2_avg(win_f_new < 0,:) = 0;
        EP2_avg(win_f_new == 1,:)  = 0;
        EP2_avg(win_f_new == 2,:)  = 0;
        
        % if entire window is positive, then model is not correct, so to
        % -1 for now
        is_pos = EP2_avg(:,2) > 0;        
        win_f_iter(is_pos) = -1;    
                
        
        
    end
    
    % function to generate final output
    
    input = joint_rcr;
    input.EP1_avg = EP1_avg;
    input.EP2_avg = EP2_avg;
    input.win_f_final = win_f_iter;
    input.gamma_pta = gamma_pta;
    input.los2vert = los2vert;
    
    clearvars -except S input
    
    jihg_create_joint_output(S,input);
    
    
    
end