function [S,output] = jihg_make_patches(S,input)
%JIHG_MAKE_PATCHES checks whether or not patches need to be created.
% Patches are required if the stack of SLCS cannot be loaded into memory.

% Author: FMG HEUFF - Feb 2020.

%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    %% Otherwise run function.
        % Read input
        
        win_subs        = input.win_subs;
        win_lines       = input.nwin_l;
        win_pixels      = input.nwin_p;
        dlat            = input.dlat;
        dlon            = input.dlon;
        bb_ll           = input.bb_ll;

        nslcs           = S.stackinfo.nslcs;
        nlines          = S.stackinfo.nlines;
        npixels         = S.stackinfo.npixels;   
        max_mem         = S.options.max_mem;
        nwin            = S.win.n;
        
        path_coords     = S.stackinfo.path_coords;

        % MAIN    
        lon = freadbk_quiet(path_coords{1},nlines);
        lat = freadbk_quiet(path_coords{2},nlines);

        nwin_l      = input.nwin_l;
        nwin_p      = input.nwin_p;
        win_lon     = input.win_lon;
        win_lat     = input.win_lat;
        win_subs    = reshape(win_subs,win_lines,win_pixels);
        
        
        % estimate size of stack that has to be loaded into memory. This
        % can vary based on the size of the area of interest.
        
        [crop_ind]  = bounding_box(lon,lat,[bb_ll(2)-dlat/2,bb_ll(4)+dlat/2],[bb_ll(1)-dlon/2,bb_ll(3)+dlon/2]);
        stack_size  = (crop_ind(3) - crop_ind(1))*(crop_ind(4)-crop_ind(2))*16*nslcs*1.10;
        npatch      = ceil(stack_size/max_mem);

        if win_lines/npatch < 4
        % We want atleast 5 lines per patch, if less than 5 we also not
        % to create patches in the pixel direction.
            
            % NOT DONE YET

        else     
        % Preferably create patches in line direction. Lines are most
        % efficiently read into memory.
            
            patch_lines     = double(round(linspace(crop_ind(1),crop_ind(3),npatch+1)));
            patch_pixels    = double(round(linspace(crop_ind(2),crop_ind(4),2)));

        end
        
        patch_win_subs      = cell(npatch,1);
        patch_win_ixs       = cell(npatch,1);

        [t1,t2]             = meshgrid(patch_pixels,patch_lines);

        % The geocoordinate grid is tilted w.r.t. to line/pixels.
        % Creating patches based on line/pixel coordinates will create
        % problems if not accounted for this tilt.
        
        % 1) Create initial patches based on line/pixels
        % 2) Check which windows are inside patch.
        % 3) Check by how many lines/pixels the window needs to be expanded
        % to include all pixels of the windows inside the patch.
        
        % Geocoordinate grid
        lon_grid            = repmat(win_lon,1,4) + [-dlon,-dlon,dlon,dlon]/2;
        lat_grid            = repmat(win_lat,1,4) + [-dlat,dlat,dlat,-dlat]/2;
        
        % initial patches, not yet expanded
        patch_p_p           = [t1(1:end-1,1),t1(1:end-1,1),t1(1:end-1,2) t1(1:end-1,2)];
        patch_p_l           = [t2(1:end-1,1),t2(2:end,:) t2(1:end-1,1)];
        patch_ind           = sub2ind([nlines,npixels],patch_p_l,patch_p_p);
    
        % Polygons make for easy checks. Initial patches in geocoordinates.
        poly_shape1 = cell(npatch,1);
        for j = 1: npatch
            poly_shape1{j} = polyshape(lon(patch_ind(j,:)),lat(patch_ind(j,:)));
        end

        patch_min = nan(npatch,2);
        patch_max = nan(npatch,2);
        
        tic
        for i = 1:nwin
        %% loop over windows window
            lon_p = lon_grid(i,:);
            lat_p = lat_grid(i,:);

            poly_shape2 = polyshape(lon_p,lat_p);            
            TF = false;
            j = 1;
            while ~TF && j <= npatch
            %% loop over patches to find inside which patch the window lies
            
                TF = overlaps(poly_shape2,poly_shape1{j});          
                if TF
                %% Overlaps? Save window index and subs.
                
                    [iy,ix]                         = ind2sub([nwin_l,nwin_p],i); 
                    patch_win_subs{j}(end+1,1:2)    = uint16([iy ix]);
                    patch_win_ixs{j}(end+1,1)       = i;                    
                    
                    % check whether or not patch needs to be expanded to
                    % include all pixels of current window.
                    temp_subs                       = double(win_subs{i});
                    max_ixs                         = max(temp_subs,[],1);
                    min_ixs                         = min(temp_subs,[],1);                    
                    patch_min(j,:)                  = min([patch_min(j,:);min_ixs],[],1);                    
                    patch_max(j,:)                  = max([patch_max(j,:);max_ixs],[],1);
                    
                    if patch_max(j,1) > nlines
                        error('cannot')
                    end
                    
                end
                
                j = j+1;
            end            
                
            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)                
            end
        end

       % Final result
        patches = [patch_min(:,1) patch_max(:,1) patch_min(:,2) patch_max(:,2)];
        
        output.patches = patches;
        output.patch_win_ixs = patch_win_ixs;
        output.patch_win_subs = patch_win_subs;
    
       save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
end