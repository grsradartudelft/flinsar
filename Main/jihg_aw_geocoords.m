function [S,output] = jihg_aw_geocoords(S)
% JIHG_AW_GEOCOORDS creates windows in geocoordinates.


% Author: FMG HEUFF - Feb 2020.

%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
        %% Otherwise run function
        
        main_dir        = S.options.main_dir;
        nlines          = S.stackinfo.nlines;
        npixels         = S.stackinfo.npixels;
        path_coords     = S.stackinfo.path_coords;
        
        win_size        = S.options.win_size; %[m]
        ll              = S.options.ll; % lowerleft of aoi
        ur              = S.options.ur; % upper right of aoi
        convert2rd      = S.options.convert2rd;
        
        % python function to convert WSG84 to RD. This makes it possible to
        % create windows closest to the provided window width in [m].
        
        P = py.sys.path;
        if count(P,'/Users/pconroy/phd/code/floris/JIHG_v6/jihg_modules/Utilities') == 0
            insert(P,int32(0),'/Users/pconroy/phd/code/floris/JIHG_v6/jihg_modules/Utilities')
        end
        
        %PC: i assume bb = bounding box, ll=lat/long, rd=dutch coordinates
        if ll(1) > 100        
            x1=py.wsg2rd.rd2wsg(ll(1),ll(2));
            x2=py.wsg2rd.rd2wsg(ur(1),ur(2));
            bb_rd = [ll(1) ll(2) ur(1) ur(2)];
            bb_ll = [x1{1},x1{2},x2{1},x2{2}];
        else

            x1=py.wsg2rd.wsg2rd(ll(1),ll(2));
            x2=py.wsg2rd.wsg2rd(ur(1),ur(2));
            bb_ll = [ll(1) ll(2) ur(1) ur(2)];
            bb_rd = [x1{1},x1{2},x2{1},x2{2}];
        end

        % read lon/lat raw files
        lon = freadbk_quiet(path_coords{1},nlines);
        lat = freadbk_quiet(path_coords{2},nlines);
        
        %PC: I dont know what this
        if strcmp(convert2rd,'yes')
            fprintf('Starting conversion of lat lon to RD...\n')
            
            crop = shaperead('E:\Dropbox\PhD\Processing\GIS\Delft\Shape_Delfland\Delfland_polygon_new.shp');
            
            in_aoi = inpolygon(lon,lat,crop.X,crop.Y);
            
            x1=py.wsg2rd.wsg2rd(lat(in_aoi),lon(in_aoi));
            rdx = double(x1{1})';
            rdy = double(x1{2})'; 
            
            T=table(rdx,rdy,lon(in_aoi),lat(in_aoi),'VariableNames',{'RD_x','RD_y','longitude','latitude'});
            
            writetable(T,[output_path,'rd_coords.csv'])

            
        end
        
        % determine number of windows
        
        %PC: doesnt this need to be a whole number?
        n_win_x = (bb_rd(3) - bb_rd(1))/(win_size);
        n_win_y = (bb_rd(4) - bb_rd(2))/(win_size);
        % determine stepsize
        dlat = (bb_ll(3) - bb_ll(1))/n_win_y;
        dlon = (bb_ll(4) - bb_ll(2))/n_win_x;
        
        lat_range = bb_ll(1):dlat:bb_ll(3);
        lon_range = bb_ll(2):dlon:bb_ll(4);

        nwin_l  = length(lat_range);
        nwin_p  = length(lon_range);
        nwin    = nwin_l*nwin_p;
        
        % preallocate cells, number of pixels per window varies.
        win_subs=cell(nwin_l*nwin_p,1);
        win_ixs=cell(nwin_l*nwin_p,1);
    
        tic
        % loop over all pixels and find the window they fall inside.
        for i = 1:length(lat(:))
            
            ix1 = find(lat(i) + dlat/2 > lat_range & lat(i) - dlat/2 < lat_range);
            ix2 = find(lon(i) + dlon/2 > lon_range & lon(i) - dlon/2 < lon_range); 

            if isempty(ix1) || isempty(ix2)
                if mod(i,round(length(lat(:))/10)) == 0
                    fprintf('Finished %i of %i pixels. [%i%% %0.2f s] \n',i,length(lat(:)),round(i/length(lat(:))*100),toc)
                end
                continue
            end 
            
            % save indices, win_line/win_pixel.
            [iy,ix]                     = ind2sub([nlines,npixels],i); 
            ix_w                        = sub2ind([nwin_l,nwin_p],ix1(1),ix2(1));
            win_subs{ix_w}(end+1,1:2)   = uint16([iy ix]);
            win_ixs{ix_w}(end+1,1)      = i; 
            
            if i == 26508
                  i
            end
            
            if mod(i,round(length(lat(:))/10)) == 0
                fprintf('Finished %i of %i pixels. [%i%% %0.2f s] \n',i,length(lat(:)),round(i/length(lat(:))*100),toc)
            end

        end
        
        [win_lon,win_lat] = meshgrid(lon_range,lat_range); 
        
        test_len = 0
        for i = 1:length(win_ixs)
            test_len = test_len + length(win_ixs{i});
        end
        
        x1=py.wsg2rd.wsg2rd(win_lat(:),win_lon(:));
        win_rdx = double(x1{1})';
        win_rdy = double(x1{2})';     
       
        
        % output
        output.win_subs = win_subs;
        output.win_ixs = win_ixs;
        output.win_lon = win_lon(:);
        output.win_lat = win_lat(:);
        output.win_rdx = win_rdx;
        output.win_rdy = win_rdy;
        
        output.nwin_l = nwin_l;
        output.nwin_p = nwin_p;
        output.nwin = nwin; 
        
        output.dlat = dlat;
        output.dlon = dlon;
        output.bb_ll = bb_ll;
        output.bb_rd = bb_rd;
        output.lon_range = lon_range;
        output.lat_range = lat_range;
        
       save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.win.nlines = output.nwin_l;
    S.win.npixels = output.nwin_p;
    S.win.n = output.nwin;
    S.win.dlat = output.dlat;
    S.win.dlon = output.dlon;
    S.win.lon_range = output.lon_range;
    S.win.lat_range = output.lat_range;

end