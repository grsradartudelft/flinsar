function [S,output] = jihg_poly_sel(S)
% JIHG_POLY_SEL ids pixels inside the provided polygon(s). Note, this is NOT
% polygon based multi-looking.

% Author: FMG HEUFF - Feb 2020.

%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
%% select pasture fields

    nlines          = S.stackinfo.nlines;
    npixels         = S.stackinfo.npixels;
    path_coords     = S.stackinfo.path_coords;
    shape_path      = S.options.shape_path;

    lon             = freadbk(path_coords{1},nlines);
    lat             = freadbk(path_coords{2},nlines);
    P               = shaperead(shape_path);
    npoly           = length(P);
    
    % function that optimizes gridding of dataset
    % to prevent needlessly large for loops over polygons and points
    f1 = @(x,c) x(1)*x(2)*c(1) + c(1)/(x(1)*x(2))*c(2)/x(1)*c(3)/x(2); 
    f2 = @(x)f1(x,[npoly,nlines,npixels]);
    options = optimset('TolX',1,'TolFun',1);
    result  = fminsearch(f2,([1,1]),options);

    n_grd_l         = round(result(1));
    n_grd_p         = round(result(2));
    grd_l_ix        = round(linspace(1,nlines,round(result(1))));
    grd_p_ix        = round(linspace(1,npixels,round(result(2))));

    pix_in          = false(size(lon)); % No pixels labeled yet.
    pix_poly_id     = zeros(size(lon));

    %PC: added this option to just process the whole image
    if S.options.do_all_pixels == 1
        pix_in(:) = 1;
        pix_poly_id(:) = 1;
    else
    
    tic
    % Loop over grid
    for i = 1:(n_grd_l-1)
        for j = 1:(n_grd_p-1)
            % create polygons from grid corners
            lon_grd = [lon(grd_l_ix(i),grd_p_ix(j)) lon(grd_l_ix(i+1),grd_p_ix(j))...
                lon(grd_l_ix(i+1),grd_p_ix(j+1)) lon(grd_l_ix(i),grd_p_ix(j+1)) lon(grd_l_ix(i),grd_p_ix(j))];

            lat_grd = [lat(grd_l_ix(i),grd_p_ix(j)) lat(grd_l_ix(i+1),grd_p_ix(j))...
                lat(grd_l_ix(i+1),grd_p_ix(j+1)) lat(grd_l_ix(i),grd_p_ix(j+1)) lat(grd_l_ix(i),grd_p_ix(j))];
            
            % Check if polygon intersects grid. 
            for k = 1:npoly
                x = inpolygon(P(k).X,P(k).Y,lon_grd,lat_grd);

                if any(x)
                %%                 
                 % If one of polygon coordinates falls inside the grid 
                 % cell, it intersects. Then check which pixels fall inside 
                 % the polygon.
    
                    lon_sel     = lon(grd_l_ix(i):grd_l_ix(i+1),grd_p_ix(j):grd_p_ix(j+1));
                    lat_sel     = lat(grd_l_ix(i):grd_l_ix(i+1),grd_p_ix(j):grd_p_ix(j+1));
                    xx          = inpolygon(lon_sel,lat_sel,P(k).X,P(k).Y);
                    
                    % Add newly labeled pixels to result.
                    pix_in(grd_l_ix(i):grd_l_ix(i+1),grd_p_ix(j):grd_p_ix(j+1)) = ...
                    pix_in(grd_l_ix(i):grd_l_ix(i+1),grd_p_ix(j):grd_p_ix(j+1)) | xx;

                    % Save polygon # pixels fall inside for each pixel.
                    sel = pix_poly_id(grd_l_ix(i):grd_l_ix(i+1),grd_p_ix(j):grd_p_ix(j+1));
                    sel(xx) = k;
                    pix_poly_id(grd_l_ix(i):grd_l_ix(i+1),grd_p_ix(j):grd_p_ix(j+1)) = sel;
               end
            end
        end
        
        if mod(i,round((n_grd_l-1)/10)) == 0
        fprintf('Finished %i of %i rows. [%i%% - %0.2f s] \n',i,(n_grd_l-1),round(i/(n_grd_l-1)*100),toc)                
        end
    end
    end
    output.pix_in       = pix_in;
    output.pix_poly_id  = pix_poly_id;
    
    
    save([output_path,fun_name,'_output'],'-struct','output');
else
    output = load([output_path,fun_name,'_output.mat']);
end

end