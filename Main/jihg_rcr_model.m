function [S,output]= jihg_rcr_model(S,input)
    

%     rcr = input.rcr;
    
    s               = dbstack;
    if length(s) == 1
        jihg_joint_rcr
    end
    fun_name        = s(1).name;
    output_path = S.options.output_path;
    save_name       = [fun_name,'_output.mat'];
    if ~(exist(fullfile(output_path,save_name),'file')==2)
        
    nwin = S.win.n;
    coh = input.tsr_coh;
    CRB = input.CRB_sm;
    tsr = input.tsr_unw;
    ifgs = input.ifg_unw;
    nifgs = S.stackinfo.nifgs;
    method= S.options.model;
    dates_num = S.stackinfo.dates_num;
    dates = datetime(datestr(dates_num(2:end)));

    in_p = S.win.win_f == 3;     
        tic
        if strcmp(method,'CRFS')        
            T = jihg_calc_crfs_model(S);
            T2 = T(dates,:);     
            model_intra_annual = T2.('model');
            model_intra_annual = smooth(model_intra_annual,5);
            model_subs = cumsum(model_intra_annual);
            % like velocity, but with varying slopes.
            model_subs = -model_subs/max(abs(model_subs))*(dates_num(end) - dates_num(1))/365.25;
            
            A = [ones(nifgs,1) model_intra_annual];
            B = [0; diff(model_intra_annual)];
            tsr_dc = diff(tsr,[],2);

            est_ini = zeros(nwin,size(A,2));
            for i = 1:nwin
                if in_p(i)
                    y = tsr_dc(i,:);        
%                     W=(coh(i,:).^2) ./ (1-coh(i,:).^2);% Fisher Information as weights
%                     W(isinf(W)) = 0;
%                     %         W = ones(1,nifgs);
                    W = 1./CRB(i,:);
                    if rank(W) == 0
                        W = ones(size(W));
                    end
                    y_new = zeros(length(y),1);
                    val = 0;
                    for j = 1:length(y)                

                        if abs(model_intra_annual(j)) < 1
                            val = 0;
                        end

                    val = val + y(j);
                    y_new(j+1) = val;

                    end

                    est_ini(i,:) = lscov(A,y_new,W');
                    test = lscov(B,ifgs(i,:)',W');
                    figure(1);
                    plot(A*est_ini(i,:)');
                    hold on
                    plot(cumsum(B*test));
                    plot(y_new);
                    hold off
                    figure(2)
                    plot(ifgs(i,:));
                    hold on
                    plot(B*test);
                    hold off
                    
                    figure(3)
                    plot(cumsum(ifgs(i,:)));
                    hold on
                    plot(cumsum(B*test));
                    
                    hold off
                    tip
                    pause
                else


                end

                if mod(i,round(nwin/10)) == 0
                    fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
                end

            end

            model_amp = A(:,2)*est_ini(:,2)';        
            
            
            B = [ones(nifgs,1) model_intra_annual];
            est_param = [est_ini(:,1)  est_ini(:,2)];

%             B = [ones(nifgs,1) intra_annual];
%             est_param = [est_ini(:,1) est_ini(:,2)];
            output.est_param = est_param;
            output.model = model_intra_annual;
            output.model_amp = model_amp'; % scaled model per prixel;
            output.in_p = in_p; % logical for model location
            output.design_matrix = B;
            save([output_path,save_name],'-struct','output');
        end
    
    else
        output = load([output_path,save_name]);
    end

end

