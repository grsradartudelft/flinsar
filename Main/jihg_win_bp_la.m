function [S,output] = jihg_win_bp_la(S,input,input_geo)
%JIHG_WIN_BP_LA computes the perp. baseline and lookangle/incidence angle/

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    %% Otherwise run function.
        % Read input
        
        nlines          = S.stackinfo.nlines;
        npixels         = S.stackinfo.npixels;
        sc_bb           = S.stackinfo.scene_corners; % Corners w.r.t. original SLC from doris 5.0.
        dem_path        = S.stackinfo.path_dem;
        res_path        = S.stackinfo.path_resfiles;
        master_date     = S.options.master_date;
        dates           = S.stackinfo.dates;        
        nwin            = S.win.n;
        win_f           = S.win.win_f;
        win_shp         = input.win_shp; 
        win_ixs         = input_geo.win_ixs;


        %% MAIN
        dem = freadbk_quiet(dem_path,nlines);
        
        % preallocate arrays
        avg_h       = zeros(nwin,1);
        scn_loc     = zeros(nwin,2);
        win_cntr    = zeros(nwin,2);
        
        % For each window find center of gravity.
        % Make sure to correct for original SLC size.

        for i = 1:nwin

            
            if win_f(i) >= 0
                ixs = win_shp{i};
                [rix,cix]       = ind2sub([nlines,npixels],ixs);
                cntr            = round(mean([rix,cix],1)); % make sure to average columns, not rows
                avg_h(i)        = mean(dem(ixs));
                scn_loc(i,:)    = cntr + sc_bb(1:2)-1; 
                win_cntr(i,:)   = cntr;
            elseif win_f(i) == -1
                ixs             = win_ixs{i};
                [rix,cix]       = ind2sub([nlines,npixels],ixs);
                cntr            = round(mean([rix,cix],1));
                avg_h(i)        = mean(dem(ixs));
                scn_loc(i,:)    = cntr + sc_bb(1:2)-1; 
                win_cntr(i,:)   = cntr; 
            end


        end
        
        is_empty    = win_f == -2; % check for empty windows
        %% Run la_bp_inc
        [la_out,bp_out,inc_out,h2ph_out] = la_bp_inc(scn_loc(~is_empty,1),scn_loc(~is_empty,2),avg_h(~is_empty),str2double(master_date),dates,res_path);
        
        la      = zeros(nwin,1);                    la(~is_empty,:)     = la_out;
        bp      = zeros(nwin,length(dates)-1);      bp(~is_empty,:)     = bp_out;
        inc     = zeros(nwin,1);                    inc(~is_empty,:)    = inc_out;
        h2ph    = zeros(nwin,length(dates)-1);      bp(~is_empty,:)     = h2ph_out;
        
        %% output
        
        output.win_la = la;
        output.win_bp = bp;
        output.win_inc = inc;
        output.win_h2ph = h2ph;
        output.win_cntr = win_cntr;
        output.scn_loc = scn_loc;
        
        
        save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.win.inc = output.win_inc;
    S.win.la  = output.win_la;

end