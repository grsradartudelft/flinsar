function [S] = jihg_id_pixels(S)
%JIHG_ID_PIXELS Labels pixels based on amplitude and contextial information

% Author: FMG HEUFF - Feb 2020.

    id_ps_flag = S.options.id_ps_flag;
    poly_flag = S.options.poly_flag;

    nlines = S.stackinfo.nlines;
    npixels = S.stackinfo.npixels;
    
    % Possible pixel ids

    pix_ps = false(nlines,npixels);
    pix_in_p = false(nlines,npixels);

    % id PS pixels based on amplitude dispersion
    if id_ps_flag
        
        ps_flag = S.options.ps_flag;
        if ps_flag == 1
            %% Use PS processing results
            % label PS pixels:
            ps_software = S.options.ps_software;
            switch ps_software
                case {'depsi'}
                    depsi_path = S.options.depsi_path;
                    depsi_project_id = S.options.depsi_project_id;
                    dates_stack = S.stackinfo.dates_stack;
                    n_epochs = length(dates_stack) - 1;
                    
                    psp_info_fid = [depsi_path,'/',depsi_project_id,'_psp_sel1.raw'];  
                    psp_info = freadbk(psp_info_fid,1,'double');
                    psp_info = reshape(psp_info,n_epochs*2+4,[])';
                    psp_ind = sub2ind([nlines,npixels],psp_info(:,3),psp_info(:,4));
                    pix_ps(psp_ind) = 1;
                otherwise
                    error('No valid ps processing used')
            end
        else  
            %% Quick-n-dirty
            % label PS pixels:
            [S,output_amp] = jihg_amp_fun(S); % check if step is done.
            D_threshold = S.options.D_thres;
            pix_ps = sqrt(output_amp.D_sq) < D_threshold;
        end
    end
    
    % id pixels in polygon(s)
    if poly_flag
        [S,output_poly] = jihg_poly_sel(S);
        pix_in_p = output_poly.pix_in;
    end

    % label remaining pixels 
    pix_rem = ~(pix_ps | pix_in_p);

    pix_id.ps = pix_ps;
    pix_id.in_p = pix_in_p;
    pix_id.rem = pix_rem;
    
    % Since the result is not large in size (3 logicals), add it to the settings
    % structure.
    
    S.pix_id = pix_id;

end

