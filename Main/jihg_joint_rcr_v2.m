function jihg_joint_rcr_v2(sim_flag)

S.options = load('jihg_J');
main_dir = S.options.main_dir;
joint_output_folder = S.options.joint_output_folder;
S.options.output_path = joint_output_folder; 
jihg_init(S);
folders = S.options.output_folder;

    db               = dbstack;
    if length(db) == 1
        sim_flag = 0;
    end


    nf = length(folders);
    ttic=tic;
    % I'll leave commenting on the simulation options for another time...
    % thanks buddy
    if sim_flag == 1
         
         dates_all = [];
         for i = 1:nf  
             input_folder = [main_dir,'/',folders{i},'/'];
             S = load([input_folder,'S.mat']);
             dates_all = [dates_all; S.stackinfo.dates_num];
             
         end
        
         S_sim = S;
         S_sim.dates = dates_all;
         S_sim.win = S.win;
         S_sim.options = S.options;
         
         
         if strcmp(S.options.sim_model,'waterlevel')
            defo_model = jihg_defo_sim_wl(S_sim);
         elseif strcmp(S.options.sim_model,'extensometer')
            defo_model = jihg_defo_sim_ext(S_sim);
         elseif strcmp(S.options.sim_model,'CRFS')
             defo_model = jihg_defo_sim_CRFS(S_sim);
         else
             error('provide correct sim model')
         end
        
        for i = 1:nf    
            input_folder = [main_dir,'/',folders{i},'/'];
            fprintf(['\n##### Simulating ',folders{i},'...\n']);
            S = load([input_folder,'S.mat']);
            jihg_steps_sim(S,defo_model);
            fprintf(['### DONE simulating ',folders{i},', elapsed time: %0.2f s. \n'],toc(ttic));
        end
        
    end
    
    fprintf('#### Starting joint-estimation. #### \n')
    for i = 1:nf
        % Read output from the different stacks and store them together.
        fprintf('# [%s] # \n' ,folders{i})
        input_folder = folders{i}; %[main_dir,'/',folders{i},'/'];
        S = load([input_folder,'S.mat']);
        S.sim_flag = sim_flag;
        cpxcoh_output = load([input_folder,'jihg_cpxcoh_output.mat'],'gamma_pta');
        
        % check which output to load
        
        if sim_flag == 1
            output_tsr = load([input_folder,'Simulation/jihg_est_tsr_sim_output']);
        else
            output_tsr = load([input_folder,'jihg_est_tsr_output.mat']);
        end

    
    %% estimate model parameters
        win_model = ones(S.win.n,1); win_model(S.win.win_f == 3) = 2;
            % temporal unwrap is done to correct possible unwrapping errors in,
            % which might still be present after the sbas+ step. This step is
            % not done on windows with pixels labeled as polygon.       
        if sim_flag ==1
            output_t_unw.est_param = output_tsr.est_param_tsr;
            output_t_unw.design_matrix = output_tsr.design_matrix;
        else
            
            fprintf('Correcting unwrapping errors in urban areas ... \n');
            [S,output_t_unw]      = jihg_temporal_unwrap(S,output_tsr); 
            fprintf('Finished - [%0.2f s]  \n',toc(ttic));
        end
        % estimate the scaling factor of the provided deformation model for
        % windows with pixels labeled as polygon.
        fprintf('Fitting first order model in rural area... \n');
        [S,output_model]      = jihg_poly_model_v2(S,output_tsr);   
        % results in 2 functional models, which we are kept seperately.
        % This is done, because of uncertainties in joint estimation of
        % linear trend and deformation model in the RCR step (see later).
        fprintf('Finished - [%0.2f s]  \n',toc(ttic));

        EP1             = output_t_unw.est_param;
        EP2             = output_model.est_param;        
        DM1             = output_t_unw.design_matrix;
        peat_regressor  = output_model.regressors;         
        
        output = output_tsr;
        output.win = S.win;
        output.gamma_pta = cpxcoh_output.gamma_pta;
        output.EP1 = EP1;
        output.EP2 = EP2;
        output.DM1 = DM1;
        output.regressors = peat_regressor;
        output.win_model = win_model;
        output.los2vert  =  1./cosd(S.win.inc);
        output.tsr_unw_rev = output_model.tsr_unw_rev;   
        joint.(['stack',num2str(i)]) =  output;
    end
    
    %% Check consistency between stacks
    % Currently excluding a window if not consistent across stacks. This could be
    % changed. 
    
    gamma_pta = zeros(S.win.n,nf);
    win_f_all = zeros(S.win.n,nf);
    win_nshp = zeros(S.win.n,nf);
    los2vert = zeros(S.win.n,nf);
    for i = 1:nf        
        gamma_pta(:,i) = joint.(['stack',num2str(i)]).gamma_pta;
        win_f_all(:,i) = joint.(['stack',num2str(i)]).win.win_f;
        win_nshp(:,i)  = joint.(['stack',num2str(i)]).win.nshp;
        los2vert(:,i)  = joint.(['stack',num2str(i)]).los2vert;
    end
    
    win_f_new = win_f_all(:,1);
%     win_f_new(~all(win_f_all == win_f_all(:,1),2)) = -1; %PC: removed
%     this line, it is not in my opinion reasonable to expect the different
%     viewing geometries to by exactly the same in all aspects 
    

    
    %% average parameters
    % keeping models seperated for possible comparison between default and
    % used model. 
    npar1 = size(DM1,2);
    EP1_avg = zeros(S.win.n,npar1);
    for j = 1:npar1
        est_param=zeros(S.win.n,nf);
        for i = 1:nf        
            est_param(:,i) = joint.(['stack',num2str(i)]).EP1(:,j);
        end 
        
        % average weighted by gamma_pta
        weights = gamma_pta./sum(gamma_pta,2);
        EP1_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical      
    end
    
    % set windows values to 0 for clarity
    if sim_flag~= 1
        EP1_avg(win_f_new < 0,:) = 0;
        EP1_avg(win_f_new == 3,:)  = 0;  
    end
    
    
    % now model for polygons
    npar2 = 2; % needs hardcoding right now, because of weird new spatially varying regressors
    EP2_avg = zeros(S.win.n,npar1);
    for j = 1:npar2
        est_param=zeros(S.win.n,nf);
        for i = 1:nf        
            est_param(:,i) = joint.(['stack',num2str(i)]).EP2(:,j);
        end 
        
        % average weighted by gamma_pta
        weights = gamma_pta./sum(gamma_pta,2);
        EP2_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical            
    end
    
    % set windows values to 0 for clarity
    if sim_flag~= 1
        EP2_avg(win_f_new < 0,:) = 0;
        EP2_avg(win_f_new == 1,:)  = 0;
        EP2_avg(win_f_new == 2,:)  = 0;
    end
    
    
    %% Remove-compute-restore based on combined estimate of parameters
    
    % Need to do it per stack, as the stacks are independent
    
    % only 2 models being used. 
    win_f_iter = win_f_new;
    n_iter = 2;
    fprintf('#### RCR #### \n')    
    fprintf('%i iterations \n',n_iter)
    for k = 1:n_iter
    %% number of iterations
        iter_step = k;
        
        for i = 1:nf
            fprintf('## [%s - iteration %i] ## \n' ,folders{i},k)
            
            % PC: ADDED
            input_folder = folders{i}; 
            
            %% per stack            
%             input_folder = [main_dir,'/',folders{i},'/'];
            S = load([folders{i} '/' 'S.mat']);
%             S = load([input_folder,'S.mat']);
            S.sim_flag = sim_flag;
            % input data

            if sim_flag == 1
                input = load([input_folder,'Simulation/jihg_est_tsr_sim_output']);
                joint_rcr.(['stack',num2str(i)]).tsr_unw_sbas = input.tsr_unw;
                
                % new_method
                input.tsr_unw = joint.(['stack',num2str(i)]).tsr_unw_rev;
            else
                input = load([input_folder,'jihg_est_tsr_output.mat']);
                % new_method
                input.tsr_unw = joint.(['stack',num2str(i)]).tsr_unw_rev;
                
            end
            
            % use TSR from previous RCR step instead.
            if k > 1
                input.tsr_unw = joint_rcr.(['stack',num2str(i)]).tsr_unw;
            end
            
            input.DM1 = joint.(['stack',num2str(i)]).DM1;
            input.regressors = joint.(['stack',num2str(i)]).regressors;            
            
            input.EP1 = joint.(['stack',num2str(i)]).EP1;
            input.EP2 = joint.(['stack',num2str(i)]).EP2;

            
            input.EP1_avg = EP1_avg./los2vert(:,i); % back to LOS
            input.EP2_avg = EP2_avg./los2vert(:,i);
            
            input.iter_step = iter_step;
            input.gamma_pta = gamma_pta(:,i);
            input.win_f_joint = win_f_iter;
            
            input.iter_step = iter_step;
            
            %% function
            fprintf('Remove-compute-restore step... \n');
            [S,output_rcr] = jihg_rcr_v2(S,input); 
            fprintf('Finished - [%0.2f s]  \n',toc(ttic));
            %%
            output_rcr.ifgs_coh              =  joint.(['stack',num2str(i)]).tsr_coh;
            joint_rcr.(['stack',num2str(i)]) =  output_rcr;
            joint_rcr.(['stack',num2str(i)]).CRB = joint.(['stack',num2str(i)]).CRB_sm;   
            joint_rcr.(['stack',num2str(i)]).CRB_dc = joint.(['stack',num2str(i)]).CRB_dc; 
        end
        
                    %% average parameters
        npar1 = size(DM1,2);
        EP1_avg = zeros(S.win.n,npar1);
        for j = 1:npar1
            est_param=zeros(S.win.n,nf);
            for i = 1:nf        
                est_param(:,i) = joint_rcr.(['stack',num2str(i)]).EP1(:,j);
            end 

            % average weighted by gamma_pta
            weights = gamma_pta./sum(gamma_pta,2);
            EP1_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical      
        end

        % set windows values to 0 for clarity
        if sim_flag~= 1
            EP1_avg(win_f_new < 0,:) = 0;
            EP1_avg(win_f_new == 3,:)  = 0;  
        end
  


        % now model for polygons
        npar2 = 2; % hardcoded
        EP2_avg = zeros(S.win.n,npar1);
        for j = 1:npar2
            est_param=zeros(S.win.n,nf);
            for i = 1:nf        
                est_param(:,i) = joint_rcr.(['stack',num2str(i)]).EP2(:,j);
            end 
            
            % any positive shrinkage values?
            is_pos = false(size(est_param));
            if j == 2
                is_pos = est_param > 0;
                 
            end
            
            est_param(is_pos) = 0;

            % average weighted by gamma_pta
            weights = gamma_pta./sum(gamma_pta,2);
            weights(is_pos) = 0;
            weights = weights./sum(weights,2); % make sure they add up to 1
            EP2_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical   
            two_or_more = sum(is_pos) > 1; 
%             if two_or_more% if two or more with positive amplitude
%                 EP2_avg(:,j) = 1;  % gets removed  later on (yes, this is rather lazy programming)
%             end
        end

        % set windows values to 0 for clarity
    if sim_flag~= 1
        EP2_avg(win_f_new < 0,:) = 0;
        EP2_avg(win_f_new == 1,:)  = 0;
        EP2_avg(win_f_new == 2,:)  = 0;
        
        % if entire window is positive, then model is not correct, so to
        % -1 for now
        
        is_pos = EP2_avg(:,2) > 0;        
%         win_f_iter(is_pos) = -1; PC: commented out (debugging)   

    end
        



                
        
        
    end
    
    % function to generate final output
    
    
    input = joint_rcr;
    input.EP1_avg = EP1_avg;
    input.EP2_avg = EP2_avg;
    input.win_f_final = win_f_iter;
    input.gamma_pta = gamma_pta;
    input.los2vert = los2vert;
    for i = 1:nf
        input.(['stack',num2str(i)]).tsr_unw_sbas = joint.(['stack',num2str(i)]).tsr_unw_rev;
    end
    
    clearvars -except S input ttic
    fprintf('Generating output... \n');
    
    %PC: this function was an impossible mess so I deleted all the junk
    %that wasnt doing anything
    %jihg_create_joint_output_v2(S,input); 
    jihg_create_joint_output_v3(S,input);
    
    fprintf('Finished. - [%0.2f s]  \n',toc(ttic));
    
    
    
end