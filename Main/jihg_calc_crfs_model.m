function [TT] = jihg_calc_crfs_model(S)
    
    output_path = S.options.output_path;
    input_path = S.options.input_path;
    fname_wd = S.options.fname_wd;
    
    %PC: changed interval type to closed
    time_range = timerange(datetime(S.options.start_date,'InputFormat','yyyyMMdd'),datetime(S.options.end_date,'InputFormat','yyyyMMdd'),'closed');

    if ~(exist(fullfile(output_path,'CRFS_model.mat'),'file')==2)
        T           = jihg_read_weatherdata([input_path,fname_wd],{'RH','EV24'});
%         output_rain = jihg_read_rainfall_data([input_path,'neerslagstation_data.txt']);
%         T = T(time_period,:);
%         t1 = T.Time(time_range);
%         RF1 = T.RH(time_range);
%         t2 = output_rain.T_rain.Time(time_range);
%         RF2 = output_rain.T_rain{time_range,1};
        
%         figure(1); bar(t1,RF1); hold on; bar(t2,-RF2);
        Time = T.Time;
        RH = T.('RH')/10;
        EV24 = T.('EV24')/10;
        
        RFS = RH - EV24; % units from 0.1 mm to mm;
        RFS(isnan(RFS)) = 0;
        CRFS = cumsum(RFS);

        model = zeros(length(RFS),1);
        model(1) = RFS(1);
        ref = 0;
        for i = 1: length(RFS)-1
            if month(Time(i)) == 1
                ref = 0;
            end
            new_val = model(i) + RFS(i+1);

            if new_val > ref
                new_val = 0;
            end

            model(i+1) = new_val;

        end  
        

        
        RFS = RH*1.5 - EV24; % units from 0.1 mm to mm;
        RFS(isnan(RFS)) = 0;
        CRFS = cumsum(RFS);

        model2 = zeros(length(RFS),1);
        model2(1) = RFS(1);
        ref = 0;
        for i = 1: length(RFS)-1
            if month(Time(i)) == 1
                ref = 0;
            end
            new_val = model2(i) + RFS(i+1);

            if new_val > ref
                new_val = 0;
            end

            model2(i+1) = new_val;

        end
        
        T_temp = timetable(Time,RH,EV24,CRFS,model,model2); 
        T_temp = T_temp(time_range,:);
        
        
        
        model_orig = T_temp.model;
        max_mod = max(model_orig);
        model1 = model_orig - max_mod;
        min_mod = min(model1);
        model = model1./abs(min_mod);
        
        model_orig2 = T_temp.model2;
        max_mod2 = max(model_orig2);
        model2 = model_orig2 - max_mod2;
        min_mod2 = min(model2);
        model2 = model2./abs(min_mod2);
        
        T_temp.model = model;
        T_temp.model2 = model2;
        T_temp.model_orig = model_orig;
        
        
        TT = T_temp;     
     
        save([output_path,'CRFS_model.mat'],'TT');
    else
        output=load([output_path,'CRFS_model.mat']);
        TT = output.TT;
    end
    

    
    

    
end