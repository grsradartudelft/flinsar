function [S,output] = jihg_cpxcoh_sim(S,input,defo_model)
%JIHG_CPXCOH computes (all) the multi-looked interferograms

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.    output_path     = [S.options.output_path,'Simulation/']; 
    output_path     = [S.options.output_path,'Simulation/']; 
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    %% Otherwise run function.
        % read input
        
        method          = S.options.pl_method;
        CRB             = S.options.CRB;
        nwin            = S.win.n;
        path_slcs       = S.stackinfo.path_slcs;
        nlines          = S.stackinfo.nlines;
        npixels         = S.stackinfo.npixels;
        nslcs           = S.stackinfo.nslcs;
        win_shp         = input.shps.win_shp;
        win_f           = input.shps.win_f;
        nshp            = input.shps.nshp;
        patches         = input.patches.patches;
        patch_win_ixs   = input.patches.patch_win_ixs;
        

        %% MAIN

        npatch          = size(patches,1);
        nifgs           = nslcs-1;
        los2vert  =  1./cosd(S.win.inc); %needed to project the vertical signal to the right LOS
        
        % preallocate arrays
        ifgs_sm         = zeros(nwin,nifgs);
        ifgs_dc         = zeros(nwin,nifgs);
        CRB_sm          = Inf(nwin,nifgs);
        CRB_dc          = Inf(nwin,nifgs);
        coh             = zeros(nwin,nifgs);
        gamma_pta       = zeros(nwin,1);       
        bad_epochs      = zeros(nwin,nslcs);
        ph_true         = zeros(nwin,nslcs);
        


        tic
        fprintf('Processing %i patches \n',npatch)
        
        
        %% model
        
        dates_num = S.stackinfo.dates_num;
        dates = datetime(datestr(dates_num(1:end)));
        

        
%         T = jihg_calc_crfs_model(S);
%         T2 = T(dates,:);
% 
%         model_intra = T2.('model');
%         model_intra = model_intra/max(abs(model_intra))*7.5; % on average 7.5 cm max shrinkage
%         tmp = cumsum(model_intra);
%         model_secular = tmp/max(abs(tmp))*(dates_num(end) - dates_num(1))/365*1; %cm
%         
        load([S.options.output_path,'jihg_unwrap_output.mat']);
        
%         ph_model = (model_intra + model_secular)*4*pi/0.0556/100; % convert from cm
        
        if S.options.sim_signal == 1   
            [~,d_ix] = ismember(dates_num,defo_model.dates);
            ph_model = (defo_model.defo_model(:,d_ix))*4*pi/0.0556/100./los2vert; % convert from cm
        else
            ph_model = zeros(size(defo_model));            
        end
        
        % spatial variation of peat, with a decorrelation time of 100 days.
%         pp = peaks(S.win.npixels);        
%         lix = round(linspace(1,S.win.npixels,S.win.nlines));
%         peat_var = pp(lix,:)/max(abs(pp(:)))*0.5+1;
%         t = dates_num - dates_num(1);
%         dt_full = t- t';
%         decorr_t = 100;
%         corr_m = exp(-abs(dt_full)/decorr_t);
%         [L] = chol(corr_m,'lower');
%         peat_var = randn(S.win.nlines,S.win.npixels);
%         peat_var = peat_var./max(abs(peat_var))*0.5+1;

        if S.options.ps_flag == 1
            ps_name = S.options.ps_software;
            if strcmp(ps_name,'depsi')
                [S,output_atmo] = jihg_depsi_atmosphere(S);
            end
            win_atmos_sm = output_atmo.win_atmos_sm;
            master_atmos = output_atmo.win_m_atmos;
            slc_atmos = [master_atmos master_atmos - win_atmos_sm];
            aps     = slc_atmos;
        end

            
%                 aps = zeros(nwin,nslcs);
% 
%                 for i = 1:nslcs
%                     atmo = sim_atmo_flo(S.win.nlines,S.win.npixels,200,200,0.2,'n');
%                     aps(:,i) = 4*pi/0.0556*atmo(:);
%         %             figure(1); imagesc(atmo)
%                 end
%             
% %         else
% %             aps = zeros(size(defo_model));  
%         end
        
        clearvars output_atmo win_atmos_sm master_atmos _slc_atmos model_intra model_secular corr_m
        
        if S.options.sim_polygon == 1
            
            poly_output = load([S.options.output_path,'jihg_poly_sel_output']);
            pix_poly_id = poly_output.pix_poly_id;
            h_rnd = defo_model.h_rnd_poly(:,d_ix)*4*pi/0.0556/100./mean(los2vert);
            
        end
        
        in_p = S.win.win_f == 3;
        
        count = 0;
        for p = 1:npatch
        %% Loop over patches
        % JIHG_CPXCOH requires the full coherence matrix. 
        % Often, the full stack of SLCS is too large to load fully into
        % memory. Hence, the stack has been split into patches, which are
        % processed individually.        
        
            % Load corners of patch
            l0 = patches(p,1);
            lN = patches(p,2);
            p0 = patches(p,3);
            pN = patches(p,4);            
        
            % size of patch
            npl         = lN - l0 + 1;            
            npp         = pN - p0 + 1;            
            slc_stack   = complex(zeros(npl*npp,nslcs));
            
            for j = 1:nslcs
            %% Load part of SLC stack
                slc = freadbk_quiet(path_slcs{j},nlines,'cpxfloat32',l0,lN,p0,pN);
                slc_stack(:,j) = slc(:);
                if mod(j,round(nslcs/10)) == 0
                    fprintf('Finished loading %i of %i slcs. [%i%% - %0.2f s] \n',j,nslcs,round(j/nslcs*100),toc)
                end
            end         
            
            patch_wins = patch_win_ixs{p};
            pf = 1;
            
            for i = patch_wins'
            %% Loop over windows in patches 
            
                ind_wrt_slc     = win_shp{i};
                
                [ir,ic]         = ind2sub([nlines,npixels],ind_wrt_slc);
                ind_wrt_patch   = sub2ind([npl,npp],ir-l0+1,ic-p0+1);
                
                cpxval                    = slc_stack(ind_wrt_patch,:);
                bad_epoch_chk             = sum(abs(cpxval),1) == 0 | any(isnan(cpxval),1); % Check for epoch with only zeros
                cpxval(:,bad_epoch_chk)   = [];
                
                % win_f: 1 -> ps, 2 -> poly with too few SHPs (all pixels
                % selected) 3 -> poly with SHPs, 0 -> rest, -1 -> empty
                % window
                if win_f(i) == 1
                    
                    if S.options.sim_ps == 1                      
                        
                        
                        
                        cpxval_sim = exp(1j*aps(i,~bad_epoch_chk));
                        if S.options.sim_noise == 1
                            coh_mx=0.99*ones(size(cpxval,2))+(1-0.99)*eye(size(cpxval,2));

                            [V,D] = eig(coh_mx);
                            Xc =(V*sqrt(D))*randn(size(cpxval))';
                            Xs =(V*sqrt(D))*randn(size(cpxval))';
                            cpx_n=(Xc+1j*Xs)';
                            cpxval_sim = cpxval_sim.*cpx_n; % zero deformation
                        end


                        
                        
                        cpx_ps_val       = exp(1j*angle(cpxval_sim));
                        cpx_ps           = cpx_ps_val.'*conj(cpx_ps_val);
                        cpx_ps           = cpx_ps.';                        
                        
                    else

                        cpx_ps_val       = exp(1j*angle(cpxval));
                        cpx_ps           = cpx_ps_val.'*conj(cpx_ps_val);
                        cpx_ps           = cpx_ps.';
                    end
                    
%                     cpx_sum = cpxval.'*conj(cpxval);
%                     abs_sum = sum(abs(cpxval).^2);
%                     norm    = sqrt(abs_sum'*abs_sum);
%                     cpxcoh  = cpx_sum.'./norm; % transposed to make columns master - slave.                  
%                     
% 
% 
%                     
%                     test(:,1) = angle(cpx_ps(2:end,1));
%                     test(:,2) = angle(cpxcoh(2:end,1));

                    
                    
                    
                    ifgs_sm(i,~bad_epoch_chk(2:end))   = angle(cpx_ps(2:end,1));
                    ifgs_dc(i,~bad_epoch_chk(2:end))   = angle(diag(cpx_ps,-1));
                    coh(i,~bad_epoch_chk(2:end))       = 0.99;
                    gamma_pta(i)                       = 0.99;
                    bad_epochs(i,:)                    = bad_epoch_chk;
                    
                elseif win_f(i) == 2 || win_f(i) == 3  ||  win_f(i) == 0

                    
                    % select pixel stacks in window
                    cpxval                    = slc_stack(ind_wrt_patch,:);
                    bad_epoch_chk             = sum(abs(cpxval),1) == 0 | any(isnan(cpxval),1); % Check for epoch with only zeros or nans
                    cpxval(:,bad_epoch_chk)   = [];
                    
                    % calculate full complex coherence
                    cpx_sum = cpxval.'*conj(cpxval);
                    abs_sum = sum(abs(cpxval).^2);
                    norm    = sqrt(abs_sum'*abs_sum);
                    cpxcoh  = cpx_sum.'./norm; % transposed to make columns master - slave.
                    nslcs_part = size(cpxcoh,2);
                    
                    cpxcoh_old = cpxcoh;
                    
                    
                    
% %                     coh_mx=abs(0.3*randn(nslcs));
% %                     coh_mx=(triu(coh_mx,1))'+triu(coh_mx);
% %                     coh_mx(coh_mx>=0.65)=0.65;
% %                     %coh_mx=0.6*coh_mx;
% %                     coh_mx=coh_mx-diag(diag(coh_mx))+eye(nslcs);
                    

%                         ph_true(i,:) = mean(peat_var(win_shp{i}).*ph_model',1);
%                         cpx_s = exp(1j*peat_var(win_shp{i}).*ph_model');
%                         dh_corr = randn(nslcs,1)*0.5; 
%                         h_rnd = L*dh_corr*4*pi/0.0556/100; % variation is 1 cm.
                        
                        
%                         ph_true(i,:) = (peat_var(i).*ph_model+h_rnd);
%                         figure(1); plot(ph_true(i,:)); hold on; plot(peat_var(i).*ph_model); hold off;
                        
                        cpxval_sim = exp(1j*aps(i,~bad_epoch_chk)); % starting with depsi atmoshere;
                        
                        
                        if S.options.sim_noise == 1 
                        
                            [V,D] = eig(abs(cpxcoh));
                            Xc =(V*sqrt(D))*randn(size(cpxval))';
                            Xs =(V*sqrt(D))*randn(size(cpxval))';
                            cpx_n=(Xc+1j*Xs)';
                            cpxval_sim = cpxval_sim.*cpx_n;
                        end
                        
                        if S.options.sim_signal ==1
                            if in_p(i)
                                
                                if S.options.sim_polygon == 1
                                    ids = pix_poly_id(ind_wrt_slc);
                                    hs_rnd = h_rnd(ids,:);
                                    defo_ph = repmat(ph_model(i,:),length(ids),1) + hs_rnd;
                                    ph_model(i,:) = mean(defo_ph);
                                    cpx_s = exp(1j*defo_ph(:,~bad_epoch_chk));
                                    cpxval_sim = cpxval_sim.*cpx_s;
                                    
%                                     if i == 1
%                                         figure; plot(hs_rnd');
%                                     end
                                                                                                          
                                else

                                    cpx_s = exp(1j*ph_model(i,~bad_epoch_chk));
                                    cpxval_sim = cpxval_sim.*cpx_s;
                                end
                                
                            else
                                cpx_s = exp(1j*ph_model(i,~bad_epoch_chk)*0); % perhaps other model in the future
                                cpxval_sim = cpxval_sim.*cpx_s;
                            end
                        end
                        
                    % adding DePSI atmosphere, so that it is subtracted later.                     
                        
 
                        cpx_sum = cpxval_sim.'*conj(cpxval_sim);
                        abs_sum = sum(abs(cpxval_sim).^2);
                        norm    = sqrt(abs_sum'*abs_sum);
                        cpxcoh  = cpx_sum.'./norm;                         

                        
                        
                    
                    
                    
                    % phase-linking method
                    if strcmp(method,'EMI')
                        [cpxcoh_opt] = jihg_ph_est_emi(cpxcoh);              
                    elseif strcmp(method,'PTA')
                        [cpxcoh_opt] = jihg_ph_est_pta(cpxcoh);        
                    elseif strcmp(method,'EVD')
                        [cpxcoh_opt] = jihg_ph_est_evd(cpxcoh);
                    end
                    
                    % ensemble temporal coherence
                    uptri           = find(triu(ones(size(cpxcoh)),1)); 
                    gamma_pta(i)    = 2/(nslcs_part^2-nslcs_part)*real(nansum(exp(1i*...
                        (angle(cpxcoh_opt(uptri)) - angle(cpxcoh(uptri))))));
                    
                    % check if something went wrong 
                    if gamma_pta(i) < 0 || gamma_pta(i) > 1 % (can't happen)
                        % try, might be problem with inversion of coh
                        [cpxcoh_opt] = jihg_ph_est_evd(cpxcoh);
                        
                        gamma_pta(i)    = 2/(nslcs_part^2-nslcs_part)*real(nansum(exp(1i*...
                            (angle(cpxcoh_opt(uptri)) - angle(cpxcoh(uptri))))));
                        if gamma_pta(i) < 0 || gamma_pta(i) > 1
                            cpxcoh_opt = cpxcoh_opt*0;
                            gamma_pta(i) = 0;
                        end
                    end
                    
                    % Cramer-Rao Bound Covariance Matrix 

                    CRBQ = jihg_CRBQ(cpxcoh,nshp(i));
                    
                    % propagate to DC
                    % A matrix is only correct if master is slc 1.
                    A = eye(size(CRBQ));
                    for n = 1:size(A,2)-1
                        A(n+1,n) = -1;
                    end
                    
                    CRB_sm(i,~bad_epoch_chk(2:end)) = diag(CRBQ);
                    CRB_dc(i,~bad_epoch_chk(2:end)) = diag(A*CRBQ*A');
                    if strcmp(CRB,'full')
                        
                        if ~(exist(fullfile([output_path,'/CRBQs/']),'dir') == 7)
                            mkdir([output_path,'/CRBQs/'])
                        end
                        save_name = ['CRBQ_win',num2str(i)];
                        save([output_path,'/CRBQs/',save_name],'CRBQ');
                        
                    end
                    
                    % Store phase-linked cpxcoh correctly
                    % Perhaps not store both for very large data.

                    ifgs_sm(i,~bad_epoch_chk(2:end))   = angle(cpxcoh_opt(2:end,1));
                    ifgs_dc(i,~bad_epoch_chk(2:end))   = angle(diag(cpxcoh_opt,-1));
                    if S.options.sim_ps ==0
                        if win_f(i) == 2 || win_f(i) == 3  ||  win_f(i) == 0
                            ifgs_dc(i,~bad_epoch_chk(2:end)) = angle(diag(cpxcoh_opt,-1).*exp(1j*uw_ref(~bad_epoch_chk(2:end))));
                        end
                    end
                    coh(i,~bad_epoch_chk(2:end))       = abs(diag(cpxcoh,-1)); 
                    bad_epochs(i,:)                    = bad_epoch_chk;
%                     figure(10);
%                     hold on
%                     if in_p(i)
%                         plot(angle(cpxcoh_opt(2:end,1)));
%                     end
%                     hold off
                    
                    nwin_patch = length(patch_wins); % Number of windows varies per patch.
                    prnt_ind   = patch_wins(round(linspace(1,nwin_patch,5))); 
                    if ismember(i,prnt_ind)
                        fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',round(pf*nwin_patch/5),nwin_patch,pf*20,toc)
                        pf = pf+1;
                    end
                end
                
        %         test = angle([cpxcoh_opt(:,1) cpxcoh_opt1(:,1) cpxcoh_opt2(:,1) cpxcoh_opt3(:,1) cpxcoh(:,1)]);
            end
            clearvars slc_stack
            fprintf('Finished patch %i of %i. [%0.2f s] \n',p,npatch,toc)
        end
        
        ifg_ix_sm = [ones(nifgs,1) (2:nslcs)'];
        ifg_ix_dc = [(1:nslcs-1)' (2:nslcs)'];
        
        %% Output
        output.ifgs_dc      = ifgs_dc;
        output.ifgs_sm      = ifgs_sm;
        output.coh          = coh;
        output.gamma_pta    = gamma_pta;
        output.nifgs        = nifgs;
        output.ifg_ix_sm    = ifg_ix_sm;
        output.ifg_ix_dc    = ifg_ix_dc;
        output.CRB_sm       = CRB_sm;
        output.CRB_dc       = CRB_dc;
        output.truth        = ph_model;
        output.aps          = aps;
        
        save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.stackinfo.nifgs = output.nifgs;
    S.stackinfo.ifg_ix_dc = output.ifg_ix_dc;
    S.stackinfo.ifg_ix_sm = output.ifg_ix_sm;
    S.options.CRB_path = [output_path,'/CRBQs/'];
    clearvars -except S output
    
end