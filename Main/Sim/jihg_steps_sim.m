function jihg_steps_sim(S,defo_model)
fprintf('#### Creating simulated interferograms. \n');
    t=tic;
    Sim = S;
    Sim.options.output_path     = [S.options.output_path,'Simulation/'];

    [Sim]           = jihg_init(Sim);
    [S,output]      = jihg_allot_win(S);
    [S,output]      = jihg_cpxcoh_sim(S,output,defo_model);
    [S,output]      = jihg_unwrap_sim(S,output);
    [S,output]      = jihg_est_tsr_sim(S,output);
                    save([Sim.options.output_path,'S'],'-struct','S');
                    fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));
end