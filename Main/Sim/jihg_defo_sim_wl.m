function [output] = jihg_defo_sim_wl(S)




    output_path = [pwd,'/'];
    input_path = S.options.input_path;
    fname_wl = S.options.fname_wl;
    fname_wlxy = S.options.fname_wlxy;
    
    n_sets = length(fname_wl);
    
    
    if ~(exist(fullfile(output_path,'defo_sim_wl.mat'),'file')==2)
        
        data = timetable;
        xy_data = table;
        for i = 1:n_sets
            TT_read = readtimetable([input_path,fname_wl{i}]);
            T_read = readtable([input_path,fname_wlxy{i}]);
            xy_data = [xy_data; T_read];
            data = outerjoin(data,TT_read);
        end

        dates_num = sort(S.dates);
        dates = datetime(datestr(dates_num(1:end)));

        radar_XY = load([S.options.output_path,'jihg_aw_geocoords_output.mat'],'win_rdx','win_rdy');

        waterlevel1 = data(dates,:);
        waterlevel = waterlevel1{:,:};
        waterlevel(waterlevel > 0) = NaN;
        
        diff_wl = diff([waterlevel(1,:);waterlevel]);
        weird_jump = diff_wl < -0.5;
        waterlevel(weird_jump) = nan;
        
        diff_wl = diff([zeros(size(waterlevel(1,:)));waterlevel]);
        weird_jump = diff_wl > 0.5;
        waterlevel(weird_jump) = nan;
        

        X = xy_data.X;
        Y = xy_data.Y;

        Xq = radar_XY.win_rdx;
        Yq = radar_XY.win_rdy;

        n_dt = size(waterlevel,1);

        query_data = zeros(n_dt,S.win.n);
        tic
        for i = 1:n_dt
            sample_data = waterlevel(i,:); 
            % nancheck
            nans = isnan(sample_data);
            warning off;
            F = scatteredInterpolant(X(~nans),Y(~nans),sample_data(~nans)','natural','nearest');

            query_data(i,:) = F(Xq,Yq);
%             warning on;
%             figure(1)
%             scatter(Xq,Yq,10,query_data(i,:),'filled');
%             caxis([-1.5,0])
%             hold on
%             
%             scatter(X(~nans),Y(~nans),50,sample_data(~nans),'filled','h','MarkerEdgeColor',[0,0,0])
%             hold off

            if mod(i,round(n_dt/10)) == 0
                fprintf('Finished interpolating waterlevel for %i of %i epochs. [%i%% - %0.2f s] \n',i,n_dt,round(i/n_dt*100),toc)
            end

        end
        
        
        max_wl = max(query_data);
        waterlevel3 = query_data - max_wl;
        min_wl_all = min(waterlevel3);
        sort_min = sort(min_wl_all);
        min_wl = sort_min(round(0.1*S.win.n));
        model_intra = waterlevel3./abs(min_wl)*S.options.sim_max_shrink;
        
        tmp = cumsum(model_intra,1);
        model_secular = tmp./max(abs(tmp),[],1)*(dates_num(end) - dates_num(1))/365*S.options.sim_sec_sub; %cm


        defo_model =  model_intra + model_secular;
        output.defo_model =  defo_model';
        output.dates      = dates_num;
        
        
        save([output_path,'defo_sim_wl.mat'],'-struct','output')
     else
        output = load([output_path,'defo_sim_wl.mat']);
    end

end