function [output] = jihg_defo_sim_ext(S)

    output_path = [pwd,'/'];
    dates_num = sort(S.dates);
    dates = datetime(datestr(dates_num(1:end)));

    T_ext = jihg_read_extensometer(S);
    T2 = T_ext(dates,:);

    model_intra = T2.data;
    
    mmax = max(model_intra);
    temp = model_intra - mmax;
    mmin = min(temp);
    model_intra = temp./abs(mmin)*S.options.sim_max_shrink; 

    tmp = cumsum(model_intra);
    model_secular = tmp/max(abs(tmp))*(dates_num(end) - dates_num(1))/365*S.options.sim_sec_sub;

    defo_model =  model_intra + model_secular;
    defo_model = repmat(defo_model,1,S.win.n);    
    
    shape_path      = S.options.shape_path;
    decorr_t = S.options.sim_poly_decorr_t; % days
    
    P               = shaperead(shape_path);
    n_poly           = length(P);
    t = dates_num;
    N = length(t);
    h_rnd = zeros(n_poly,N);
    
    if S.options.sim_polygon == 1
            dt_full = t' - t;
            corr_m = exp(-abs(dt_full)/decorr_t);
            [L] = chol(corr_m,'lower');
            
            for i = 1:n_poly                
                dh_corr = randn(N,1); 
                h_rnd(i,:) = L*dh_corr; % random height difference with std of 10 cm
            end
            
            
    end
    
    output.defo_model =  defo_model';
    output.dates      = dates_num;
    output.h_rnd_poly = h_rnd*S.options.sim_max_shrink*S.options.sim_poly_var;

    save([output_path,'defo_sim_ext.mat'],'-struct','output')       

end