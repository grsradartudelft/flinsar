function [output] = jihg_defo_sim_CRFS(S)
        

    output_path = [pwd,'/'];
    dates_num = sort(S.dates);
    dates = datetime(datestr(dates_num(1:end)));

    T = jihg_calc_crfs_model(S);
    T2 = T(dates,:);       

    model_intra = T2.('model');
    model_intra = model_intra/max(abs(model_intra))*S.options.sim_max_shrink;

    model_intra = smooth(model_intra,10);

    tmp = cumsum(model_intra);
    model_secular = tmp/max(abs(tmp))*(dates_num(end) - dates_num(1))/365*S.options.sim_sec_sub;

    defo_model =  model_intra + model_secular;

    defo_model = repmat(defo_model,1,S.win.n);

    output.defo_model =  defo_model';
    output.dates      = dates_num;


    save([output_path,'defo_sim_CRFS.mat'],'-struct','output')       

end