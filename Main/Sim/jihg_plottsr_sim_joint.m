function jihg_plottsr_sim_joint(~,~,S,results,truth,win)

    [x_get,y_get,~]=ginput(1);
    
    nlines = S.win.nlines;
    npixels = S.win.npixels;
    dates_num = S.stackinfo.dates_num;
    ifg_ix = S.stackinfo.ifg_ix_sm;
    dlat = win.dlat;
    dlon = win.dlon;
    lat_range = win.lat_range;
    lon_range = win.lon_range;    
     tsr_unw = results.tsr_unw;
%     ifgs_coh = results.ifgs_coh;
    
    ix1 = find(lat_range < y_get + dlat/2,1,'last');
    ix2 = find(lon_range < x_get + dlon/2,1,'last');
    ix = sub2ind([nlines,npixels],ix1,ix2);
    
    date_p = datetime(datestr(dates_num(2:end)));
%     date_p = dates_num(2:end);
    tsr_p = -tsr_unw(ix,:)/2/pi;
%     


    DM = [results.DM_final(:,1:2) results.DM_final(:,ix)];
    est = results.EP_final(ix,:);
    est_p = -(DM*est')/2/pi;
    
%     est_p = -smooth(tsr_est(ix,:),5)/2/pi;
    true_p = truth(ix,2:end)/2/pi; true_p = true_p - true_p(1);
%     coh_p =  ifgs_coh(ix,:);
    nshp = S.win.nshp(ix);

    h = figure;
    set(gcf,'position', [158*2 164*2 1200/2 600/2]) 

    plot(date_p,tsr_p,'.k','MarkerSize',15);
    hold on
    plot(date_p,est_p,'r','LineWidth',2);
    plot(date_p,true_p,'b','LineWidth',2);
    ylim1 = ylim;
%     plot(date_p,est_p + 0.5,'--k')
%     plot(date_p,est_p - 0.5,'--k')

%     colormap(jet)
    hold off
    title({[num2str(ix1) ',' num2str(ix2)],num2str(ix),num2str(S.win.win_f(ix))})
    grid on;
    grid minor;
%     datetick
    ylabel('cycles');
    ylim(ylim1);
    legend('obs','est','truth')

    
% %     btn = uicontrol(h,'Style', 'pushbutton', 'String', 'pdf',...
%     'Units','normalized','Position', [0.01 0.01 0.09 0.05],...
%     'Callback', {@jihg_plotpdfpatch,h,date_p,coh_p,tsr_p,nshp});
%     
% %     btn2 = uicontrol(h,'Style', 'pushbutton', 'String', 'reset',...
%     'Units','normalized','Position', [0.11 0.01 0.09 0.05],...
%     'Callback', {@delete_patch,h});
% 
% %     btn = uicontrol(h,'Style', 'pushbutton', 'String', 'pdf2',...
%     'Units','normalized','Position', [0.21 0.01 0.09 0.05],...
%     'Callback', {@jihg_plotpdf,h,date_p,coh_p,tsr_p,nshp});
% 
% %     sld1=uicontrol('Style','slider',...
%     'Min',0,'Max',1,'Value',0.5,'SliderStep',[0.1,0.2],...
%     'Units','normalized','Position',[0.8 0.01 0.1 0.03] ,...
%     'Callback', {@set_transparency,h}); 



% 
%     figure;
%     plot(date_p,coh_p)
%     datetick
%     grid on
    
    number = h.Number;
    txts = findobj('Type','Text');
    scat = findobj('Type','Scat');    
    if ~isempty(txts)
        strings = [txts.String];
        chk = strcmp(strings,num2str(number)); 
        delete(txts(chk));
        delete(scat(chk));
    end
    

    
    figure(100)
    scatter(x_get,y_get,50,'Filled','k','s')
    text(x_get,y_get,num2cell(number),'FontSize',12,'FontWeight','bold');
    figure(number);
        

end

function delete_patch(src,~,h)

t=findobj(h,'Type','Patch');
delete(t)
end

function set_transparency(source,~,h)
    num=source.Value;
    h1=findobj(h,'Type','Patch');
    set(h1,'Alpha','none')
    set(h1,'FaceAlpha',num)
    set(h1,'EdgeAlpha',num)
    
end