function [ aps ] = sim_atmo_flo(nlines,npixels,az_spacing,ra_spacing,scale,info)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


beta=[5/3 8/3 2/3];
regime = zeros(1,3);




grid_size = min([nlines,npixels]);
if rem(grid_size,2) % fracsurfatmo requires even gridsize.
    grid_size = grid_size + 1; 
end

xi  = (1:ra_spacing:npixels*ra_spacing);
yi  = (1:az_spacing:nlines*az_spacing);
x = linspace(1,max(xi(end),yi(end)),grid_size);
y = linspace(1,max(xi(end),yi(end)),grid_size);

area_size = max(xi(end),yi(end));
ratio = 2000/area_size;
if ratio > 1
    ratio = 1;
end

regime(1) = round(99*(1-ratio));
regime(2) = 99;
regime(3) = 100; %noise level

res = fracsurfatmo(grid_size, beta, regime,'n',scale);
    




[X,Y] = meshgrid(x,y);
[Xi,Yi] = meshgrid(xi,yi);
aps =interp2(X,Y,res,Xi,Yi,'linear');

if strcmp(info,'y')
    figure;imagesc(aps);colorbar;title('Atmospheric Phase Screen [m]')
end

end

