function [S,output] = jihg_unwrap_nnb(S,input)
%JIHG_UNWRAP Unwraps interferograms using SNAPHU

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    save_name       = [fun_name,'_output.mat'];
  
     if ~(exist(fullfile(output_path,save_name),'file')==2)
     %% Otherwise run function.
        % Read input

        nifgs           = S.stackinfo.nifgs;
        lambda          = S.options.lambda;
        unwrap_method   = S.options.unwrap_method;
        nwin            = S.win.n;
        win_f           = S.win.win_f;
        nlines          = S.win.nlines;
        npixels         = S.win.npixels;
        ESM_EDC         = S.options.pl_ESM_EDC;
        
        if strcmp(ESM_EDC,'EDC')
            ifgs        = input.ifgs_dc;
        else
            ifgs        = input.ifgs_sm;
        end

        coh             = input.coh; 
        
        
        %% MAIN
        %make snaphu.conf.in
        system('echo INFILE  phase.in > snaphu.conf.in');        
        system('echo INFILE  phase.in > snaphu.conf.in');
        system(['echo LINELENGTH ',num2str(npixels),' >> snaphu.conf.in']);
        system('echo OUTFILE phase.out >> snaphu.conf.in');
        system('echo CORRFILE coherence.in >> snaphu.conf.in');
        system('echo STATCOSTMODE DEFO >> snaphu.conf.in');
        system(['echo INITMETHOD ',unwrap_method,' >> snaphu.conf.in']);
        system('echo INFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo OUTFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo CORRFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo EARTHRADIUS 6378000.0 >> snaphu.conf.in');
        system(['echo LAMBDA ',num2str(lambda/1000),' >> snaphu.conf.in']);
    %     system(['!echo "NCORRLOOKS ' num2str(ncorrlooks) '" >> snaphu.conf.in']);
    
    
        % Currently JIHG_CPXCOH outputs the ESM.
        uw_ph   = zeros(nwin,nifgs);       
        ifg     = ifgs;
        [l_grd,p_grd] = ndgrid(1:nlines,1:npixels);
        
        in_p = win_f ==3;

        
        % provided reference
        [ref_ind] = jihg_unw_setref(S);
        
        tic
        for i = 1:nifgs
    %         unwrap by snaphu
            ifg_in = ifg(:,i);            
            coh_in = coh(:,i);  
            out = coh_in < 0.15 & in_p;
            
            v = ifg_in(~out & in_p);
            x = p_grd(~out & in_p);
            y = l_grd(~out & in_p);
            xq = p_grd(out);
            yq = l_grd(out);
            
            vq = griddata(x,y,v,xq,yq,'nearest');
            
            ifg_in(out) = vq;
            
            fwritebk(reshape(ifg_in,nlines,npixels),'phase.in','float32');
            fwritebk(reshape(coh_in,nlines,npixels),'coherence.in','float32');
    
            system('snaphu -f snaphu.conf.in -s >> snaphu.log');
            
            uw          = freadbk_quiet('phase.out',nlines,'float32');            
            uw_ref      = uw - mean(uw(ref_ind));            
            uw_ph(:,i)  = uw_ref(:);
            
%             figure(11); imagesc(reshape(ifg_in,nlines,npixels))
%             figure(12); imagesc(uw_ref)
%             figure(13); imagesc(reshape(out,nlines,npixels))
%             figure(14); imagesc(reshape(~out & in_p,nlines,npixels))
            
            if mod(i,round(nifgs/10)) == 0
                fprintf('Finished %i of %i ifgs. [%i%% - %0.2f s] \n',i,nifgs,round(i/nifgs*100),toc)
                
            end

        end        
        
        delete('phase.in'); delete('coherence.in'); delete('phase.out'); delete('snaphu.conf.in');
        delete('snaphu.log');
        
        output.ifgs_uw = uw_ph;
        output.ifgs_coh = coh;
        save([output_path,save_name],'-struct','output');
    
    else
        output = load([output_path,save_name]);
    end


end
    