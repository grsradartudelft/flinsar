
function [output] = jihg_read_extensometer(S)
input_path = S.options.input_path;
fname_ext = S.options.fname_ext;
file_name = [input_path,fname_ext];

T = readtable(file_name,'Sheet','bewerking');
T.Properties.VariableNames{1} = 'Time';
% T(1:9,:) = [];

t = datetime(T.Time,'InputFormat','dd-MMM-yy hh:mm:ss a');
data = T{:,12};
% for i = 1:length(data)
%     
%     data(i,1) = str2double(cell2mat(T.Var8(i)))*1000;
% end

TT = timetable(t,data);
TT = retime(TT,'daily','mean');
% tr1 = datetime('01-01-2019','InputFormat','dd-MM-yyyy');
% tr2 =datetime('01-02-2019','InputFormat','dd-MM-yyyy');
% Trange = timerange(tr1,tr2);
% T_Jan = TT(Trange,:);

% mean_jan = retime(T_Jan,'monthly','mean');

output = TT;
output.Properties.VariableNames = {'data'};

end