function [TT] = jihg_calc_crfs_model_v2(S)
    
    output_path = S.options.output_path;
    input_path = S.options.input_path;
    fname_wd = S.options.fname_wd;
    n_stations = length(fname_wd);

    if ~(exist(fullfile(output_path,'CRFS_model.csv'),'file')==2)
        TT = timetable;
        for i = 1:n_stations
            T = jihg_read_weatherdata([input_path,fname_wd{i}],{'RH','EV24'});        

            Time = T.Time;
            RH = T.('RH')/10;
            EV24 = T.('EV24')/10;

            RFS = RH - EV24; % units from 0.1 mm to mm;
            RFS(isnan(RFS)) = 0;
            CRFS = cumsum(RFS);

            model = zeros(length(RFS),1);
            model(1) = RFS(1);
            ref = 0;
            for i = 1: length(RFS)-1
                if month(Time(i)) == 1
                    ref = 0;
                end
                new_val = model(i) + RFS(i+1);

                if new_val > ref
                    new_val = 0;
                end

                model(i+1) = new_val;

            end

            T_temp = timetable(Time,model);
            T =      timetable(Time,RH,EV24,CRFS,model);
            writetimetable(T,[output_path,fname_wd{i}(1:end-4),'-w-CRFS.csv']);
            TT = outerjoin(TT,T_temp);
        end
       
     
        writetimetable(TT,[output_path,'CRFS_model.csv']);
    else
        TT = readtimetable([output_path,'CRFS_model.csv']);
    end
    

    
    

    
end