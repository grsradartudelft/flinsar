function [S,output]= jihg_poly_model_v2(S,input)


%% need to change function to be able to interpolate multiple weatherstations/other sources of data
% How to deal with datasets that do not cover the entire sensing period?
% - We can merge them with other sources. Which is leading then? 
        % Main source + added (lowerlevel) models (Extensometer ->
        % grounwaterlevel -> CRFS. However, how do we determine what we
        % want to interpolate? Extensometer has 2 spots, not really
        % possible to interpolate. Groundwater level larger area, but does
        % that behave similarly on longer distances? What about CRFS?
        % For now lets focus on grounwater level and CRFS
        
        %
    s               = dbstack;
    if length(s) == 1
        jihg_joint_rcr
    end
    fun_name        = s(1).name;
    output_path = S.options.output_path;
    save_name       = [fun_name,'_output.mat'];
    
    if S.sim_flag == 1
        save_name       = ['Simulation/',fun_name,'_sim_output.mat'];
    end
    
%     if  ~(exist(fullfile(output_path,save_name),'file')==2)
        nwin = S.win.n;
        coh = input.tsr_coh;
        CRB = input.CRB_sm;
        tsr = input.tsr_unw;
        ifgs = input.ifg_unw;
        nifgs = S.stackinfo.nifgs;
        method= S.options.model;
        dates_num = S.stackinfo.dates_num;

        dates = datetime(datestr(dates_num(2:end)));
        
        regressors = S.options.model;

        n_models = length(regressors);
        % order of models is order of importance. Model 1 is leading.

        % models in format timetable with columns measurement series. This
        % means that the current CRFS_model output needs to be adapted.

        for i = 1:n_models
            
            if strcmp(S.options.fit_sim_model,'yes') && S.sim_flag ==1
                
                if strcmp(S.options.sim_model,'waterlevel')
                    defo_model = load(fullfile(pwd,'defo_sim_wl.mat'));
                    [~,d_ix] = ismember(dates_num,defo_model.dates);
%                     temp = defo_model.defo_model(:,d_ix)'./S.options.sim_max_shrink;
                    temp = defo_model.defo_model(:,d_ix)';
                    temp = temp./max(abs(temp));
                    intra_annual_models = temp(2:end,:) - temp(1,:);
                elseif strcmp(S.options.sim_model,'extensometer')
                    defo_model = load(fullfile(pwd,'defo_sim_ext.mat'));
                    [~,d_ix] = ismember(dates_num,defo_model.dates);
                    temp = defo_model.defo_model(:,d_ix)'./S.options.sim_max_shrink;
                    intra_annual_models = temp(2:end,:) - temp(1,:);
                elseif strcmp(S.options.sim_model,'CRFS')
                    defo_model = load(fullfile(pwd,'defo_sim_CRFS.mat'));
                    [~,d_ix] = ismember(dates_num,defo_model.dates);
                    temp = defo_model.defo_model(:,d_ix)'./S.options.sim_max_shrink;
                    intra_annual_models = temp(2:end,:) - temp(1,:);
                else
                 error('provide correct sim model')
                end
            
            else
                current_model = regressors{i};
                if strcmp(current_model,'waterlevel')

                    intra_annual_models = jihg_read_waterlevel(S); % needs further optimization

                end

                 if strcmp(current_model,'CRFS')
                    T = jihg_calc_crfs_model(S);
                    T2 = T(dates,:);
                    
%                     %PC: added check that the date vectors match
%                     if size(T2,1) < size(dates,1)
%                         % usually its that last date that is missing, so
%                         % loop backwards through dates
%                         for j = size(dates,1):-1:1 
%                             if find(dates(j),T2(:,1),'descending')
                    
                    if S.options.CRFS_v2 == 1
                        intra_annual_models = repmat(T2.model2,1,nwin);
                    else
                        intra_annual_models = repmat(T2.model,1,nwin);
                    end
                    if S.options.rain_unw == 1
                        % for beta part, trying new unwrapping stuff
                        TR = timerange(dates(1)-days(5),dates(end));
                        RF = T(TR,1);
                        test = retime(RF,'regular','sum','TimeStep',days(6));
                        test.Time = test.Time+days(5);
                        RF_epoch = test.RH(dates);
                        no_sub = RF_epoch > 5;
                        no_sub2 = RF_epoch > 15;

                        no_sub = repmat(no_sub',nwin,1);
                        no_sub2 = repmat(no_sub2',nwin,1);

                        ifgs_sub = ifgs > 0.75*pi;
                        ifgs_sub2 = ifgs > 0.25*pi;

                        ifgs_new = ifgs;
                        ifgs_new(ifgs_sub & no_sub) = ifgs(ifgs_sub & no_sub) - 2*pi;
    %                     ifgs_new(ifgs_sub2 & no_sub2) = ifgs(ifgs_sub2 & no_sub2) - 2*pi;
                        tsr_new = cumsum(ifgs_new,2);
                        tsr = tsr_new;
                    
                        
                    end
                 end

                if strcmp(current_model,'extensometer') 
                    T_ext = jihg_read_extensometer(S);
                    T2 = T_ext(dates,:);
                    model_intra = T2.data;
                    mmax = max(model_intra);
                    temp = model_intra - mmax;
                    mmin = min(temp);
                    model_intra = temp./abs(mmin);
                    intra_annual_models=repmat(model_intra,1,nwin);
                end 
            end


        end
        
        est_param = zeros(nwin,2);
        regressors = zeros(size(intra_annual_models));
        in_p = S.win.win_f == 3; 
        tic
        for j = 1:nwin
            if in_p(j)
            
%                 model = smooth(intra_annual_models(:,j),3);
                model = intra_annual_models(:,j);

                regressors(:,j) = model;
                Time = dates;
                T_model = timetable(Time,model);
                
                

               
                monthly_avg = retime(T_model,'monthly','mean');
                monthly_avg.model = monthly_avg.model + linspace(1e-10,1e-9,height(monthly_avg))'; %add small number to make values unique
                first_half = ismember(month(monthly_avg.Time),1:6);
                year_max = retime(monthly_avg(first_half,:),'yearly','max');
                year_min = retime(monthly_avg,'yearly','min');            

                % don't use years starting in second part
                if size(year_max,1) ~= size(year_min,1)
                    [~,x1] = setdiff(year(year_min.Time),year(year_max.Time));
                    year_min(x1,:) = [];                
                end
                
                m_ix_max = ismember(monthly_avg.model, year_max.model);
                m_ix_min = ismember(monthly_avg.model, year_min.model);

                months_max = monthly_avg.Time(m_ix_max);
                months_min = monthly_avg.Time(m_ix_min);            


                date_sel = []; yr_max_ref =[];nr_periods = 0;
                for i =  1 : length(months_max)
                    if months_max(i) < months_min(i)
                        TR =timerange(months_max(i),months_min(i),'months');
                        Ttemp = T_model(TR,:);
                        t = Ttemp.Time;
                        date_sel = [date_sel;t]; 
                        yr_max_ref = [yr_max_ref; repmat(year_max.model(i),length(t),1)];
                        nr_periods = nr_periods + 1;
                    end
                end
                
                if nr_periods == 0
                    est_param(j,:) = 0;
                    regressors(:,j) = 0;
                    continue
                end
                [~,yr_ix] = ismember(year(date_sel),year(year_max.Time));
                yrs = unique(yr_ix);
                B = zeros(length(date_sel),length(nr_periods));
                for i = 1:nr_periods
                    B(yr_ix == yrs(i),i) = 1;
                end
                
                B = [B T_model.model(date_sel)];
                
                ph_in = tsr(j,:)';        
%                 W=(coh(i,:).^2) ./ (1-coh(i,:).^2);% Fisher Information as weights
%                 W(isinf(W)) = 0;
%                 %         W = ones(1,nifgs);
                W = 1./CRB(j,:)';
                if rank(W) == 0
                    W = ones(size(W));
                end


                T_win = timetable(dates,ph_in,W);
                win_monthly_avg= retime(T_win,'monthly','mean');
                avg_max_month = win_monthly_avg(m_ix_max,:);
                T_sel = T_win(date_sel,:);                  

                ph_win_model = T_sel.ph_in - avg_max_month.ph_in(yr_ix);
                
                if rank(B) < size(B,2)
                    1+1;
                end
                
                est = lscov(B,ph_win_model,T_sel.W);
%                     est1 = lscov(B,ph_win_model);
                est_param(j,:) = [mean(est(1:nr_periods)) est(end)];
                
%                 figure(1); plot(T_sel.dates, ph_win_model); hold on; 
% 
%                 plot(T_sel.dates,T_sel.ph_in); 
%                 plot(T_sel.dates,B*est,'LineWidth',2)
% %                 plot(T_sel.dates,B*est1,'--','LineWidth',2)
%                 hold off



            end
            if mod(j,round(nwin/10)) == 0
                    fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',j,nwin,round(j/nwin*100),toc)      
            end
        end
        
%         B = [ones(nifgs,1) intra_annual_models];
%         model_amp = B(:,end)*est_param(:,end)'; 
        output.tsr_unw_rev = tsr;
        output.est_param = est_param;
        output.regressors = regressors;
%         output.model_amp = model_amp'; % scaled model per prixel;
        output.in_p = in_p; % logical for model location
%         output.design_matrix =  ;

    % PC: I dont know what this is i'm just adding code to make this hot
    % pile of garbage run
    if exist('m_ix_max','var')
        output.ref_ix = m_ix_max;
    else
        output.ref_ix = nan;
        warning('output.ref_ix set to nan')
    end
        save([output_path,save_name],'-struct','output');

%     else
%         output = load([output_path,save_name]);
%     end

end

