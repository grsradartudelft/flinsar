function [nbs] = jihg_mean_test(amp_mean_sel,nslcs,alpha)

        k_alpha              = norminv(1-alpha/2,0,1); 
        
        amp_sorted          = sort(amp_mean_sel);
        amp_median          = amp_sorted(round(length(amp_mean_sel)/2));
        ix_ref              = find(amp_mean_sel==amp_median,1);

        amp_mean_ref        = amp_mean_sel(ix_ref);
        amp_std_ref         = 0.52/sqrt(nslcs) * amp_mean_ref;
        nbs                 = abs(amp_mean_ref - amp_mean_sel) <= k_alpha*amp_std_ref;

end