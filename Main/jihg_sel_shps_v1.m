function [S,output] = jihg_sel_shps(S,input_geo,input_amp)
%JIHG_SEL_SHPS selects per window (statistically) homogenous pixels.
% These pixels are later used for multi-looking. Pixels not belonging to
% the main group are not used in further processing.

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    %% Otherwise run function.
        % Read input
        alpha           = S.options.alpha;
        mean_test       = S.options.mean_test;

        nslcs           = S.stackinfo.nslcs;

        pix_PS          = S.pix_id.ps;
        pix_in_p        = S.pix_id.in_p;
        pix_rem         = S.pix_id.rem;
        nwin            = S.win.n;        

        win_ixs         = input_geo.win_ixs;
        amp_mean        = input_amp.amp_mean;
        
        %% MAIN
        k_alpha         = norminv(1-alpha/2,0,1); 
        
        % Preallocate cells and arrays
        is_shp          = cell(nwin,1);
        win_shp         = cell(nwin,1);
        nshp            = zeros(nwin,1);        
        win_f           = zeros(nwin,1); % check for type of windows 1 = PS, 2 = PS in field, 3 = field, 0 = other.
       
        
        tic
        for i = 1:nwin
        %% Loop over all windows
        
            ix      = win_ixs{i}; % Pixels inside window
            
            % Check label
            is_ps       = pix_PS(ix);    % PS
            is_in_p     = pix_in_p(ix);  % In polygons
            is_rem      = pix_rem(ix);   % Remainder

            nps_win     = sum(is_ps);
            nin_p_win   = sum(is_in_p);

            ntot_win    = length(ix);
            
            if ~any(nin_p_win)
            %% If no pixels belonging to polygon
                if nps_win > 0.1*ntot_win
                    %% Select based on PS
                    win_shp{i}=win_ixs{i}(is_ps,:);
                    nshp(i)=size(win_shp{i},1);
                    win_f(i) = 1;
                else
                    %% Select all
                    win_shp{i}=win_ixs{i}(is_ps | is_rem,:);
                    nshp(i)=size(win_shp{i},1);
                    win_f(i) = 0;
                end

            else
            %% If pixels in polygon
                if nps_win > 0.5*ntot_win 
                    %% if field is more than 50% PS than median will be a PS.
                    win_shp{i}=win_ixs{i}(is_ps,:);
                    nshp(i)=size(win_shp{i},1);
                    win_f(i) = 2;
                else
                    %% Select pixels in polygon
                    ix = ix(is_in_p);
                    
                    if mean_test == 1
                        
                        win_amp_mean        = double(amp_mean(ix));
                        amp_sorted          = sort(win_amp_mean);
                        amp_median          = amp_sorted(round(length(ix)/2));
                        ix_ref              = ix(find(win_amp_mean==amp_median,1));

                        amp_mean_ref        = amp_mean(ix_ref);
                        amp_std_ref         = 0.52/sqrt(nslcs) * amp_mean_ref;
                        amp_mean_sel        = amp_mean(ix);
                        nbs                 = abs(amp_mean_ref - amp_mean_sel) <= k_alpha*amp_std_ref;
                        is_shp{i}           = nbs & ~is_ps(is_in_p);

                        if sum(is_shp{i}) < 25 % should be an option, I suppose
                                win_shp{i}=win_ixs{i}(is_ps | is_rem,:);
                                nshp(i)=size(win_shp{i},1);
                                win_f(i) = 0;
                            if mod(i,round(nwin/10)) == 0
                                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)    
                            end
                            continue
                        end

                        win_shp{i}          = win_ixs{i}(is_shp{i},:);
                        nshp(i)             = size(win_shp{i},1);
                        win_f(i)            = 3;
                    end
                end     


            end

            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)                
            end
        end
        
        %% output
        output.win_shp          = win_shp;
        output.nshp             = nshp;
        output.win_f            = win_f;
        output.is_shp           = is_shp;
        
        save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.win.win_f = output.win_f;
    S.win.nshp  = output.nshp;

end