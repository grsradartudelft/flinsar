function [x_out,acheck] = jihg_solve_ambiguity(x,model,a)
%UNTITLED7 Summary of this function goes here
%   x = observations that are (un)wrapped (array)
%   model = estimated model to correct unsolved ambiguities in x with
%   (array)
%   a = size of ambiguity. (single value or array)

if length(a) == 1
    a = ones(length(x),1)*a;
end   
    x_new = x-model;
    x_new = mod(x_new+0.5*a,a)-0.5*a;
    acheck = round((model+x_new-x)./(a));
    x_out = a.*acheck + x;   
    
%
end

