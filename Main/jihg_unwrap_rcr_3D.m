function [S,output] = jihg_unwrap_rcr_3D(S,input)

    output_path     = S.options.output_path;

    
    s               = dbstack;
    fun_name        = s(1).name;

    iter_step             = input.iter_step;
    if isfield(input,'joint')
        save_name       = [fun_name,'_joint_output',num2str(iter_step),'.mat'];
        overwrite       = input.overwrite;
    else
        save_name       = [fun_name,'_output',num2str(iter_step),'.mat'];
        overwrite = 0;
    end
    output.iter_step      = iter_step;

    
     if ~(exist(fullfile(output_path,save_name),'file')==2) || overwrite
        %make snaphu.conf.in
        
        
        
        nifgs           = S.stackinfo.nifgs;
        lambda          = S.options.lambda;
        unwrap_method   = S.options.unwrap_method;
        dates_num       = S.stackinfo.dates;
        nwin            = S.win.n;
        nlines          = S.win.nlines;
        npixels         = S.win.npixels;

        ifgs            = input.ifgs;
        gamma_pta       = input.gamma_pta; 
        win_f_joint     = input.win_f_joint;      

        XY = load([output_path,'jihg_aw_geocoords_output.mat'],'win_rdx','win_rdy');
        
        xy = [XY.win_rdx,XY.win_rdy];
%         [l_grd,p_grd] = ndgrid(1:nlines,1:npixels);     

        [ref_ind] = jihg_unw_setref(S);        

        %% remove bad points and replace by nnb 
        coh_in = gamma_pta;   
        out = win_f_joint == -1 | coh_in < 0;

        if S.options.inc_rem ~= 1
            out = out | win_f_joint == 0;
        end       
        
            
        ifg_in = ifgs(~out,:);
        xy_in  = xy(~out,:);
        uw_ph = zeros(size(ifgs));
%         x = p_grd(~out);
%         y = l_grd(~out);
%         xq = p_grd(out);
%         yq = l_grd(out);
%         
%         ifgs_nnb = ifgs;
%         for i = 1:nifgs
%             ifg_vq = griddata(x,y,ifg_v(:,i),xq,yq,'nearest');
%             ifgs_nnb(out,i) = ifg_vq;
%         end
            
            
        %% unwrap with 3D algorithm used in StaMPS (more than anything because I understand how it works).
%         uw_ph = ifgs;
        ph_w = ifg_in;        
        
        ifg_ix = S.stackinfo.ifg_ix_sm;
        options=struct('master_day',dates_num(1));
        options.time_win=360;
        options.unwrap_method='3D';
        options.grid_size = S.options.win_size;
        options.prefilt_win=4;
        options.la_flag='n';
        options.scf_flag='n';

        %run StaMPS unwrapping method
        [ph_uw,msd]=uw_3d(ph_w,xy_in,dates_num,ifg_ix,[],options);
        uw_ph(~out,:) =  ph_uw;
        uw_ph = uw_ph - mean(uw_ph(ref_ind,:));

        %remove intermediate files
        delete('snaphu.costinfile');delete('snaphu.in');delete('snaphu.out')
        delete('uw_grid.mat');delete('uw_interp.mat');delete('uw_phaseuw.mat');delete('uw_space_time.mat');


            
            
            
%             figure(11); imagesc(reshape(ifg_in,nlines,npixels))
%             figure(12); imagesc(uw_ref)
%             figure(13); imagesc(reshape(out,nlines,npixels))
%             figure(14); imagesc(reshape(~out,nlines,npixels))
            
        
        output.ifgs_uw  = uw_ph;
        output.ifgs_coh = gamma_pta;
        save([output_path,save_name],'-struct','output');
    
    else
        output = load([output_path,save_name]);
    end


end
    