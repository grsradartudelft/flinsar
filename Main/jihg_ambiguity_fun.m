function [est_param]=jihg_ambiguity_fun(y,A,W,p_min,p_max,p_step)
% JIHG ambiguity_fun creates a search space of possible 1 or 2 parameter
% functional models. The size of the search space is determined by the
% step size and (expected) bounds of the parameter. 
% The goal is to find the values of parameters corresponding to the model
% with the least square error.
% 
    
    n_p = size(A,2);
    N   = length(y);
    
    if isrow(y)
        y = y';
        W = W';
    end
    
    if n_p ==1
        search_space = p_min:p_step:p_max;
        all_ts       = kron(A,search_space);
        e_ts=repmat(y,1,length(search_space))-all_ts;         
        L2norm = 1/(N)*sum(wrapToPi(e_ts.*1./W.*e_ts));
        
        [~,lind]=min(L2norm);
        est_param = search_space(1,lind);
        
    elseif n_p == 2
        
        search_space1 = p_min(1):p_step(1):p_max(1);
        search_space2 = p_min(2):p_step(2):p_max(2);
        [ss1,ss2]=meshgrid(search_space1,search_space2);
        
        search_space=[ss1(:) ss2(:)]';       
        
        all_ts=kron(A(:,1),search_space(1,:))+ ...
                kron(A(:,2),search_space(2,:));            
       
        e_ts=wrapToPi(repmat(y,1,length(search_space))-all_ts);         
        L2norm = 1/(sum(W))*sum(e_ts.*W.*e_ts);
        
        [~,lind]=min(L2norm);
        est_param = [search_space(1,lind) search_space(2,lind)];
    end
    

end