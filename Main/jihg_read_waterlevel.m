function [output] = jihg_read_waterlevel(S)




    output_path = S.options.output_path;
    input_path = S.options.input_path;
    fname_wl = S.options.fname_wl;
    fname_wlxy = S.options.fname_wlxy;
    
    n_sets = length(fname_wl);
    
    
    if ~(exist(fullfile(output_path,'waterlevel_interp.mat'),'file')==2)
        
        data = timetable;
        xy_data = table;
        for i = 1:n_sets
            TT_read = readtimetable([input_path,fname_wl{i}]);
            T_read = readtable([input_path,fname_wlxy{i}]);
            xy_data = [xy_data; T_read];
            data = outerjoin(data,TT_read);
        end

        dates_num = S.stackinfo.dates_num;
        dates = datetime(datestr(dates_num(2:end)));

        radar_XY = load([output_path,'jihg_aw_geocoords_output.mat'],'win_rdx','win_rdy');

        waterlevel1 = data(dates,:);
        waterlevel_orig = waterlevel1{:,:};
        n_dt = size(waterlevel_orig,1);
        wl_median = nanmedian(waterlevel_orig);
        waterlevel = waterlevel_orig - nanmedian(waterlevel_orig);          
        
        diff_wl = diff([waterlevel(1,:);waterlevel]);
        weird_jump = diff_wl < -0.5;
        waterlevel(weird_jump) = nan;
        
        diff_wl = diff([zeros(size(waterlevel(1,:)));waterlevel]);
        weird_jump = diff_wl > 0.5;
        waterlevel(weird_jump) = nan;

        X = xy_data.X;
        Y = xy_data.Y;

        Xq = radar_XY.win_rdx;
        Yq = radar_XY.win_rdy;
        
        for i = 1:n_dt
            

            
            y = waterlevel(i,:);
           
            % remove nans
                
            avg_y = nanmean(y);
            std_y = nanstd(y);                

            outlier = y > avg_y + 3*std_y | y < avg_y - 3*std_y ;
%             figure(1); plot(y)
            while any(outlier)
                
                [wi,ix] = max(abs(y - avg_y));
                y(ix) = nan;
                avg_y = nanmean(y);
                std_y = nanstd(y); 
                outlier = y > avg_y + 3*std_y | y < avg_y - 3*std_y ;
%                 figure(2); plot(y)
            end
                
            waterlevel(i,:) = y;
            
        end
        
        
        
        n_dt = size(waterlevel,1);

        query_data = zeros(n_dt,S.win.n);
        for i = 1:n_dt
            sample_data = waterlevel(i,:); 
            % nancheck
            nans = isnan(sample_data);
            warning off;
            F = scatteredInterpolant(X(~nans),Y(~nans),sample_data(~nans)','natural','nearest');

            query_data(i,:) = F(Xq,Yq);
            warning on;
%             figure(1)
%             scatter(Xq,Yq,10,query_data(i,:) - query_data(1,:),'filled');
%             colorbar
%             hold on
%             
%             scatter(X(~nans),Y(~nans),50,sample_data(~nans) - waterlevel(1,~nans),'filled','h','MarkerEdgeColor',[0,0,0])
%             hold off

        end
        
        
        max_wl = max(query_data);
        waterlevel3 = query_data - max_wl;
        min_wl = min(waterlevel3);
        waterlevel_scaled_intp = waterlevel3./abs(min_wl);
        
        F = scatteredInterpolant(X,Y,wl_median','natural','nearest');
        wl_median_interp=F(Xq,Yq);
    
        query_data = query_data + repmat(wl_median_interp,1,n_dt)';

        output =  waterlevel_scaled_intp;
        
        
        nwin = S.win.n;
        win_names = join([string(repmat('win',nwin,1)) strtrim(string(num2str((1:nwin)')))],'');
        TT = array2timetable(waterlevel_scaled_intp,'RowTimes',dates,'VariableNames',win_names);
        TT_true = array2timetable(query_data,'RowTimes',dates,'VariableNames',win_names);
        save([output_path,'waterlevel_interp.mat'],'TT','TT_true','max_wl','min_wl');
     else
        out = load([output_path,'waterlevel_interp.mat']);
        TT = out.TT;
        output = TT{:,:};
    end

end