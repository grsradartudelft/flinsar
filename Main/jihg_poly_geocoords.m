function [S,output] = jihg_poly_geocoords(S)
% JIHG_POLY_GEOCOORDS creates windows in geocoordinates based on polygons.


% Author: FMG HEUFF - Feb 2020.

%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
        %% Otherwise run function
        
        main_dir        = S.options.main_dir;
        nlines          = S.stackinfo.nlines;
        npixels         = S.stackinfo.npixels;
        path_coords     = S.stackinfo.path_coords;     
        
        win_size        = S.options.win_size; %[m]
        ll              = S.options.ll; % lowerleft of aoi
        ur              = S.options.ur; % upper right of aoi
        convert2rd      = S.options.convert2rd;

        
        % python function to convert WSG84 to RD. This makes it possible to
        % create windows closest to the provided window width in [m].
        if count(py.sys.path,[main_dir,'/jihg_modules/Utilities']) == 0
            insert(py.sys.path,int32(0),[main_dir,'/jihg_modules/Utilities'])
        end
        % read lon/lat raw files
        lon = freadbk_quiet(path_coords{1},nlines);
        lat = freadbk_quiet(path_coords{2},nlines);
        
        
        % determine number of windows

        % determine stepsize
        dlat = S.win.dlat;
        dlon = S.win.dlon;
        
        aoi_lon = [S.win.lon_range(1) - dlon/2; S.win.lon_range(1) - dlon/2; S.win.lon_range(end) + dlon/2; S.win.lon_range(end) + dlon/2]; 
        aoi_lat = [S.win.lat_range(1) - dlat/2; S.win.lat_range(end) + dlat/2; S.win.lat_range(end)+ dlat/2; S.win.lat_range(1) - dlat/2];      
        
        
        x = inpolygon(lon,lat,aoi_lon,aoi_lat);
        
        [S,output] = jihg_poly_sel(S);
        
        poly_ids = output.pix_poly_id;
        
        npoly = unique(poly_ids);
        
        poly_ids(~x) = 0;
        poly_ix = unique(poly_ids);
        poly_ix(poly_ix == 0) = [];
        npoly = length(poly_ix);
%         lat_range = bb_ll(1):dlat:bb_ll(3);
%         lon_range = bb_ll(2):dlon:bb_ll(4);
% 
%         nwin_l  = length(lat_range);
%         nwin_p  = length(lon_range);
%         nwin    = nwin_l*nwin_p;
        
        % preallocate cells, number of pixels per window varies.
        win_subs=cell(npoly,1);
        win_ixs=cell(npoly,1);
        win_lat = zeros(npoly,1);
        win_lon = zeros(npoly,1);
        win_median = zeros(npoly,1);
        win_cntr = zeros(npoly,2);
        patch_min = nan(1,2);
        patch_max = nan(1,2);
    
        tic
        % loop over all pixels and find the window they fall inside.
        for i = 1:npoly
            
%             ix1 = find(lat(i) + dlat/2 >= lat_range & lat(i) - dlat/2 < lat_range);
%             ix2 = find(lon(i) + dlon/2 >= lon_range & lon(i) - dlon/2 < lon_range); 
            ixs = find(poly_ix(i) == poly_ids);

%             if isempty(ix1) || isempty(ix2)
%                 if mod(i,round(length(lat(:))/10)) == 0
%                     fprintf('Finished %i of %i pixels. [%i%% %0.2f s] \n',i,length(lat(:)),round(i/length(lat(:))*100),toc)
%                 end
%                 continue
%             end 
            
            % save indices, win_line/win_pixel.
            [iy,ix]                     = ind2sub([nlines,npixels],ixs); 
            win_subs{i}                 = uint16([iy ix]);
            win_ixs{i}                  = ixs; 
            
            [~,iy_sort] = sort(iy);[~,ix_sort] = sort(ix);
            ix_median                   = [iy(iy_sort(round(length(iy_sort)/2))) ix(ix_sort(round(length(ix_sort)/2)))];  
            nn_ix                       = dsearchn([iy,ix],ix_median);
            win_median(i)               = ixs(nn_ix);
            win_cntr(i,:)               = [iy(nn_ix),ix(nn_ix)];
            win_lon(i)                  = lon(win_median(i));
            win_lat(i)                  = lat(win_median(i));
            temp_subs                   = double(win_subs{i});
            max_ixs                     = max(temp_subs,[],1);
            min_ixs                     = min(temp_subs,[],1);
            patch_min              = min([patch_min;min_ixs],[],1);                    
            patch_max              = max([patch_max;max_ixs],[],1);

            if mod(i,round(npoly/10)) == 0
                fprintf('Finished %i of %i pixels. [%i%% %0.2f s] \n',i,npoly,round(i/npoly)*100,toc)
            end

        end
        
%         [win_lon,win_lat] = meshgrid(lon_range,lat_range); 

        patches = [patch_min(:,1) patch_max(:,1) patch_min(:,2) patch_max(:,2)];
        
        x1=py.wsg2rd.wsg2rd(win_lat(:),win_lon(:));
        win_rdx = double(x1{1})';
        win_rdy = double(x1{2})';     
       
        
        % output
        output.win_subs = win_subs;
        output.win_ixs = win_ixs;
        output.win_lon = win_lon(:);
        output.win_lat = win_lat(:);
        output.win_rdx = win_rdx;
        output.win_rdy = win_rdy;
        output.patches_poly = patches;
        output.win_cntr = win_cntr;
        
%         output.nwin_l = nwin_l;
%         output.nwin_p = nwin_p;
        output.npoly = npoly; 
        
        
       save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.win.npoly = output.npoly;

end