function jihg_steps_single
    cwd = pwd;
    S = load('jihg_S');


    master_dates = S.master_date;
    output_folders = S.output_folder;
    stack_paths = S.stack_path;
    input_folder = S.input_folder;
    tic
    
    if iscell(master_dates)
        output_folder = output_folders{1};
        disp(['##### Processing ',output_folder,'...']);
        S.output_path = output_folder; %[cwd,'/',output_folder,'/'];
        S.input_path = input_folder; %[cwd,'/',input_folder,'/'];
        S.master_date = master_dates{1};
        S.stack_path = stack_paths{1};
        
        if S.depsi_atmosphere == 1
           S.depsi_path = S.depsi_path{1};
           S.depsi_project_id = S.depsi_project_id{1};
        end
        
        save('jihg_S','-struct','S');
        jihg_steps;
        disp(['### DONE processing ',output_folder,', elapsed time: ' num2str(toc) ' s.'])
    else
        output_folder = output_folders;
        fprintf(['\n##### Processing ',output_folder,'...\n']);
        S.output_path = [cwd,'/',output_folder,'/'];
        S.input_path = [cwd,'/',input_folder,'/'];
        S.master_date = master_dates;
        S.stack_path = stack_paths;
        save('jihg_S','-struct','S');
        jihg_steps;
        fprintf(['### DONE processing ',output_folder,', elapsed time: %0.2f s. \n'],toc);
    end

end