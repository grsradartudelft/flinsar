function jihg_create_joint_output_v3(S,input)

% PC: this function was a goddamn mess so I deleted all the crap that wasnt
% doing anything. If you want to see it look at v2

folders   = S.options.output_folder;
nf        = length(folders);
nwin      = S.win.n;
win_f     = input.win_f_final;
los2vert  = input.los2vert;
gamma_pta = input.gamma_pta;

%% average final_parameters
EP_final_avg = zeros(S.win.n,3);
for j = 1:3
    est_param=zeros(S.win.n,nf);
    for i = 1:nf
        est_param(:,i) = input.(['stack',num2str(i)]).EP_final(:,j);
    end
    
    % average weighted by gamma_pta
    weights = gamma_pta./sum(gamma_pta,2);
    EP_final_avg(:,j)  = sum(weights.*est_param.*los2vert,2);    % weigh and project to vertical
end

%% for clarity set first mean of first winter to zero (clarity...right)
ph2h = -S.options.lambda*1000/4/pi;

win_names = join([string(repmat('win',nwin,1)) strtrim(string(num2str((1:nwin)')))],'');
win_poly = win_f == 3;

% store everything as timetables PC: I hate timetables
TSR = timetable;
CRB = timetable;
EST = timetable;
TSR_W = timetable;
TSR_SBAS = timetable;
COH = timetable;
STD = timetable;
POLY = timetable;
TT_wl = timetable;
UNW_E = timetable;

%PC added stack names and indices so that we can actually access the output
%and know where it came from
STACK_ID  = [];
STACK_IDX = [];

for j = 1:nf
    input_folder = folders{j};
    S = load([input_folder,'S.mat']);
    model_output = load([input_folder,'jihg_poly_model_v2_output.mat']);
       
    dates_num       = S.stackinfo.dates_num(2:end);
    ref_period      = S.options.ref_period;
    ref_datenum     = datenum(ref_period{1},'yyyymmdd'):datenum(ref_period{2},'yyyymmdd');
    ref             = ismember(dates_num,ref_datenum);
    
    DM_final = input.(['stack',num2str(j)]).DM_final;
    est_final = zeros(nwin,length(dates_num));
    for i = 1:nwin
        est_final(i,:) = EP_final_avg(i,:)*DM_final(:,[1:2,i+2])'./los2vert(i,j);
    end
    
    ref_mean = mean(est_final(:,ref),2);
    est_final = est_final - ref_mean ;
    
    epoch = datetime(datestr(S.stackinfo.dates_num(2:end)));
    crb   = input.(['stack',num2str(j)]).CRB_dc;
    crb_dc = input.(['stack',num2str(j)]).CRB_dc;
    
    % make a final correction per stack based on final estimate (PC: what
    % does this mean)
    tsr_in = input.(['stack',num2str(j)]).tsr_unw;
    ref_mean = mean(tsr_in(:,ref),2);
    tsr_in = tsr_in - ref_mean;
    tsr_ph = zeros(size(tsr_in));
    for i = 1:nwin
        tsr_ph(i,:) = jihg_solve_ambiguity(tsr_in(i,:)',est_final(i,:)',2*pi);
    end
    
    tsr_unw_sbas = input.(['stack',num2str(j)]).tsr_unw_sbas;
    tsr_coh = input.(['stack',num2str(j)]).ifgs_coh;
    ref_mean = mean(tsr_unw_sbas(:,ref),2);
    tsr_unw_sbas= tsr_unw_sbas - ref_mean;
    
    std_phase = zeros(size(tsr_coh));
    if S.options.std_output == 1
        for i = 1:S.win.n
            if S.win.win_f == 1
                std_phase(i,:) = deg2rad(coh2std(tsr_coh(i,:),'ps',0));
            else
                std_phase(i,:) = deg2rad(coh2std(tsr_coh(i,:),'ds',S.win.nshp(i),0));
            end
        end
    end
    
    % experimental: create indication for likelihood of unwrapping error
    est_individual   = model_output.est_param(:,2).*model_output.regressors';
    d_est_individual = gradient(est_individual/2/pi);
    
    likelihood_unw_err = abs(d_est_individual) + sqrt(crb_dc)/2/pi;
    likelihood_unw_err(likelihood_unw_err>1) = 1;
    likelihood_unw_err(:,1) = 0;
    
    % RCR time series
    tsr = tsr_ph.*los2vert(:,j)*ph2h;
    std_tsr = -std_phase.*los2vert(:,j)*ph2h;
    tsr(~win_poly,:) = NaN;
    
    % Single stack EMI time-series (not sbas)
    tsr_sbas = tsr_unw_sbas.*los2vert(:,j)*ph2h;
    tsr_sbas(~win_poly,:) = NaN;

    est_final(~win_poly,:) = NaN;
    crb(~win_poly) = NaN;
    
    est_final = est_final.*los2vert(:,j)*ph2h;
    crb = -crb.*los2vert(:,j)*ph2h;
    
    TSR = [TSR;array2timetable([tsr',repmat(j,length(epoch),1)],'RowTimes',epoch,'VariableNames',[win_names;{'stack'}])];
    TSR_SBAS = [TSR_SBAS;array2timetable([tsr_sbas',repmat(j,length(epoch),1)],'RowTimes',epoch,'VariableNames',[win_names;{'stack'}])];
    COH = [COH;array2timetable([tsr_coh',repmat(j,length(epoch),1)],'RowTimes',epoch,'VariableNames',[win_names;{'stack'}])];
    STD = [STD;array2timetable([std_tsr',repmat(j,length(epoch),1)],'RowTimes',epoch,'VariableNames',[win_names;{'stack'}])];
    CRB = [CRB;array2timetable(crb','RowTimes',epoch,'VariableNames',win_names)];
    EST = [EST;array2timetable(est_final','RowTimes',epoch,'VariableNames',win_names)];
    temp = model_output.est_param(:,2).*model_output.regressors';
    temp = temp - mean(temp(:,ref),2);
    poly_amp = temp.*los2vert(:,j)*ph2h;
    POLY = [POLY;array2timetable(poly_amp','RowTimes',epoch,'VariableNames',win_names)];
    UNW_E = [UNW_E;array2timetable(likelihood_unw_err','RowTimes',epoch,'VariableNames',win_names)];
    
    % needed for correct mean (PC: wtf are you talking about)
    tsr_w = tsr_ph.*los2vert(:,j)*ph2h;
    tsr_w(~win_poly,:) = NaN;
    TSR_W = [TSR_W;array2timetable([tsr_w',repmat(j,length(epoch),1)],'RowTimes',epoch,'VariableNames',[win_names;{'stack'}])];
    
    % PC: added stack index to access these outputs which contain
    % all stacks just thrown together
    [~,current_stack,~] = fileparts(S.options.stack_path);
    startIdx = size(TSR_SBAS,1) - length(epoch) + 1;
    STACK_ID{j} = current_stack;
    STACK_IDX(j) = startIdx;
end

coords = load([input_folder,'jihg_aw_geocoords_output.mat']);
win_lon = coords.win_lon;
win_lat = coords.win_lat;
win_rdx = coords.win_rdx;
win_rdy = coords.win_rdy;
gamma_pta = mean(gamma_pta,2);

los2vert = mean(los2vert,2); %PC: what is this for?

lin_sub = -EP_final_avg(:,2)*ph2h; % minus sign because of minus sign in ph2h; 
lin_sub(~win_poly) = NaN; %******* Check: MISSING WINDOWS coming from here???************************
model_amp = EP_final_avg(:,3)*ph2h;
model_amp(~win_poly) = NaN;
[win_p,win_l] = meshgrid(1:coords.nwin_p,1:coords.nwin_l);
win_l = win_l(:);
win_p = win_p(:);
win_type = win_f;
VAR = table(win_names,win_p,win_l,win_lon,win_lat,win_rdx,win_rdy,lin_sub,model_amp,win_type,gamma_pta,los2vert);

%% Output
clearvars -except S VAR TSR CRB EST TSR_new POLY new_vals win_names TT_wl sim_flag T_Sim TSR_SBAS COH UNW_E STD STACK_ID STACK_IDX

% PC: the _6D outputs are the weighted averages of all RCR outputs...i think
TSR = sortrows(TSR);
TSR_6d = retime(TSR,'regular','mean','TimeStep',days(6));
CRB_6d = retime(CRB,'regular','mean','TimeStep',days(6));
COH_6d = retime(COH,'regular','mean','TimeStep',days(6));
STD_6d = retime(STD,'regular','mean','TimeStep',days(6));
EST_6d = retime(EST,'regular','mean','TimeStep',days(6));
UNW_E = retime(UNW_E,'regular','mean','TimeStep',days(6));

save(['results_p1_',S.options.run_name],'VAR','TSR','CRB','EST','TSR_SBAS','STACK_ID','STACK_IDX');
save(['results_p2_',S.options.run_name],'EST_6d','TSR_6d','CRB_6d','COH_6d','STD_6d');
save(['results_p3_',S.options.run_name],'TT_wl','POLY','UNW_E');
save(['results_p4_',S.options.run_name],'TSR_SBAS','COH','STD');


end

