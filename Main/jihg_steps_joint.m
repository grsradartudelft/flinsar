function jihg_steps_joint(joint_output_folder)

% This function is required to make everything work with multiple stacks.
% It creates the input similar to that of a singe stack processing run. 

    J = load([joint_output_folder 'jihg_J']);
    S = J;

    master_dates = J.master_date;
    output_folders = J.output_folder;
    stack_paths = J.stack_path;
    input_folder = J.input_folder;
    if J.ps_flag == 1
        depsi_paths = J.depsi_path;
        depsi_project_ids = J.depsi_project_id;
    end
    tic
    
    for i = 1:length(master_dates)   
        output_folder = output_folders{i};
        fprintf(['\n##### Processing ',output_folder,'...\n']);
        S.output_path = output_folder; %[cwd,'/',output_folder,'/'];
        S.input_path = input_folder; %[cwd,'/',input_folder,'/'];
        S.master_date = master_dates{i};
        S.stack_path = stack_paths{i};
        if J.ps_flag == 1
            S.depsi_path = depsi_paths{i};
            S.depsi_project_id = depsi_project_ids{i};
        end
        save([J.joint_output_folder 'jihg_S'],'-struct','S');
        cd(joint_output_folder)
        jihg_steps;
        fprintf(['### DONE processing ',output_folder,', elapsed time: %0.2f s. \n'],toc);
    end
    
    
    if J.sim_joint ==1        
        jihg_joint_rcr_v2(1);
    else
        jihg_joint_rcr_v2(0);
    end

end