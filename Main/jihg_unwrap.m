function [S,output] = jihg_unwrap(S,input)
%JIHG_UNWRAP Unwraps interferograms using SNAPHU

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    save_name       = [fun_name,'_output.mat'];
  
     if ~(exist(fullfile(output_path,save_name),'file')==2)
     %% Otherwise run function.
        % Read input

        nifgs           = S.stackinfo.nifgs;
        lambda          = S.options.lambda;
        unwrap_method   = S.options.unwrap_method;
        nwin            = S.win.n;
        nlines          = S.win.nlines;
        npixels         = S.win.npixels;
        ESM_EDC         = S.options.pl_ESM_EDC;
        ps_flag         = S.options.ps_flag;
        inc_rem         = S.options.inc_rem;
        win_f           = S.win.win_f;
        
        
        if strcmp(ESM_EDC,'EDC')
            ifgs        = input.ifgs_dc;
        else
            ifgs        = input.ifgs_sm;
        end
        
        if ps_flag == 1
            ps_name = S.options.ps_software;
            if strcmp(ps_name,'depsi')
                % used to subtract the atmosphere to make unwrapping more
                % accurate.
                [S,output_atmo] = jihg_depsi_atmosphere(S);
            end
            win_atmos     = output_atmo.win_atmos_dc;
        end

        coh            = input.coh; 
        
        
        
        %% MAIN
        %make snaphu.conf.in
        system('echo INFILE  phase.in > snaphu.conf.in');        
        system('echo INFILE  phase.in > snaphu.conf.in');
        system(['echo LINELENGTH ',num2str(npixels),' >> snaphu.conf.in']);
        system('echo OUTFILE phase.out >> snaphu.conf.in');
        system('echo CORRFILE coherence.in >> snaphu.conf.in');
        system('echo STATCOSTMODE DEFO >> snaphu.conf.in');
        system(['echo INITMETHOD ',unwrap_method,' >> snaphu.conf.in']);
        system('echo INFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo OUTFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo CORRFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo EARTHRADIUS 6378000.0 >> snaphu.conf.in');
        system(['echo LAMBDA ',num2str(lambda/1000),' >> snaphu.conf.in']);
    %     system(['!echo "NCORRLOOKS ' num2str(ncorrlooks) '" >> snaphu.conf.in']);
    
    
        % Currently JIHG_CPXCOH outputs the ESM.
        uw_ph   = zeros(nwin,nifgs); 
        uw_ref  = zeros(nifgs,1);
        ifg     = ifgs;
        
        % in case we want to exclude pixels
        if inc_rem ==0
            excl = win_f == 0;
            coh(excl,:) = 0.01;
        end
        
        % provided reference
        [ref_ind] = jihg_unw_setref(S);
        
        tic
        
        
        snaphu_path = S.options.snaphu_path; %PC: added this path
        for i = 1:nifgs
    %         unwrap by snaphu
            
            if ps_flag == 1
                ifg_in = angle(exp(1j*ifg(:,i)).*conj(exp(1j*win_atmos(:,i))));
            else
                ifg_in = ifg(:,i);
            end
            
            coh_in = coh(:,i);            
            
            fwritebk(reshape(ifg_in,nlines,npixels),'phase.in','float32');
            fwritebk(reshape(coh_in,nlines,npixels),'coherence.in','float32');
            
            system([snaphu_path 'snaphu -f snaphu.conf.in -s >> snaphu.log']);
            
            uw          = freadbk_quiet('phase.out',nlines,'float32');
            uw_ref(i)   = mean(uw(ref_ind));    % needed for sims
            uw_ph(:,i)  = uw(:) - uw_ref(i);       
  
            
            
            if mod(i,round(nifgs/10)) == 0
                fprintf('Finished %i of %i ifgs. [%i%% - %0.2f s] \n',i,nifgs,round(i/nifgs*100),toc)
                
            end

        end        
        
        delete('phase.in'); delete('coherence.in'); delete('phase.out'); delete('snaphu.conf.in');
        delete('snaphu.log');
        
        output.ifgs_uw = uw_ph;
        output.uw_ref = uw_ref;
        output.ifgs_coh = coh;
        output.CRB_sm  = input.CRB_sm;
        output.CRB_dc  = input.CRB_dc;
        save([output_path,save_name],'-struct','output');
        
        
    
    else
        output = load([output_path,save_name]);
    end

    clearvars -except S output
end
    