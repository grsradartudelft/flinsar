function [S,output] = jihg_cpxcoh(S,input)
%JIHG_CPXCOH computes (all) the multi-looked interferograms

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    %% Otherwise run function.
        % read input
        method          = S.options.pl_method;
        CRB             = S.options.CRB;
        nwin            = S.win.n;
        path_slcs       = S.stackinfo.path_slcs;
        nlines          = S.stackinfo.nlines;
        npixels         = S.stackinfo.npixels;
        nslcs           = S.stackinfo.nslcs;
        win_shp         = input.shps.win_shp;
        win_f           = input.shps.win_f;
        nshp            = input.shps.nshp;
        patches         = input.patches.patches;
        patch_win_ixs   = input.patches.patch_win_ixs;
        

        %% MAIN

        npatch          = size(patches,1);
        nifgs           = nslcs-1;
        
        % preallocate arrays
        ifgs_sm         = zeros(nwin,nifgs);
        ifgs_dc         = zeros(nwin,nifgs);
        CRB_sm          = Inf(nwin,nifgs);
        CRB_dc          = Inf(nwin,nifgs);
        coh             = zeros(nwin,nifgs);
        gamma_pta       = zeros(nwin,1);       
        bad_epochs      = zeros(nwin,nslcs);

        tic
        fprintf('Processing %i patches \n',npatch)
        
        for p = 1:npatch
        %% Loop over patches
        % JIHG_CPXCOH requires the full coherence matrix. 
        % Often, the full stack of SLCS is too large to load fully into
        % memory. Hence, the stack has been split into patches, which are
        % processed individually.        
        
            % Load corners of patch
            l0 = patches(p,1);
            lN = patches(p,2);
            p0 = patches(p,3);
            pN = patches(p,4);            
        
            % size of patch
            npl         = lN - l0 + 1;            
            npp         = pN - p0 + 1;            
            slc_stack   = complex(zeros(npl*npp,nslcs));
            
            for j = 1:nslcs
            %% Load part of SLC stack
                slc = freadbk_quiet(path_slcs{j},nlines,'cpxfloat32',l0,lN,p0,pN);
                slc_stack(:,j) = slc(:);
                if mod(j,round(nslcs/10)) == 0
                    fprintf('Finished loading %i of %i slcs. [%i%% - %0.2f s] \n',j,nslcs,round(j/nslcs*100),toc)
                end
            end         
            
            patch_wins = patch_win_ixs{p};
            pf = 1;
            
            for i = patch_wins'
            %% Loop over windows in patches 
            
                ind_wrt_slc     = win_shp{i};
                
                [ir,ic]         = ind2sub([nlines,npixels],ind_wrt_slc);
                ind_wrt_patch   = sub2ind([npl,npp],ir-l0+1,ic-p0+1);
                
                % win_f: 1 -> ps, 2 -> poly with too few SHPs (all pixels
                % selected) 3 -> poly with SHPs, 0 -> rest, -1 -> empty
                % window
                if win_f(i) == 1 % Average phase of the PS inside the window
                    
                    cpxval                    = slc_stack(ind_wrt_patch,:);
                    bad_epoch_chk             = sum(abs(cpxval),1) == 0 | any(isnan(cpxval),1); % Check for epoch with only zeros
                    cpxval(:,bad_epoch_chk)   = [];
                    
                    cpx_ps_val       = exp(1j*angle(cpxval));
                    cpx_ps           = cpx_ps_val.'*conj(cpx_ps_val);
                    cpx_ps           = cpx_ps.';                    
                   
                    
                    ifgs_sm(i,~bad_epoch_chk(2:end))   = angle(cpx_ps(2:end,1));
                    ifgs_dc(i,~bad_epoch_chk(2:end))   = angle(diag(cpx_ps,-1));
                    coh(i,~bad_epoch_chk(2:end))       = 0.99;
                    gamma_pta(i)                       = 0.99;
                    bad_epochs(i,:)                    = bad_epoch_chk;
                    
                elseif win_f(i) == 0 || win_f(i) == 2 || win_f(i) == 3

                    
                    % select pixel stacks in window
                    cpxval                    = slc_stack(ind_wrt_patch,:);
                    bad_epoch_chk             = sum(abs(cpxval),1) == 0 | any(isnan(cpxval),1); % Check for epoch with only zeros or nans
                    cpxval(:,bad_epoch_chk)   = [];
                    
                    % calculate full complex coherence
                    cpx_sum = cpxval.'*conj(cpxval); % PC: .' is the non-conjugate transpose
                    abs_sum = sum(abs(cpxval).^2);
                    norm    = sqrt(abs_sum'*abs_sum);
                    cpxcoh  = cpx_sum.'./norm; % transposed to make columns master - slave.
                    %                                              ^--------wtf does this even mean
                  
                    % PC: added correct coherence calculation
%                     cpxcoh  = cpx_sum./norm;
                    
%                    % PC: alternative coherence calculation (for checking: easier to read)
%                     meanPower = mean( abs(cpxval).^2, 1 );
%                     cpxcohA = zeros(length(meanPower));   
%                     for a = 1:length(meanPower)
%                         for b = 1:length(meanPower)
%                             cpxcohA(a,b) = mean(cpxval(:,a).*conj(cpxval(:,b))) / sqrt(meanPower(a)*meanPower(b));
%                         end
%                     end
                    
                    % PC: added for debugging
                    % Plot the coherence matrix
%                     figure
%                     imagesc(abs(cpxcohF))
                    
                    nslcs_part = size(cpxcoh,2);
                    % phase-linking method
                    if strcmp(method,'EMI')
                        [cpxcoh_opt] = jihg_ph_est_emi(cpxcoh);              
                    elseif strcmp(method,'PTA')
                        [cpxcoh_opt] = jihg_ph_est_pta(cpxcoh);        
                    elseif strcmp(method,'EVD')
                        [cpxcoh_opt] = jihg_ph_est_evd(cpxcoh);
                    end
                    
                    % ensemble temporal coherence
                    uptri           = find(triu(ones(size(cpxcoh)),1)); 
                    gamma_pta(i)    = 2/(nslcs_part^2-nslcs_part)*real(nansum(exp(1i*...
                        (angle(cpxcoh_opt(uptri)) - angle(cpxcoh(uptri))))));
                    
                    % check if something went wrong 
                    if gamma_pta(i) < 0 || gamma_pta(i) > 1 % (can't happen)
                        % try, might be problem with inversion of coh
                        [cpxcoh_opt] = jihg_ph_est_evd(cpxcoh);
                        
                        gamma_pta(i)    = 2/(nslcs_part^2-nslcs_part)*real(nansum(exp(1i*...
                            (angle(cpxcoh_opt(uptri)) - angle(cpxcoh(uptri))))));
                        if gamma_pta(i) < 0 || gamma_pta(i) > 1
                            cpxcoh_opt = cpxcoh_opt*0;
                            gamma_pta(i) = 0;
                        end
                    end
                    
                    % Cramer-Rao Bound Covariance Matrix 

                    CRBQ = jihg_CRBQ(cpxcoh,nshp(i));
                    
                    % propagate to DC
                    % A matrix is only correct if master is slc 1.
                    A = eye(size(CRBQ));
                    for n = 1:size(A,2)-1
                        A(n+1,n) = -1;
                    end
                    
                    CRB_sm(i,~bad_epoch_chk(2:end)) = diag(CRBQ);
                    CRB_dc(i,~bad_epoch_chk(2:end)) = diag(A*CRBQ*A');
                    if strcmp(CRB,'full') % This takes up a lot of storage!
                        
                        if ~(exist(fullfile([output_path,'/CRBQs/']),'dir') == 7)
                            mkdir([output_path,'/CRBQs/'])
                        end
                        save_name = ['CRBQ_win',num2str(i)];
                        save([output_path,'/CRBQs/',save_name],'CRBQ');
                        
                    end
                    
                    % Store phase-linked cpxcoh correctly
                    % Perhaps not store both for very large data.

                    ifgs_sm(i,~bad_epoch_chk(2:end))   = angle(cpxcoh_opt(2:end,1));
                    ifgs_dc(i,~bad_epoch_chk(2:end))   = angle(diag(cpxcoh_opt,-1));
                    coh(i,~bad_epoch_chk(2:end))       = abs(diag(cpxcoh,-1)); 
                    bad_epochs(i,:)                    = bad_epoch_chk;

                    
                    nwin_patch = length(patch_wins); % Number of windows varies per patch.
                    prnt_ind   = patch_wins(round(linspace(1,nwin_patch,5))); 
                    if ismember(i,prnt_ind)
                        fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',round(pf*nwin_patch/5),nwin_patch,pf*20,toc)
                        pf = pf+1;
                    end
                end
                
            end
            clearvars slc_stack
            fprintf('Finished patch %i of %i. [%0.2f s] \n',p,npatch,toc)
        end
        
        ifg_ix_sm = [ones(nifgs,1) (2:nslcs)'];
        ifg_ix_dc = [(1:nslcs-1)' (2:nslcs)'];
        
        %% Output
        output.ifgs_dc      = ifgs_dc;
        output.ifgs_sm      = ifgs_sm;
        output.coh          = coh;
        output.gamma_pta    = gamma_pta;
        output.nifgs        = nifgs;
        output.ifg_ix_sm    = ifg_ix_sm;
        output.ifg_ix_dc    = ifg_ix_dc;
        output.CRB_sm       = CRB_sm;
        output.CRB_dc       = CRB_dc;
        
        save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.stackinfo.nifgs = output.nifgs;
    S.stackinfo.ifg_ix_dc = output.ifg_ix_dc;
    S.stackinfo.ifg_ix_sm = output.ifg_ix_sm;
    S.options.CRB_path = [output_path,'/CRBQs/'];
    clearvars -except S output
    
end