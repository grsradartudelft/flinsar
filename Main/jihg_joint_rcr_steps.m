function [S,output] = jihg_joint_rcr_steps(S,input)
% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.

    s               = dbstack;    
    fun_name        = s(1).name;
    output_path     = S.options.output_path;
    rcr             = input.rcr;
    save_name       = [fun_name,'_output',num2str(rcr),'.mat'];  
    if S.sim_flag == 1
        save_name       = ['Simulation/',fun_name,'_sim_output',num2str(rcr),'.mat'];
    end

    if ~(exist(fullfile(output_path,save_name),'file')==2) ||  input.overwrite
    %% Otherwise run function
        % read input
        tsr_unw         = input.tsr_unw;
        coh             = input.tsr_coh;
        CRB             = input.CRB;
        nwin            = S.win.n;
        win_f           = S.win.win_f;
        est_param       = input.est_param;
        est_param_final = zeros(nwin,3); % need to make this depend on final model for pixels;

        default = 1; % currently only 2 models, and 1 being the default
        win_model = input.win_model;
        
        
        %% Remove step
        not_used = isinf(CRB);   
        
        ifgs_r = zeros(size(tsr_unw));

        models = zeros(size(tsr_unw));        
        for i = 1:nwin
            wm = win_model(i);
            A = input.design_matrix{wm};
            model = A*est_param{wm}(i,:)';%            
            models(i,:) = model - model(1);

            % subtract model
            phi = exp(1i*tsr_unw(i,:)); % SM
            phi_mod = exp(1i*models(i,:));
            phi_r = phi.*conj(phi_mod);
            ifgs_r(i,:) = angle(phi_r);

        end
        

        %% Compute
        input.ifgs  = ifgs_r;
        input.coh   = coh;
        input.rcr   = rcr;
        input.joint = 1;
        [S,output]  = jihg_unwrap_rcr(S,input);   
        % add option to use nnb interpolation before unwrapping
%         [S,output]  = jihg_unwrap_nnb_rcr(S,input);       

        %% Restore

        phi_flat= output.ifgs_uw;
        % add back
        tsr_rcr = phi_flat + models;
        %% est       
        % correct any unwrapping mistakes and estimate through improved
        % unwrapped results
        tsr = zeros(size(phi_flat));
        tsr_est = zeros(size(phi_flat));        
        
        tic
        for i = 1:nwin      
        
            used = ~not_used(i,:);
            if sum(used) ==0
                % simple check to see whether or not the window was used
                continue
            end
            
            if strcmp(CRB,'full') && sum(used) > 0
                Q = zeros(nifgs);
                Q_in = load([S.options.CRB_path,'CRBQ_win',num2str(i)],'CRBQ'); Q_in = Q_in.CRBQ;
                Q(used,used) = Q_in;
                Q(diag(diag(Q) == 0)) = 9999;
                [~,p] = chol(Q);
                if p > 0
                    % default using just variances
                    Q = 1./CRB(i,:).';
                    fprintf('Not positive definite\n')
                end   
            else
                % default using just variances
                Q = 1./CRB(i,:);
            end
            
            wm = win_model(i);
            A = input.design_matrix{wm};
            tsr_in = tsr_rcr(i,:);
            est = lscov(A,tsr_in',Q);
            
            
            %%  Did the model fit?
            if wm == 2 && est(2) < 0
                
                tsr_tmp = jihg_solve_ambiguity(tsr_in',A*est,2*pi);
%                 tsr(i,:) = tsr_tmp;
                est_param{wm}(i,:) = lscov(A,tsr_tmp,Q); 

            else
            %% use default model
                wm = win_model(default);
                A = input.design_matrix{wm};
                model = A*est_param{wm}(i,:)';
                model = model' - model(1);
                tsr_in = phi_flat(i,:) + model;
                
                est = lscov(A,tsr_in',Q);
                tsr_tmp = jihg_solve_ambiguity(tsr_in',A*est,2*pi);
                est_param{wm}(i,:) = lscov(A,tsr_tmp,Q); 

            end
            %% correct original tsr with estimate
%             tsr_tmp = jihg_solve_ambiguity(tsr_in',A*est_param{wm}(i,:)',2*pi);
            tsr_tmp = jihg_solve_ambiguity(tsr_unw(i,:)',A*est_param{wm}(i,:)',2*pi);           
            
            tsr(i,:) = tsr_tmp;
            if wm == 2
                A = [input.design_matrix{1} input.design_matrix{2}(:,2)]; % should be function
                est_param_final(i,:) = lscov(A,tsr(i,:)',Q);
                tsr_est(i,:) = A*est_param_final(i,:)';
                
            else
                est_param_final(i,1:2) = lscov(A,tsr(i,:)',Q);
                tsr_est(i,:) = A*est_param_final(i,1:2)';
            end

            new_win_mod(i) = wm;

                if mod(i,round(nwin/10)) == 0
                    fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
                end
        end


        output.tsr_unw_rcr = phi_flat;
        output.tsr_unw = tsr;
        output.est_param = est_param; % used for RCR
        output.est_param_final = est_param_final;
        output.tsr_est = tsr_est;
        output.model = model;
        output.win_model = new_win_mod;
%         output.model_used = model_used;

        save([output_path,save_name],'-struct','output');
    else
        output = load([output_path,save_name]);
    end
end