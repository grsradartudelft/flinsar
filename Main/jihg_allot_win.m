function [S,output] = jihg_allot_win(S)
%JIHG_ALLOT_WIN creates multi-looking windows. Pixels inside a window can
%be filtered based on presets. 

% Author: FMG HEUFF - 02/2020.

geocoords = S.options.geocoords; % Check options. (Geocoordinates currently only option)
if geocoords%%
    % this function makes a grid
    [S,output_geo] = jihg_aw_geocoords(S);
end

polycoords = S.options.polycoords;
if polycoords
    % this function requires a polygon file with the defined shapes that
    % will be used in the complex averaging
    [S,output_geo_poly] = jihg_poly_geocoords(S);
end

% if patch
    % makes patches to help your PC with handling the large amounts of
    % data.
    [S,output_patches] = jihg_make_patches(S,output_geo);
% end

shp_flag = S.options.shp_flag;
if shp_flag
%%   Selects the SHPs or statistically homogenous pixels (brothers/siblings)     
    [S,output_amp]    = jihg_amp_fun(S); % probably already done, just loads the output
    [S,output_shps]     = jihg_sel_shps(S,output_geo,output_amp);
    % this one needs a switch still
%     [S,output_shps_poly]   = jihg_sel_shps_poly(S,output_geo_poly,output_amp);
    % calculate the geocoordinates of windows
    [S,~]             = jihg_win_bp_la(S,output_shps,output_geo);

end

%% Output
    output.patches = output_patches;
    output.shps = output_shps;
    if polycoords
        output.shps_poly = output_shps_poly;
        output.patches_poly = output_geo_poly.patches_poly;
    end
    
    clearvars -except S output
end