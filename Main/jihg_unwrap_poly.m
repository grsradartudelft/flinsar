function [S,output] = jihg_unwrap_poly(S,input_grid,input_poly)
%JIHG_UNWRAP Unwraps interferograms using SNAPHU

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    save_name       = [fun_name,'_output.mat'];
  
     if ~(exist(fullfile(output_path,save_name),'file')==2)
     %% Otherwise run function.
        % Read input

        nifgs           = S.stackinfo.nifgs;
        lambda          = S.options.lambda;
        unwrap_method   = S.options.unwrap_method;
        nwin            = S.win.n;
        nlines          = S.win.nlines;
        npixels         = S.win.npixels;
        ESM_EDC         = S.options.pl_ESM_EDC;
        ps_flag         = S.options.ps_flag;
        inc_rem         = S.options.inc_rem;
        win_f           = S.win.win_f;
        
        
        if strcmp(ESM_EDC,'EDC')
            ifgs_grid        = input_grid.ifgs_dc;
            ifgs_poly        = input_poly.ifgs_dc;
        else
            ifgs_grid        = input_grid.ifgs_sm;
            ifgs_poly        = input_poly.ifgs_sm;
        end
        
        if ps_flag == 1
            ps_name = S.options.ps_software;
            if strcmp(ps_name,'depsi')
                [S,output_atmo] = jihg_depsi_atmosphere(S);
                [S,output_atmo_poly] = jihg_depsi_atmosphere_poly(S);
            end
        end

        coh           = input_grid.coh; 
        coh_poly      = input_poly.coh; 
        win_atmos     = output_atmo.win_atmos_dc;
        poly_atmos    = output_atmo_poly.win_atmos_dc;
        
        %%
        %remove fields in grid
        in_p = S.win.win_f == 3;

        
        % then find location of polygons within grid cells
        win_info = load([output_path,'jihg_poly_geocoords_output.mat']);
        
        lat_range = S.win.lat_range;
        lon_range = S.win.lon_range;
        dlat = S.win.dlat;
        dlon = S.win.dlon;
        
        ifgs_cpx = exp(1i*ifgs_grid);
        ifgs_coh = coh;
        ifgs_cpx(in_p,:) = 0;
        ifgs_coh(in_p,:) = 0.01;
        ifgs_cnt = double(abs(angle(ifgs_cpx(:,1))) > 0);
        for i = 1:S.win.npoly
            ix1 = find(win_info.win_lat(i) + dlat/2 >= lat_range & win_info.win_lat(i) - dlat/2 < lat_range);
            ix2 = find(win_info.win_lon(i) + dlon/2 >= lon_range & win_info.win_lon(i) - dlon/2 < lon_range); 
            ix = sub2ind([S.win.nlines,S.win.npixels],ix1,ix2);
            ifgs_cpx(ix,:) = ifgs_cpx(ix,:) + exp(1i*ifgs_poly(i,:));
            ifgs_coh(ix,:) = ifgs_coh(ix,:) + coh_poly(i,:);
            ifgs_cnt(ix) = ifgs_cnt(ix) + 1;
        end
        
        ifgs_coh(ifgs_cnt>0,:) = ifgs_coh(ifgs_cnt>0,:)./ifgs_cnt(ifgs_cnt >0);
        
        %% MAIN
        %make snaphu.conf.in
        system('echo INFILE  phase.in > snaphu.conf.in');        
        system('echo INFILE  phase.in > snaphu.conf.in');
        system(['echo LINELENGTH ',num2str(npixels),' >> snaphu.conf.in']);
        system('echo OUTFILE phase.out >> snaphu.conf.in');
        system('echo CORRFILE coherence.in >> snaphu.conf.in');
        system('echo STATCOSTMODE DEFO >> snaphu.conf.in');
        system(['echo INITMETHOD ',unwrap_method,' >> snaphu.conf.in']);
        system('echo INFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo OUTFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo CORRFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo EARTHRADIUS 6378000.0 >> snaphu.conf.in');
        system(['echo LAMBDA ',num2str(lambda/1000),' >> snaphu.conf.in']);
    %     system(['!echo "NCORRLOOKS ' num2str(ncorrlooks) '" >> snaphu.conf.in']);
    
    
        % Currently JIHG_CPXCOH outputs the ESM.
        uw_ph   = zeros(nwin,nifgs); 
        uw_ref  = zeros(nifgs,1);
        ifg     = angle(ifgs_cpx);
        
        % in case we want to exclude pixels
        if inc_rem ==0
            excl = win_f == 0;
            ifgs_coh(excl,:) = 0.01;
        end
        
        % provided reference
        [ref_ind] = jihg_unw_setref(S);
        [l_grd,p_grd] = ndgrid(1:nlines,1:npixels);
        
        tic
        for i = 1:nifgs
    %         unwrap by snaphu
            
            if ps_flag == 1
                ifg_in = angle(exp(1j*ifg(:,i)).*conj(exp(1j*win_atmos(:,i))));
            else
                ifg_in = ifg(:,i);
            end
            
            coh_in = ifgs_coh(:,i);            
            
            out = coh_in < 0.15 & in_p;
            
            v = ifg_in(~out & in_p);
            x = p_grd(~out & in_p);
            y = l_grd(~out & in_p);
            xq = p_grd(out);
            yq = l_grd(out);
            
            vq = griddata(x,y,v,xq,yq,'nearest');
            
            ifg_in(out) = vq;
            
            fwritebk(reshape(ifg_in,nlines,npixels),'phase.in','float32');
            fwritebk(reshape(coh_in,nlines,npixels),'coherence.in','float32');
    
            system('snaphu -f snaphu.conf.in -s >> snaphu.log');
            
            uw          = freadbk_quiet('phase.out',nlines,'float32');            
            uw_ref      = uw - mean(uw(ref_ind));            
            uw_ph(:,i)  = uw_ref(:);
            
%             figure(11); imagesc(reshape(ifg_in,nlines,npixels))
%             figure(12); imagesc(uw_ref)
%             figure(13); imagesc(reshape(out,nlines,npixels))
%             figure(14); imagesc(reshape(~out & in_p,nlines,npixels))
%             tip

            
            
            if mod(i,round(nifgs/10)) == 0
                fprintf('Finished %i of %i ifgs. [%i%% - %0.2f s] \n',i,nifgs,round(i/nifgs*100),toc)
                
            end

        end        
        
        delete('phase.in'); delete('coherence.in'); delete('phase.out'); delete('snaphu.conf.in');
        delete('snaphu.log');
        
        output.ifgs_uw = uw_ph;
        output.uw_ref = uw_ref;
        output.ifgs_coh = coh;
        output.CRB_sm  = input_grid.CRB_sm;
        output.CRB_dc  = input_grid.CRB_dc;
        save([output_path,save_name],'-struct','output');
        
        
    
    else
        output = load([output_path,save_name]);
    end

    clearvars -except S output
end
    