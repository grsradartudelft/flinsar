function [S] = jihg_read_stackinfo(S)
%JIHG_READ_STACK Reads DORIS 5.0 output preprocessed for JIHG.

%   Input: Structure with the required variables. See JIHG_MAIN.
%   - S.options.stack_path      - Path to dir containing preproc. DORIS 5.0 output 
%   - S.options.stack_id        - ID Tag
%   - S.options.master_date     - Master date of the stack

%   Output: Structure containing paths to data.

% Author: FMG HEUFF - Feb 2020.

    path                = S.options.stack_path;
    id_name             = S.options.stack_id;
    master_date         = S.options.master_date;
    start_date          = S.options.start_date;
    end_date            = S.options.end_date;
    excl_dates          = S.options.exclude_dates;  
    
    if ~strcmp(path(end),'/') && ~strcmp(path(end),'/')
        path = [path,'/'];
    end
    % open dates and nlines and npixels
    date_range = str2double(start_date):str2double(end_date);   
    
    dates_orig = load([path,'dates.txt']);
    dates_in = ismember(dates_orig,date_range);
    dates_out = ismember(dates_orig,cellfun(@str2double,excl_dates));

    dates = dates_orig(dates_in & ~dates_out);
    dates_num = datenum(num2str(dates),'yyyymmdd');
    master_ix = dates == str2double(master_date);
    nlines = load([path,'nlines_crp.txt']);
    npixels = load([path,'npixels_crp.txt']);
    
    fl = nlines(2); ll = nlines(3); fp = npixels(2); lp = npixels(3);
    nlines = nlines(1);
    npixels = npixels(1);
    nslcs = length(dates);


    id_name = ['_',id_name];
    % create list of required files
    stackinfo.path_slcs = cellstr([repmat(path,nslcs,1) num2str(dates) repmat(['/slc_srd',id_name,'.raw'],nslcs,1)]);
    stackinfo.path_coords = {[path,master_date,'/lam',id_name,'.raw'];[path,master_date,'/phi',id_name,'.raw']};
    stackinfo.path_dem = [path,master_date,'/dem_radar',id_name,'.raw'];
    stackinfo.path_resfiles = cellstr([repmat(path,nslcs,1) num2str(dates) repmat('/slave.res',nslcs,1)]);
    stackinfo.path_resfiles(master_ix) = cellstr([path,master_date,'/master.res']);
    
    stackinfo.nlines = nlines;
    stackinfo.npixels = npixels;
    stackinfo.scene_corners = [fl,fp,ll,lp];
    stackinfo.dates_stack = dates_orig;
    stackinfo.dates = dates;
    stackinfo.dates_num= dates_num;
    stackinfo.nslcs = nslcs;
    stackinfo.dropped = dates_out;
    
    clearvars -except S stackinfo
    S.stackinfo = stackinfo;
end

