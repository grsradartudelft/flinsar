function [S,output]= jihg_rcr_model_w_amb_fn(S,input)
    
    nwin = S.win.n;
    coh = input.tsr_coh;
    tsr = input.tsr_unw;
    nifgs = S.stackinfo.nifgs;
    method= S.options.model;
    dates_num = S.stackinfo.dates_num;
    dates = datetime(datestr(dates_num));
    output_path = S.options.output_path;
    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    tic
        if strcmp(method,'CRFS')        
            T = jihg_calc_crfs_model(S);
        end

        T2 = T(dates,:);     
        model = T2.('model');
        model = smooth(model,5);
        A = [ones(nifgs,1) model];

        est_param = zeros(nwin,size(A,2));
        for i = 1:nwin

            y = tsr(i,:);        
            W=(coh(i,:).^2) ./ (1-coh(i,:).^2);% Fisher Information as weights
            W(isinf(W)) = 0;
    %         W = ones(1,nifgs);
            p_min = [-0.5*pi, -0.1];
            p_max = [0.5*pi, 0];
            p_step = [(p_max(1) - p_min(1))/100,(p_max(2) - p_min(2))/100];

            [est_param(i,:)]=jihg_ambiguity_fun(y,A,W,p_min,p_max,p_step);
            
            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
            end

        end

        model = A(:,2)*est_param(:,2)';

        output.est_param = est_param;
        output.model = model;
        output.tsr = tsr;
        output.coh = coh;

        save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end

end

