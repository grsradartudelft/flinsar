function [ref_ind] = jihg_unw_setref(S)

    geocoords = S.options.geocoords;
    ref_ll    = S.options.ref_ll;
    ref_rad    = S.options.ref_rad;
    ps_flag    = S.options.ps_flag;
    
    if geocoords 
        [~,output] = jihg_aw_geocoords(S);
    end
    
%     win_lon = output.win_lon;
%     win_lat = output.win_lat;

%     
    win_rdx = output.win_rdx;
    win_rdy = output.win_rdy;
    
    if ps_flag == 1
        is_ps = S.win.win_f == 1;
        win_rdx(~is_ps) = Inf;
        win_rdy(~is_ps) = Inf;        
    end
    
    if ref_ll(1) < 100  
        main_dir       = S.options.main_dir;
        if count(py.sys.path,[main_dir,'/jihg_modules/Utilities']) == 0            
            insert(py.sys.path,int32(0),[main_dir,'/jihg_modules/Utilities'])
        end
        xy = py.wsg2rd.wsg2rd(ref_ll(1),ref_ll(2));
        ref_xy = [xy{1},xy{2}];
    end
    
    dist_sq=(win_rdx-ref_xy(1)).^2+(win_rdy-ref_xy(2)).^2; 
    ref_ind=dist_sq<=ref_rad^2;    
    if ~any(ref_ind)
        if ps_flag == 1
            error('No PS windows near reference location, increase radius or change location. \n');
        else
            error('No windows near reference location, increase radius or change location. \n');
        end
    end
end