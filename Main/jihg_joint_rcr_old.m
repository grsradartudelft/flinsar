function jihg_joint_rcr(sim_flag)

S.options = load('jihg_J');
main_dir = S.options.main_dir;
joint_output_folder = S.options.joint_output_folder;
S.options.output_path = [main_dir,'/',joint_output_folder,'/'];
jihg_init(S);
folders = S.options.output_folder;

db               = dbstack;
if length(db) == 1
    sim_flag = 0;
end


nf = length(folders);
% nf =4 ;
stack = struct('est_param',{},'los2vert',{},'design_matrix',{},'tsr_unw',{},'tsr_coh',{},'win_model',{},'ifg_unw',{},'CRB',{},'ref_ix',{},'est_param_final',{});
for i = 1:nf
    input_folder = [main_dir,'/',folders{i},'/'];
    S = load([input_folder,'S']);
    if ~isfield(S,'steps')
        jihg_steps;
        S = load([input_folder,'S']);
    end
    S.sim_flag = sim_flag;
    try
        output_tsr = load(S.steps.est_tsr_name);        
    catch
        jihg_steps
        output_tsr = load(S.steps.est_tsr_name);
    end 
    
    if sim_flag == 1
        output_tsr = load('Simulation/jihg_est_tsr_sim_output');
    end
    
    
    [S,output_t_unw]      = jihg_temporal_unwrap(S,output_tsr); 
    [S,output_model]      = jihg_poly_model(S,output_tsr);     
    
    EP1             = output_t_unw.est_param;
    EP2             = output_model.est_param;
    
    win_model       = ones(S.win.n,1);
    win_model(output_model.in_p) = 2; 

    
    DM1     = output_t_unw.design_matrix;
    DM2     = output_model.design_matrix; 
%     DM2(:,2) = DM2(:,2)*0.0001;    
    
    los2vert =  1./cosd(S.win.inc);
    s.est_param = {EP1,EP2};
    s.los2vert = los2vert;
    s.design_matrix = {DM1, DM2};
    s.tsr_unw = output_tsr.tsr_unw;
    s.ifg_unw = output_tsr.ifg_unw;
    s.tsr_coh = output_tsr.tsr_coh;
    s.win_model = win_model;
    s.CRB = output_tsr.CRB_sm;
    s.ref_ix = S.poly_model.ref_ix;
    s.est_param_final = 0;
    stack(i) = s;
    
end

nwin = S.win.n;

n_models = length(stack(1).est_param);
npar1 = size(DM1,2); npar2 = size(DM2,2);

avg_est_params = cell(1,n_models);
for j = 1:n_models
    nparam = size(stack(1).est_param{j},2);
    est_params = zeros(nwin,nparam);
    for i = 1:nf


    %     est_params = est_params + output.(['tsr',num2str(i)]).est_param_tsr.*...
    %         los2vert.(['tsr',num2str(i)]);  
        est_params = est_params + stack(i).est_param{j}.*stack(i).los2vert;
    end
        avg_est_params{j} = est_params/nf;
end

avg_est_params_final = cell(1,n_models);
for j = 1
    rcr = j;    
    for i = 1:nf
        input_folder = [main_dir,'/',folders{i},'/'];
        S = load([input_folder,'S']); 
        S.sim_flag = sim_flag;
        est_param = cell2mat(avg_est_params)./stack(i).los2vert;
        input.tsr_unw = stack(i).tsr_unw;
        input.ifg_unw = stack(i).ifg_unw;
        input.tsr_coh = stack(i).tsr_coh;
        input.CRB = stack(i).CRB;
        input.design_matrix = stack(i).design_matrix;
        input.est_param = {est_param(:,1:npar1),est_param(:,npar1+1:npar1+npar2)};
        input.rcr = rcr;
        input.n_models = n_models;
        input.win_model = stack(i).win_model;
        input.npar = [npar1,npar2];
        
        input.overwrite = 0;
        
        [S,output] = jihg_joint_rcr_steps(S,input);
        s.est_param = output.est_param;
        s.est_param_final = output.est_param_final;
        s.tsr_unw = stack(i).tsr_unw;
        s.ifg_unw = stack(i).ifg_unw;
        s.tsr_coh = stack(i).tsr_coh;
        s.design_matrix = stack(i).design_matrix;
        s.CRB     = stack(i).CRB;
        s.ref_ix = stack(i).ref_ix;
        stack(i) = s;
    end
    
    for k = 1:n_models
        nparam = size(stack(1).est_param{k},2);
        est_params = zeros(nwin,nparam);
        est_params_final = zeros(nwin,3);
        for i = 1:nf


        %     est_params = est_params + output.(['tsr',num2str(i)]).est_param_tsr.*...
        %         los2vert.(['tsr',num2str(i)]);  
            est_params = est_params + stack(i).est_param{k}.*stack(i).los2vert;
            est_params_final = est_params_final + stack(i).est_param_final.*stack(i).los2vert;
        end
            avg_est_params{k} = est_params/nf;
            avg_est_params_final{k} = est_params_final/nf;
    end
end

tsr_all = [];
days_all = [];
ifg_ix_all = [];
model_all = [];
CRB = [];
stack_ids = [];
curr_slcs = 0;
for i = 1:nf
    
    input_folder = [main_dir,'/',folders{i},'/'];
    
    S = load([input_folder,'S']); 
    pta_out(:,i) = load([input_folder,'jihg_cpxcoh_output.mat'],'gamma_pta','CRB_sm');
    
    nwin            = S.win.n;
    win_f           = S.win.win_f;
    in_p            = win_f ==3 ;
    A               = stack(i).design_matrix{1};
    B               = stack(i).design_matrix{2};
    n_obs(:,i)      = S.win.nshp;
    np_A            = 1:size(A,2);
    np_B            = 1:size(B,2);
    est_ini         = avg_est_params_final{1}./stack(i).los2vert;
    dates_num       = S.stackinfo.dates_num(2:end);
    years           = year(datetime(datestr(dates_num)));
    winter          = month(datetime(datestr(dates_num))) == 1 ;
    ref             = min(years(winter)) == years & winter;
    
%     model_default = A*est_ini(:,1:npar1)' ;
%     model_peat    = B*est_ini(:,npar1+1:npar1+npar2)' ;
%     model = model_default;
%     model(:,in_p) = model_peat(:,in_p);
    model = [A B(:,2)]*est_ini';
    model = model';
    model = model - mean(model(:,ref),2);

    tsr_in = stack(i).tsr_unw;
    tsr_final = zeros(size(tsr_in));  
%     tsr_in = tsr_in - mean(tsr_in(:,ref),2);       
    for j = 1:nwin
        tsr_tmp = jihg_solve_ambiguity(tsr_in(j,:)',model(j,:)',2*pi);
        tsr_final(j,:) = tsr_tmp;
    end
        
 
    tsr_final = tsr_final.*stack(i).los2vert.*-0.0556*1000/4/pi;
    tsr_all = [tsr_all  tsr_final];
    days_all = [days_all; dates_num];
    CRB = [CRB pta_out(:,i).CRB_sm];
    ifg_ix_all = [ifg_ix_all; S.stackinfo.ifg_ix_sm + curr_slcs];
    curr_slcs = length(ifg_ix_all);
    model_all = [model_all  model.*stack(i).los2vert.*-0.0556*1000/4/pi];
    stack_ids = [stack_ids; ones(length(dates_num),1)*i];
    
%     figure(1);
%     plot(datetime(datestr(S.stackinfo.dates_num)),model(j,:));
%     hold on
%     plot(datetime(datestr(S.stackinfo.dates_num)),tsr_tmp,'o')
    
end
% hold off

    load([input_folder,'jihg_aw_geocoords_output.mat']);
    S.options = load('jihg_settings');
    epoch = datetime(datestr(days_all));
%     stack_name = S.options.stack_names(stack_ids)';

    win_names = join([string(repmat('win',nwin,1)) strtrim(string(num2str((1:nwin)')))],'');
    
    TSR = array2timetable(tsr_all','RowTimes',epoch,'VariableNames',win_names);
    TSR = retime(TSR,'regular','mean','TimeStep',days(6));
    VAR = array2timetable(CRB','RowTimes',epoch,'VariableNames',win_names);
    VAR = retime(VAR,'regular','mean','TimeStep',days(6));
    EST = array2timetable(model_all','RowTimes',epoch,'VariableNames',win_names);
    EST = EST(TSR.Time,:);


    lin_sub = avg_est_params_final{1}(:,2)*0.0556*1000/4/pi;
    model_amp = avg_est_params_final{1}(:,3)*0.0556*1000/4/pi;
    [win_p,win_l] = meshgrid(1:nwin_p,1:nwin_l);
    win_l = win_l(:);
    win_p = win_p(:);
    win_type = win_f;
    gamma_pta = [pta_out(1).gamma_pta pta_out(2).gamma_pta pta_out(3).gamma_pta]; 
    gamma_pta = mean(gamma_pta,2);
    
    AUX = table(win_names,win_p,win_l,win_lon,win_lat,win_rdx,win_rdy,lin_sub,model_amp,win_type,gamma_pta);
    DM = [];
    for i = 1:nf
        DM = [DM; stack(i).design_matrix{1} stack(i).design_matrix{2}(:,2)];
    end
    MODEL = timetable(DM,'RowTimes',epoch);
    
    MODEL = MODEL(TSR.Time,:);

    
    save(['results_',S.options.run_name],'TSR','VAR','AUX','MODEL','EST');
    



end