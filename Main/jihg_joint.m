function jihg_joint

S.options = load('jihg_J');
main_dir = S.options.main_dir;
joint_output_folder = S.options.joint_output_folder;
S.options.output_path = [main_dir,'/',joint_output_folder,'/'];
jihg_init(S);
folders = S.options.output_folders;


nf = length(folders);

for i = 1:nf
    input_folder = [main_dir,'/',folders{i},'/'];
    S = load([input_folder,'S']);
    if ~isfield(S,'steps')
        jihg_steps;
        S = load([input_folder,'S']);
    end
    
    try
        output.(['tsr',num2str(i)]) = load(S.steps.est_tsr_name);
    catch
        jihg_steps
        output.(['tsr',num2str(i)]) = load(S.steps.est_tsr_name);
    end  
    
    los2vert.(['tsr',num2str(i)]) = 1./cosd(S.win.inc);
end

nwin = S.win.n;
nparam = size(output.(['tsr',num2str(1)]).design_matrix,2);

est_params = zeros(nwin,nparam);
for i = 1:nf
     
    est_params = est_params + output.(['tsr',num2str(i)]).est_param_tsr.*...
        los2vert.(['tsr',num2str(i)]);   
end
    est_params = est_params/nf;
    
    
end