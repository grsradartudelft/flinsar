function [S,output] = jihg_unwrap_rcr(S,input)

    output_path     = S.options.output_path;

    
    s               = dbstack;
    fun_name        = s(1).name;

    iter_step             = input.iter_step;
    if isfield(input,'joint')
        save_name       = [fun_name,'_joint_output',num2str(iter_step),'.mat'];
        overwrite       = input.overwrite;
        
        if S.sim_flag == 1
            save_name       = ['Simulation/',fun_name,'_joint_sim_output',num2str(iter_step),'.mat'];
        end
        
    else
        save_name       = [fun_name,'_output',num2str(iter_step),'.mat'];
        overwrite = 1;
        if S.sim_flag == 1
            save_name       = ['Simulation/',fun_name,'_sim_output',num2str(iter_step),'.mat'];
        end
        
    end
    output.iter_step      = iter_step;

    
     if ~(exist(fullfile(output_path,save_name),'file')==2) || overwrite
        %make snaphu.conf.in
        nifgs           = S.stackinfo.nifgs;
        lambda          = S.options.lambda;
        unwrap_method   = S.options.unwrap_method;
        nwin            = S.win.n;
        nlines          = S.win.nlines;
        npixels         = S.win.npixels;

        ifgs            = input.ifgs;
        gamma_pta       = input.gamma_pta; 
        win_f_joint     = input.win_f_joint;
        
        system('echo INFILE  phase.in > snaphu.conf.in');        
        system('echo INFILE  phase.in > snaphu.conf.in');
        system(['echo LINELENGTH ',num2str(npixels),' >> snaphu.conf.in']);
        system('echo OUTFILE phase.out >> snaphu.conf.in');
        system('echo CORRFILE coherence.in >> snaphu.conf.in');
        system('echo STATCOSTMODE DEFO >> snaphu.conf.in');
        system(['echo INITMETHOD ',unwrap_method,' >> snaphu.conf.in']);
        system('echo INFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo OUTFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo CORRFILEFORMAT FLOAT_DATA >> snaphu.conf.in');
        system('echo EARTHRADIUS 6378000.0 >> snaphu.conf.in');
        system(['echo LAMBDA ',num2str(lambda/1000),' >> snaphu.conf.in']);
    %     system(['!echo "NCORRLOOKS ' num2str(ncorrlooks) '" >> snaphu.conf.in']);

       
        uw_ph=zeros(nwin,nifgs);       
        
        [l_grd,p_grd] = ndgrid(1:nlines,1:npixels);       
        
        ifg = ifgs;
        [ref_ind] = jihg_unw_setref(S);
        
        tic
        snaphu_path = S.options.snaphu_path; %PC: added this path
        for i = 1:nifgs
    %         unwrap by snaphu
            ifg_in = ifg(:,i);            
            coh_in = gamma_pta;   
            out = win_f_joint <0 | coh_in < 0 | isnan(ifg_in);
            
            if S.options.inc_rem == 1
                out = out | win_f_joint == 0;
            end
            
            ifg_v = ifg_in(~out);
            coh_v = coh_in(~out);
            x = p_grd(~out);
            y = l_grd(~out);
            xq = p_grd(out);
            yq = l_grd(out);
            
            ifg_vq = griddata(x,y,ifg_v,xq,yq,'nearest');      
            coh_vq = griddata(x,y,coh_v,xq,yq,'nearest');  
            
            ifg_in(out) = ifg_vq;
            coh_in(out) = coh_vq;
            
            fwritebk(reshape(ifg_in,nlines,npixels),'phase.in','float32');
            fwritebk(reshape(coh_in,nlines,npixels),'coherence.in','float32');
            system([snaphu_path 'snaphu -f snaphu.conf.in -s >> snaphu.log']);
            uw = freadbk_quiet('phase.out',nlines,'float32');

            uw_ref      = uw - mean(uw(ref_ind));
            uw_ref(out) = 0;
            uw_ph(:,i)  = uw_ref(:);
            
%             figure(11); imagesc(reshape(ifg_in,nlines,npixels))
%             figure(12); imagesc(uw_ref)
%             figure(13); imagesc(reshape(out,nlines,npixels))
%             figure(14); imagesc(reshape(~out,nlines,npixels))
            
            if mod(i,round(nifgs/10)) == 0
                fprintf('Finished %i of %i ifgs. [%i%% - %0.2f s] \n',i,nifgs,round(i/nifgs*100),toc)                
            end
            
%             figure(1); imagesc(reshape(ifg_in,nlines,npixels));
%             figure(2); imagesc(uw_ref);

        end  
        
        delete('phase.in'); delete('coherence.in'); delete('phase.out'); delete('snaphu.conf.in');
        delete('snaphu.log');
        
        output.ifgs_uw  = uw_ph;
        output.ifgs_coh = gamma_pta;
        save([output_path,save_name],'-struct','output');
    
    else
        output = load([output_path,save_name]);
    end


end
    