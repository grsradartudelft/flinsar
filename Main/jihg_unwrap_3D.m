function [S,output] = jihg_unwrap_3D(S,input)
%JIHG_UNWRAP Unwraps interferograms using SNAPHU

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    save_name       = [fun_name,'_output.mat'];
  
     if ~(exist(fullfile(output_path,save_name),'file')==2)
     %% Otherwise run function.
        % Read input

        nifgs           = S.stackinfo.nifgs;
        lambda          = S.options.lambda;
        unwrap_method   = S.options.unwrap_method;
        dates_num       = S.stackinfo.dates;
        nwin            = S.win.n;
        nlines          = S.win.nlines;
        npixels         = S.win.npixels;
        ESM_EDC         = S.options.pl_ESM_EDC;
        ps_flag         = S.options.ps_flag;
        inc_rem         = S.options.inc_rem;
        win_f           = S.win.win_f;        
        ESM_EDC = 'ESM';
        
        if ps_flag == 1
            ps_name = S.options.ps_software;
            if strcmp(ps_name,'depsi')
                [S,output_atmo] = jihg_depsi_atmosphere(S);
                
            end
        end
        

        if strcmp(ESM_EDC,'EDC')
            ifgs        = input.ifgs_dc;
            ifg_ix = S.stackinfo.ifg_ix_dc;
            win_atmos     = output_atmo.win_atmos_dc;
        else
            ifgs        = input.ifgs_sm;
            ifg_ix = S.stackinfo.ifg_ix_sm;
            win_atmos     = output_atmo.win_atmos_sm;
        end
        
        coh            = input.coh; 
        


        XY = load([output_path,'jihg_aw_geocoords_output.mat'],'win_rdx','win_rdy');
        
        xy = [XY.win_rdx,XY.win_rdy];
%         [l_grd,p_grd] = ndgrid(1:nlines,1:npixels);     

        [ref_ind] = jihg_unw_setref(S);        

        %% remove bad points and replace by nnb 
 
        out = win_f < 0;

        if S.options.inc_rem ~= 1
            out = out | win_f == 0;
        end       
        
            
%         ifg_v = ifgs(~out,:);
%         x = p_grd(~out);
%         y = l_grd(~out);
%         xq = p_grd(out);
%         yq = l_grd(out);
%         for i = 1:nifgs
%             ifg_vq = griddata(x,y,ifg_v(:,i),xq,yq,'nearest');
%             ifgs_in(out,i) = ifg_vq;
%         end
        
        ifgs_in = ifgs(~out,:);
        xy_in  = xy(~out,:);
        atmos_in = win_atmos(~out,:);
        uw_ph = zeros(size(ifgs));

            
            
        %% unwrap with 3D algorithm used in StaMPS (more than anything because I understand how it works).
%         uw_ph = ifgs;
        ph_w = angle(exp(1j*ifgs_in).*conj(exp(1j*atmos_in)));
        
        
        options=struct('master_day',dates_num(1));
        options.time_win=30;
        options.unwrap_method='3D_FULL';
        options.grid_size = S.options.win_size;
        options.prefilt_win=4;
        options.la_flag='n';
        options.scf_flag='n';

        %run StaMPS unwrapping method
        [ph_uw,msd]=uw_3d(ph_w,xy_in,dates_num,ifg_ix,[],options);
        uw_ph(~out,:) =  ph_uw;
        uw_ref   = mean(uw_ph(ref_ind,:));    % needed for sims
        uw_ph = uw_ph - uw_ref;
        %remove intermediate files
        delete('snaphu.costinfile');delete('snaphu.in');delete('snaphu.out')
        delete('uw_grid.mat');delete('uw_interp.mat');delete('uw_phaseuw.mat');delete('uw_space_time.mat');
        
%         %% compare
%         uw = load([output_path,'/jihg_unwrap_output_no_3D.mat']);
%         for i = 1:nifgs
%             figure(1); imagesc(reshape(uw_ph(:,i),S.win.nlines,S.win.npixels))
%             figure(2);imagesc(reshape(uw.ifgs_uw(:,i),S.win.nlines,S.win.npixels))
%         end
        

        
        output.ifgs_uw = uw_ph;
        output.uw_ref = uw_ref;
        output.ifgs_coh = coh;
        output.CRB_sm  = input.CRB_sm;
        output.CRB_dc  = input.CRB_dc;
        save([output_path,save_name],'-struct','output');
    
    else
        output = load([output_path,save_name]);
    end


end
    