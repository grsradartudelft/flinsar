function [CRBQ] = jihg_CRBQ(cpxcoh,L)

    
    coh = abs(cpxcoh);
    nslcs   = size(coh,1);   
    
    [~,p] = chol(coh);
    e     =1e-6;
    while p > 0
        coh     = coh+e*eye(nslcs);
        [~,p]   = chol(coh);
        e       = e*2;
    end               
   
    t       = eye(nslcs);
    t(:,1)  = [];
    X       = 2*L*(coh.*inv(coh)-eye(nslcs));
    CRBQ    = pinv(t'*X*t);
    
    tri_up  = triu(ones(nslcs-1)) == 1;
	CRBQ_t  = CRBQ.';
    CRBQ(tri_up) =  CRBQ_t(tri_up);   
    


end