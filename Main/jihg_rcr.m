function [S,output] = jihg_rcr(S,input)
% Author: FMG HEUFF - 03/2020.
    
%% If output exists, load it instead.

    s               = dbstack;    
    fun_name        = s(1).name;
    output_path     = S.options.output_path;
    iter_step       = input.iter_step;
    save_name       = [fun_name,'_output',num2str(iter_step),'.mat'];  
    if S.sim_flag == 1
        save_name       = ['Simulation/',fun_name,'_sim_output',num2str(iter_step),'.mat'];
    end

    input.overwrite = 1;
    if ~(exist(fullfile(output_path,save_name),'file')==2) ||  input.overwrite
    %% otherwise run function    
        
    
        tsr_unw         = input.tsr_unw;
        gamma_pta       = input.gamma_pta;
        CRB             = input.CRB_sm;
        win_f           = input.win_f_joint;
        EP1_avg         = input.EP1_avg;
        EP2_avg         = input.EP2_avg;
        DM1             = input.DM1;
        DM2             = input.DM2;
        EP1             = input.EP1;
        EP2             = input.EP2;
        
        nwin            = S.win.n;
        rcr_types       = S.options.rcr_types;
        
        default         = 1;
        
        %% Remove Step
        
        % estimated time-series for the two models
        if strcmp(rcr_types,'all')
            est_EP1 = EP1_avg*DM1';       
            est_EP2 = EP2_avg*DM2';

            % wrap the original unwrap timeseries and estimed tsr
            tsr_w       = angle(exp(1j*tsr_unw)); %        
            est_EP1_w   = angle(exp(1j*est_EP1));
            est_EP2_w   = angle(exp(1j*est_EP2));

            % remove estimate

            tsr_w_EP1    = tsr_w.*conj(est_EP1_w);
            tsr_w_EP2    = tsr_w.*conj(est_EP2_w);
        else
            
            est_EP1 = zeros(size(tsr_unw));  % subtract nothing      
            est_EP2 = EP2_avg*DM2';

            % wrap the original unwrap timeseries and estimed tsr
            tsr_w       = exp(1j*tsr_unw); %        
            est_EP1_w   = exp(1j*est_EP1);
            est_EP2_w   = exp(1j*est_EP2);

            % remove estimate

            tsr_w_EP1    = tsr_w.*conj(est_EP1_w);
            tsr_w_EP2    = tsr_w.*conj(est_EP2_w);            
            
        end
        
        
        %% Compute
        % create interferogram to be unwrapped
        
        input.ifgs          = angle(tsr_w_EP1 + tsr_w_EP2); % in tsr_joint_rcr the model used per window has already been set.
        input.coh           = repmat(gamma_pta,1,S.stackinfo.nslcs-1);
        input.iter_step     = iter_step;
        input.win_f_joint   = win_f;
        
        [S,output]          = jihg_unwrap_rcr(S,input); 
        
        %% Restore        
        tsr_unw_EP         = output.ifgs_uw;
        
%         v_res = lscov(DM1,tsr_unw_EP');
%         
% %         figure(1); imagesc(reshape(v1(2,:),S.win.nlines,S.win.npixels)); caxis([-10,10]);
% %         for i = 1:S.win.n            
% %             
% %             figure(2); imagesc(reshape(tsr_unw_EP(:,i),S.win.nlines,S.win.npixels)); caxis([-25,25]);
% %         end
% %         
% %         figure(3); plot(tsr_unw_EP(sub2ind([S.win.nlines,S.win.npixels],51,61),:))
%         
        tsr_rcr            = tsr_unw_EP + est_EP1 + est_EP2;
        
        %% est       
        % correct any unwrapping mistakes and estimate through improved
        % unwrapped results
        tsr_new     = zeros(size(tsr_rcr));
        est_EP1_new = zeros(size(tsr_rcr));           
        est_EP2_new = zeros(size(tsr_rcr)); 
        EP1_new     = zeros(size(EP1_avg));
        EP2_new     = zeros(size(EP2_avg));
        
        v_res       = zeros(size(EP1_avg,1),1);
        v_est       = zeros(size(tsr_rcr));
        
        dates_num       = S.stackinfo.dates_num(2:end);
        years           = year(datetime(datestr(dates_num)));
        winter          = any(month(datetime(datestr(dates_num))) == [1 2],2) ;
        ref             = 2018 == years & winter;     
        
        tic
        
        %% first do all windows with standard model
        for i = 1:nwin             
            
            tsr_win = tsr_rcr(i,:); 
            tsr_win = tsr_win - mean(tsr_win(ref));
                
            if win_f(i) == 1
                % unweighted least squares for PS
                est_param = lscov(DM1,tsr_win');
                est = DM1*est_param;             
                % correct unwrapping errors
                tsr_tmp = jihg_solve_ambiguity(tsr_win',est,2*pi);
                EP1_new(i,:) = lscov(DM1,tsr_tmp); 
            else
                if win_f(i) > -1
                    if strcmp(CRB,'full') && sum(used) > 0
                        Q = zeros(nifgs);
                        Q_in = load([S.options.CRB_path,'CRBQ_win',num2str(i)],'CRBQ'); Q_in = Q_in.CRBQ;
                        Q(used,used) = Q_in;
                        Q(diag(diag(Q) == 0)) = 9999;
                        [~,p] = chol(Q);
                        if p > 0
                            % default using just variances
                            Q = 1./CRB(i,:).';
                            fprintf('Not positive definite\n')
                        end   
                    else
                        % default using just variances
                        Q = 1./CRB(i,:);
                    end                

                    est_param = lscov(DM1,tsr_win',Q);
                    est = DM1*est_param;             
                    % correct unwrapping errors
                    tsr_tmp = jihg_solve_ambiguity(tsr_win',est,2*pi);
                    EP1_new(i,:) = lscov(DM1,tsr_tmp,Q); 
                end
            end


             
             % correct original tsr with new estimate
             est_EP1_new(i,:)   = DM1*EP1_new(i,:)';
             tsr_new(i,:)       = jihg_solve_ambiguity(tsr_unw(i,:)',est_EP1_new(i,:)',2*pi);  
             
             
            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
            end
        end
        
        %% now model 2 for polygons 
        
        % for visualization purposes
        EP_final = [EP1_new zeros(nwin,1)];
        DM_final        = [DM1 DM2(:,2)];
        for i = 1:nwin    
            tsr_win = tsr_rcr(i,:);                  
                
            if win_f(i) == 3
                if strcmp(CRB,'full') && sum(used) > 0
                    Q = zeros(nifgs);
                    Q_in = load([S.options.CRB_path,'CRBQ_win',num2str(i)],'CRBQ'); Q_in = Q_in.CRBQ;
                    Q(used,used) = Q_in;
                    Q(diag(diag(Q) == 0)) = 9999;
                    [~,p] = chol(Q);
                    if p > 0
                        % default using just variances
                        Q = 1./CRB(i,:).';
                        fprintf('Not positive definite\n')
                    end   
                else
                    % default using just variances
                    Q = 1./CRB(i,:);
                end                

%                 est_param = lscov(DM2,tsr_win',Q);
%                 est = DM2*est_param;

                est_param = lscov(DM_final,tsr_win',Q);
                est = DM_final*est_param;
                
                % correct unwrapping errors
                tsr_tmp = jihg_solve_ambiguity(tsr_win',est,2*pi);
%                 EP2_new(i,:) = lscov(DM2,tsr_tmp,Q); 
                temp = lscov(DM_final,tsr_tmp,Q); 
                EP2_new(i,:) = temp([1,3]);
                v_res(i,1) = temp(2);
                v_est(i,:) = DM_final(:,1:2)*temp([1,2]);
                

                % correct original tsr with new estimate
                est_EP2_new(i,:)   = DM2*EP2_new(i,:)';
%                 tsr_new(i,:)       = jihg_solve_ambiguity(tsr_unw(i,:)',est_EP2_new(i,:)',2*pi);  
                tsr_new(i,:)       = jihg_solve_ambiguity(tsr_unw(i,:)',DM_final*temp,2*pi);  
                
                EP_final(i,:) = lscov(DM_final,tsr_new(i,:)',Q);
            end

            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
            end
        end
        
        


        output.tsr_unw_rc = tsr_unw_EP;
        output.tsr_unw = tsr_new;
        output.EP1 = EP1_new; % used for RCR
        output.EP2 = EP2_new;
        output.v_res = v_res;
        output.v_est = v_est;
        
        output.est_EP1 = est_EP1_new;
        output.est_EP2 = est_EP2_new;
        output.DM_final = DM_final;
        
        output.EP_final = EP_final;
   
        
        save([output_path,save_name],'-struct','output');
    else
        output = load([output_path,save_name]);
    end


end