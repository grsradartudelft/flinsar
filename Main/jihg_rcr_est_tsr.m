function [S,output] = jihg_rcr_est_tsr(S,input)
    
    output_path     = S.options.output_path;
    ESM_EDC         = S.options.pl_ESM_EDC;
    nifgs           = S.stackinfo.nifgs;
    ifg_ix          = S.stackinfo.ifg_ix;
    dates_num       = S.stackinfo.dates_num;
    nwin            = S.win.n;
    
    ifgs_uw         = input.ifgs_uw;
    coh             = input.ifgs_coh;
    rcr             = input.rcr;
    
    s               = dbstack;
    fun_name        = s(1).name;
    save_name       = [fun_name,'_output_rcr',num2str(rcr),'.mat'];
    output.rcr      = input.rcr;
    
     if ~(exist(fullfile(output_path,save_name),'file')==2)
        
        if strcmp(ESM_EDC,'EDC')
            A = ([0;dates_num(ifg_ix(2:end,2)) - dates_num(ifg_ix(1:end-1,2))])/365.25;
        elseif strcmp(ESM_EDC,'ESM')
            A = [ones(nifgs,1) (dates_num(ifg_ix(:,2)) - dates_num(ifg_ix(:,1)))/365.25];
            warning('Not correctly implemented, will give errors');
        end
        
        
        est_param_ifg = zeros(nwin,size(A,2));
        tsr_est_ifg = zeros(nwin,nifgs);
        for i = 1:nwin
            ph_win = ifgs_uw(i,:)';
            est_param_ifg(i,:) = lscov(A,ph_win);
            tsr_est_ifg(i,:) = A*est_param_ifg(i,:)';
        end
        
         if strcmp(ESM_EDC,'EDC')
            tsr_unw = cumsum(ifgs_uw,2);
            A = [ones(nifgs,1) (dates_num(ifg_ix(:,2)) - dates_num(ifg_ix(1)))/365.25];
            est_param_tsr = zeros(nwin,size(A,2));
            tsr_est = zeros(nwin,nifgs);
            for i = 1:nwin
                ph_win = tsr_unw(i,:)';
                est_param_tsr(i,:) = lscov(A,ph_win);
                tsr_est(i,:) = A*est_param_tsr(i,:)';
            end
         end
        
        
        output.est_param_ifg = est_param_ifg;
        output.est_param_tsr = est_param_tsr;
        output.tsr_unw = tsr_unw; 
        output.tsr_coh = coh;
        output.tsr_est = tsr_est;
        output.tsr_est_ifg = tsr_est_ifg;
        
        save([output_path,save_name],'-struct','output');
    else
        output = load([output_path,save_name]);
    end
    

end