function [S] = jihg_init(S)
%JIHG_INIT  Checks if output might overwrite older results. Otherwise
%create necessary folders.
%   TODO: Overwrite check is not very robuust yet.

% Author: FMG HEUFF - Feb 2020.


output_path = S.options.output_path;
overwrite_check = S.options.overwrite_check;

if ~(exist(fullfile(output_path),'dir') == 7)
    mkdir(output_path)
    options = S.options;
    save([output_path,'jihg_settings.mat'],'-struct','options')
else
    if overwrite_check
        prompt = 'Do you want to continue with current run? (y/n)\n';
        answer = input(prompt,'s');
        if strcmp(answer,'y')                   
             fprintf('Reinitializing...\n')
        else
            prompt = 'Do you want to reset? (y/n)\n';
            answer = input(prompt,'s');
            if strcmp(answer,'y')                   
                fprintf('Resetting...\n')
                
                % windows command for deleting directory. More efficient
                % than matlab command.
                system(['rmdir /q/s ','"',output_path,'"']);
                mkdir(output_path);
                options = S.options;
                save([output_path,'jihg_settings.mat'],'-struct','options')
             
            else
                return
            end
                
        end
    else
        options = S.options;
        save([output_path,'jihg_settings.mat'],'-struct','options')
    end

end


end
