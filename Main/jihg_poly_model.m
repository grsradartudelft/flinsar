function [S,output]= jihg_poly_model(S,input)
    

%     rcr = input.rcr;
    
    s               = dbstack;
    if length(s) == 1
        jihg_joint_rcr
    end
    fun_name        = s(1).name;
    output_path = S.options.output_path;
    save_name       = [fun_name,'_output.mat'];
    
    if S.sim_flag == 1
        save_name       = [fun_name,'_sim_output.mat'];
        
    end
    
    if S.options.extensometer == 1
        save_name       = [fun_name,'_ext_output.mat'];
    end
    if ~(exist(fullfile(output_path,save_name),'file')==2)
        
    nwin = S.win.n;
    coh = input.tsr_coh;
    CRB = input.CRB_sm;
    tsr = input.tsr_unw;
    ifgs = input.ifg_unw;
    nifgs = S.stackinfo.nifgs;
    method= S.options.model;
    dates_num = S.stackinfo.dates_num;
    dates = datetime(datestr(dates_num(2:end)));

    in_p = S.win.win_f == 3;     
        tic
        if strcmp(method,'CRFS')        
            T = jihg_calc_crfs_model(S);
            if S.options.extensometer == 1
                T_ext = jihg_read_extensometer(S);
                T_ext_d = retime(T_ext,'daily','mean');
                T_ext_m = retime(T_ext,'monthly','mean');
                T_CRFS = T(T_ext_d.t,:);
                T_CRFS_m = retime(T_CRFS,'monthly','mean');
%                 x = T_ext_d.data(T_CRFS.Time);
%                 A = [ones(size(x)) T_CRFS.model];
%                 est = lscov(A,smooth(x,10));
                  CRFS_max = max(T_CRFS_m.model);
                  CRFS_min = min(T_CRFS_m.model);
                  ext_max = max(T_ext_m.data);
                  ext_min = min(T_ext_m.data);
                  amp_ext= abs(ext_max - ext_min);
                  amp_CRFS =abs(CRFS_max - CRFS_min);
                  amp = amp_ext/amp_CRFS;
                  
                  offset = T_CRFS.model(1)*amp - T_ext_d.data(1);
                  
                T.ext_CRFS = T.model*amp;
                add_ext =T_ext_d(T_CRFS.Time,:);
                T.ext_CRFS(T_CRFS.Time,:) = add_ext.data - max(add_ext.data(dates,:));% + offset;
                T2 = T(dates,:);
                model_intra_annual = T2.ext_CRFS;
                model_intra_annual = smooth(model_intra_annual,3);
                T2.model = model_intra_annual;
                  
                  
                
            else
                T2 = T(dates,:);
                model_intra_annual = T2.model;
            end
            
            
            
            
            model_intra_annual = smooth(model_intra_annual,3);
            T2.model = model_intra_annual;
            
            monthly_avg = retime(T2,'monthly','mean');
            first_half = ismember(month(monthly_avg.Time),1:6);
            year_max = retime(monthly_avg(first_half,:),'yearly','max');
            year_min = retime(monthly_avg,'yearly','min');            

            % don't use years starting in second part
            if size(year_max,1) ~= size(year_min,1)
                [~,x1] = setdiff(year(year_min.Time),year(year_max.Time));
                year_min(x1,:) = [];                
            end
            
            m_ix_max = ismember(monthly_avg.model, year_max.model);
            m_ix_min = ismember(monthly_avg.model, year_min.model);
            
            months_max = monthly_avg.Time(m_ix_max);
            months_min = monthly_avg.Time(m_ix_min);
            
            
            
            date_sel = []; yr_max_ref =[];nr_periods = 0;
            for i =  1 : length(months_max)
                if months_max(i) < months_min(i)
                    TR =timerange(months_max(i),months_min(i),'months');
                    Ttemp = T2(TR,:);
                    t = Ttemp.Time;
                    date_sel = [date_sel;t]; 
                    yr_max_ref = [yr_max_ref; repmat(year_max.model(i),length(t),1)];
                    nr_periods = nr_periods + 1;
                end
            end
             
            A = [ones(size(date_sel)) T2.model(date_sel) - yr_max_ref];
            [~,yr_ix] = ismember(year(date_sel),year(year_max.Time));
            B = zeros(length(date_sel),length(nr_periods));
            for i = 1:nr_periods
                B(yr_ix == i,i) = 1;
            end
            B = [B T2.model(date_sel) - yr_max_ref];
            
            est_param = zeros(nwin,2);
            for i = 1:nwin
                if in_p(i)
                    ph_in = tsr(i,:)';        
%                     W=(coh(i,:).^2) ./ (1-coh(i,:).^2);% Fisher Information as weights
%                     W(isinf(W)) = 0;
%                     %         W = ones(1,nifgs);
                    W = 1./CRB(i,:)';
                    if rank(W) == 0
                        W = ones(size(W));
                    end
                    
                    
                    T_win = timetable(dates,ph_in,W);
                    win_monthly_avg= retime(T_win,'monthly','mean');
                    avg_max_month = win_monthly_avg(m_ix_max,:);
                    T_sel = T_win(date_sel,:);                  
                    
                    ph_win_model = T_sel.ph_in - avg_max_month.ph_in(yr_ix);
                    est = lscov(B,ph_win_model,T_sel.W);
%                     est1 = lscov(B,ph_win_model);
                    est_param(i,:) = [mean(est(1:nr_periods)) est(end)];
                    
%                     figure(1); plot(T_sel.dates, ph_win_model); hold on; 
                    
%                     plot(T_sel.dates,T_sel.ph_in); 
%                     plot(T_sel.dates,B*est,'LineWidth',2)
%                     plot(T_sel.dates,B*est1,'--','LineWidth',2)
%                     hold off
%                     
%                       1+1

                else


                end

                if mod(i,round(nwin/10)) == 0
                    fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
                end

            end

                  
            
            
            B = [ones(nifgs,1) model_intra_annual];

%             B = [ones(nifgs,1) intra_annual];
%             est_param = [est_ini(:,1) est_ini(:,2)];
            model_amp = B(:,end)*est_param(:,end)';  
            output.est_param = est_param;
            output.model = model_intra_annual;
            output.model_amp = model_amp'; % scaled model per prixel;
            output.in_p = in_p; % logical for model location
            output.design_matrix = B;
            output.ref_ix = m_ix_max;
            save([output_path,save_name],'-struct','output');
        end
    
    else
        output = load([output_path,save_name]);
    end
    S.poly_model.ref_ix = output.ref_ix;
end

