function [S,output] = jihg_temporal_unwrap(S,input)
% jihg_temporal_unwrap (re)-estimates the unwrapped phase, based on the ambiguity function (or periodogram).
% This is done, because I noticed a couple of sudden full cycle jumps in the unwrapped
% time series over urban areas. Such jumps can occur due to unwrapping
% errors using SNAPHU. It is safe to assume that urban areas should not
% suddenly jump up or down, so we can use the AF to correct them.
    output_path     = S.options.output_path;
    s               = dbstack;
    fun_name        = s(1).name;
    save_name       = [fun_name,'_output.mat'];
    if S.sim_flag == 1
        save_name       = [fun_name,'_sim_output.mat'];
    end
    if ~(exist(fullfile(output_path,save_name),'file')==2)
        
            
        ifg_ix          = S.stackinfo.ifg_ix_sm;
        dates_num       = S.stackinfo.dates_num;
        nifgs           = S.stackinfo.nifgs;

        tsr_in          = input.tsr_unw;   
        coh             = input.tsr_coh;
        CRB             = input.CRB_sm;
        est_param_orig  = input.est_param_tsr;
        nwin            = S.win.n;
        in_p = S.win.win_f == 3;
        
        A = [ones(nifgs,1) (dates_num(ifg_ix(1)) - dates_num(ifg_ix(:,2)))/365.25];
        
        est_param = zeros(nwin,size(A,2));
        model = zeros(size(tsr_in));
        tsr_unw = zeros(size(tsr_in));
        tic
        
        for i = 1:nwin
            if ~in_p(i)
                y = tsr_in(i,:);
%                 W=(coh(i,:).^2) ./ (1-coh(i,:).^2);% Fisher Information as weights
%                 W(isinf(W)) = 0;

                W = 1./CRB(i,:);
                if rank(W) == 0
                    W = ones(size(W));
                end
                est = est_param_orig(i,:);
                p_min = [est(1) - pi,est(2) - 3*pi];
                p_max = [est(1) + pi,est(2) + 3*pi];
                p_step = [(p_max(1) - p_min(1))/100,(p_max(2) - p_min(2))/100];
                est_new   = jihg_ambiguity_fun(y,A,W,p_min,p_max,p_step);
                y_hat = A*est_new';            
                tsr_tmp = jihg_solve_ambiguity(tsr_in(i,:)',y_hat,2*pi);
                est_param(i,:) = lscov(A,tsr_tmp,W');
                model(i,:) = A*est_param(i,:)';
                tsr_unw(i,:) = tsr_tmp;
            end
            
            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
            end
            
        end
        
        output.est_param = est_param;
        output.model = model;
        output.tsr_unw = tsr_unw;
        output.design_matrix = A;
        save([output_path,save_name],'-struct','output');
    
    else
        output = load([output_path,save_name]);
    end
    
end