function [output]=jihg_read_weatherdata(filename,w_vars)
    
    fid = fopen(filename);
    weatherdataread = textscan(fid,['%s %s %s %s %s %s'...
        '%s %s %s %s %s'...
        '%s %s %s %s %s'...
        '%s %s %s %s %s'...
        '%s %s %s %s %s'...
        '%s %s %s %s %s'...
        '%s %s %s %s %s'...
        '%s %s %s %s %s'],'Delimiter',',');
    fclose(fid);
    if ~iscell(w_vars)
        w_vars = {w_vars};
    end
    
    ix = find(strcmp(weatherdataread{1},'# STN'));
    weatherdata = [weatherdataread{1,:}];
    weather_header = weatherdata(ix,:);
    
    
    for i = 1:length(w_vars)
       w_ix(i) =  find(strcmp(weather_header,w_vars(i)));
    end
    
    weatherdata = str2double(weatherdata(ix+1:end,:));
    
    dates = weatherdata(:,2);
    dates = datetime(datestr(datenum(num2str(dates),'yyyymmdd')));
    
    output = array2timetable(weatherdata(:,w_ix),'RowTimes',dates,'VariableNames',w_vars);

end