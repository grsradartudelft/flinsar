function [S,output] = jihg_est_tsr(S,input)

%jigh_est_tsr estimates a linear model (offset + velocity) and the
% creates the time series w.r.t. to a chosen reference. Since we are using
% phase-linking the time series is already estimated, so the only step
% required is to convert the unwrapped results from SNAPHU to the desired
% time series.
    
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
        
        ESM_EDC     = S.options.pl_ESM_EDC;
        nifgs       = S.stackinfo.nifgs;        
        dates_num   = S.stackinfo.dates_num;
        nwin        = S.win.n;
        win_f       = S.win.win_f;
        CRB         = S.options.CRB;

        ifgs_uw   = input.ifgs_uw;
        coh       = input.ifgs_coh;  
        CRB_sm    = input.CRB_sm;
        CRB_dc    = input.CRB_dc;
        
        not_used = isinf(CRB_sm);     
        
        W_ps = ones(nifgs,1); % use no weights for ps.
        
        % makes sure we used the right weights. 
        if strcmp(ESM_EDC,'EDC')
            ifg_ix      = S.stackinfo.ifg_ix_dc;
            A = ([dates_num(ifg_ix(:,1)) - dates_num(ifg_ix(:,2))])/365.25;
            W = 1./CRB_dc;
        elseif strcmp(ESM_EDC,'ESM')
            ifg_ix      = S.stackinfo.ifg_ix_sm;
            A = [ones(nifgs,1) (dates_num(ifg_ix(:,1)) - dates_num(ifg_ix(:,2)))/365.25];
            W = 1./CRB_sm;
            warning('Not correctly implemented, will give errors');
        end
        
        A_DC = eye(nifgs);
        for n = 1:size(A_DC,2)-1
            A_DC(n+1,n) = -1;
        end
                    
        
        est_param_ifg = zeros(nwin,size(A,2));
        tsr_est_ifg = zeros(nwin,nifgs);
        fprintf('Estimating parameters...\n');
        for i = 1:nwin

            ph_win = ifgs_uw(i,:)';
            
            if win_f(i) == 1
                est_param_ifg(i,:) = lscov(A,ph_win,W_ps);
            else

                used = ~not_used(i,:);
                if sum(used) ==0
                    if mod(i,round(nwin/10)) == 0
                        fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
                    end
                    continue
                end
                if strcmp(CRB,'full') 
                    Q = zeros(nifgs);                
                    Q_in = load([S.options.CRB_path,'CRBQ_win',num2str(i)],'CRBQ'); Q_in = Q_in.CRBQ;
                    Q(used,used) = Q_in;
                    Q(diag(diag(Q) == 0)) = 9999;
                    if strcmp(ESM_EDC,'EDC')
                        Q = A_DC*Q*A_DC';   
                    end
                    tri_up  = triu(ones(size(Q))) == 1;
                    Q_t  = Q.';
                    Q(tri_up) =  Q_t(tri_up); 
                    [~,p] = chol(Q);
                    if p > 0
                        est_param_ifg(i,:) = lscov(A,ph_win,W(i,:)');
    %                     fprintf('Not positive definite \n')
                    else   
                        est_param_ifg(i,:) = lscov(A,ph_win,Q);
                    end
                else
                    est_param_ifg(i,:) = lscov(A,ph_win,W(i,:)');
                end
            end
            tsr_est_ifg(i,:) = A*est_param_ifg(i,:)';
            
            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
            end
        end
        
         if strcmp(ESM_EDC,'EDC')
            fprintf('Estimating parameters after unwrapping daisy-chain...\n');
            tsr_unw = cumsum(ifgs_uw,2);
            A = [ones(nifgs,1) [(dates_num(ifg_ix(1))-dates_num(ifg_ix(:,2)))]/365.25];
            est_param_tsr = zeros(nwin,size(A,2));
            tsr_est = zeros(nwin,nifgs);
            
            % PC: changed to W = CRB_dc; 
%             W = CRB_sm;
            W = CRB_dc;
            
            for i = 1:nwin
                ph_win = tsr_unw(i,:)';  
                
                
                if win_f(i) == 1
                    est_param_tsr(i,:) = lscov(A,ph_win,W_ps);
                else
                
                    used = ~not_used(i,:);
                    if sum(used) ==0
                        if mod(i,round(nwin/10)) == 0
                            fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
                        end
                        continue
                    end

                    if strcmp(CRB,'full') 
                        Q = zeros(nifgs);
                        Q_in = load([S.options.CRB_path,'CRBQ_win',num2str(i)],'CRBQ'); Q_in = Q_in.CRBQ;
                        Q(used,used) = Q_in;
                        Q(diag(diag(Q) == 0)) = 9999;
                        tri_up  = triu(ones(size(Q))) == 1;
                        Q_t  = Q.';
                        Q(tri_up) =  Q_t(tri_up); 
                        [~,p] = chol(Q);
                        if p > 0
                            est_param_tsr(i,:) = lscov(A,ph_win,W(i,:)');
                            fprintf('Not positive definite')
                        else                 

                            est_param_tsr(i,:) = lscov(A,ph_win,Q);
                        end
                    else                
                        est_param_tsr(i,:) = lscov(A,ph_win,W(i,:)');
                    end
                end
                tsr_est(i,:) = A*est_param_tsr(i,:)';
                
                if mod(i,round(nwin/10)) == 0
                    fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
                end
                
            end
         else
             est_param_tsr = est_param_ifg;
             tsr_est       = tsr_est_ifg;
             tsr_unw       = ifgs_uw;
         end
        
        
        output.est_param_ifg = est_param_ifg;
        output.est_param_tsr = est_param_tsr;
        output.ifg_unw = ifgs_uw;
        output.tsr_unw = tsr_unw;
        output.design_matrix = A;
        output.tsr_coh = coh;
        output.tsr_est = tsr_est;
        output.tsr_est_ifg = tsr_est_ifg;
        output.CRB_sm = CRB_sm;
        output.CRB_dc = CRB_dc;
         
        save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.steps.est_tsr = 1;
    S.steps.est_tsr_name = [output_path,fun_name,'_output.mat'];
    
    clearvars -except S output

end