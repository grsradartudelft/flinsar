function [cpxcoh_opt] = jihg_ph_est_emi(cpxcoh_in)
%JIHG_PH_EST_EMI
% Based on Ansari et al. (2018)

% Author: FMG HEUFF - 02/2020.
    cpxcoh = cpxcoh_in;
    % check if matrix is positive definite, otherwise regularize
    [~,p] = chol(abs(cpxcoh));
    e=1e-6;
    nslcs = length(cpxcoh);
    while p > 0
        cpxcoh = cpxcoh+e*eye(nslcs);
        [~,p] = chol(abs(cpxcoh));
        e = e*2;
    end        
    
    % EMI
    % After testing, I believe the SVD or EIG give the same results. SVD is
    % computationally more efficient.
    
    % PC: added method from Ansari et al. to check result
%     [V,D] = eig(inv(abs(cpxcoh)).*cpxcoh);
%     [lambda,idx] = min(diag(D));
%     zeta = V(:,idx);
%     n = zeta'*zeta;
%     zeta = sqrt(n)/norm(zeta) * zeta;
%     cpxcoh_optA = lambda * abs(cpxcoh) .* (zeta*zeta');
    
    [U,S,~] = svd(inv(abs(cpxcoh)).*cpxcoh); % weighted with coh.
    
    cpxcoh_opt = U(:,end)*S(end,end)*U(:,end)';      
    
    % plot for debugging
%     figure
%     hold on
%     plot(angle(cpxcoh_optA(:,1)))
%     plot(angle(cpxcoh_opt(:,1)))
    
end