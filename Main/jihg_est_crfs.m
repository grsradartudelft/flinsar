function jihg_calc_crfs(S)
    
    output_path = S.options.output_path;
    input_path = S.options.input_path;
    fname_wd = S.options.fname_wd;

    if ~(exist(fullfile(output_path,'CRFS.csv'),'file')==2)
        TT = jihg_read_weatherdata([input_path,fname_wd],{'RH','EV24'});
        
    else
        TT = readtimetable([output_path,'CRFS.csv']);
    end
end