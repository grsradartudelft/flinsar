function [output]=jihg_read_rainfall_data(filename)
    
    fid = fopen(filename);
    weatherdataread = textscan(fid,['%s %s %s %s %s'],'Delimiter',',');
    fclose(fid);
    
    ix = find(strcmp(weatherdataread{1},'STN'));
    weatherdata = [weatherdataread{1,:}];
    weather_header = weatherdata(ix,:);    
    
    loc = weatherdata(ix+1:end,5);
    weatherdata = str2double(weatherdata(ix+1:end,:));
    
    dates = weatherdata(:,2);
    dates = datetime(datestr(datenum(num2str(dates),'yyyymmdd')))-days(1);
    
    T_rain = timetable;
    T_snow = timetable;
    
    stations = weatherdata(:,1);    
    ids = unique(stations);
    
    for i = 1:length(ids)
        sel = stations == ids(i);
        names = loc(sel);        
        temp1 = array2timetable(weatherdata(sel,3),'RowTimes',dates(sel),'VariableNames',names(1));
        temp2 = array2timetable(weatherdata(sel,4),'RowTimes',dates(sel),'VariableNames',names(1));
        T_rain = outerjoin(T_rain,temp1);
        T_snow = outerjoin(T_snow,temp2);
    end
    
    output.T_rain = T_rain;
    output.T_snow = T_snow;
    
    save('rainfall_data.mat','-struct','output');

end