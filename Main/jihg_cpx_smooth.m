function [phi_smooth,phi_noise] = jihg_cpx_smooth(phi,weight_factor)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if weight_factor == 1
    weight_factor = eye(length(phi));
end

if ~isrow(phi)
    phi = phi.';
end

    % wrap
    if isreal(phi)
        cpx_temp = exp(1i*phi); 
    else
        cpx_temp = phi;
    end
    % smooth the time-series to unwrap (in case weight_factor is not
    % eye(n phi))
    cpx_smooth = (weight_factor'*cpx_temp.').';
    phi_smooth = angle(cpx_smooth);
    phi_noise= angle(cpx_temp.*conj(cpx_smooth)); % difference is noise

end

