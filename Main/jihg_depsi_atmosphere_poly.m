function [S,output] = jihg_depsi_atmosphere_poly(S)

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    save_name       = [fun_name,'_output.mat'];
    
     if ~(exist(fullfile(output_path,save_name),'file')==2)
     %% Otherwise run function.
        % Read input

        depsi_path = S.options.depsi_path;
        depsi_project_id = S.options.depsi_project_id;
        dates_stack = S.stackinfo.dates_stack; % original dates (used in DePSI aswell)
        master_date = S.options.master_date;
        dropped_slcs = S.stackinfo.dropped;
        nslcs        = S.stackinfo.nslcs;
        
        ESM_EDC = S.options.pl_ESM_EDC;

        win_info = load([output_path,'jihg_poly_geocoords_output.mat']);
        win_cntr = win_info.win_cntr;

        psp_info_fid = [depsi_path,'/',depsi_project_id,'_psp_sel1.raw'];
        master_atmos_fid = [depsi_path,'/',depsi_project_id,'_psp_atmo_master1_sel1.raw'];
        psp_atmos_fid = [depsi_path,'/',depsi_project_id,'_psp_atmo_sel1.raw'];

        n_epochs = length(dates_stack) - 1;
        psp_info = freadbk(psp_info_fid,1,'double');
        master_atmos = freadbk(master_atmos_fid,1,'double');
        psp_atmos = freadbk(psp_atmos_fid,1,'double')';

        psp_info = reshape(psp_info,n_epochs*2+4,[])';
        psp_atmos = reshape(psp_atmos,n_epochs,[])';
        % add master-master
        master_ix = find(str2double(master_date) == dates_stack);
        psp_atmos = [psp_atmos(:,1:master_ix-1) zeros(size(psp_atmos,1),1) psp_atmos(:,master_ix:end)];
        
        psp_atmos(:,dropped_slcs) = [];
        master_ix = find(str2double(master_date) == dates_stack(~dropped_slcs)); % new location of master image
        %% interpolate
        
        win_atmos_dc = zeros(S.win.npoly,nslcs-1);
        win_atmos_sm = zeros(S.win.npoly,nslcs-1);

        slc_atmos = zeros(length(master_atmos),nslcs);
        for i = 1:nslcs
            if i == master_ix
                slc_atmos(:,i) = master_atmos';
            else
                slc_atmos(:,i) = -(psp_atmos(:,i) - master_atmos');
            end
        end
        
%         figure(1); scatter(win_cntr(:,2),win_cntr(:,1));  hold on; scatter(psp_info(:,4),psp_info(:,3));
        

        F = scatteredInterpolant(psp_info(:,4),psp_info(:,3),master_atmos');
        win_m_atmos = F(win_cntr(:,2),win_cntr(:,1));
    
        for i = 1:nslcs-1 
            F.Values = slc_atmos(:,i) - slc_atmos(:,i+1);
            win_atmos_dc(:,i) = F(win_cntr(:,2),win_cntr(:,1));
%             figure(2); scatter(psp_info(:,4),psp_info(:,3),10,F.Values ,'filled')
%             figure(3);  imagesc(reshape(win_atmos_dc(:,i),S.win.nlines,S.win.npixels));
            
        end
        
        

        for i = 1:nslcs-1 
            F.Values = slc_atmos(:,1) - slc_atmos(:,i+1);
            win_atmos_sm(:,i) = F(win_cntr(:,2),win_cntr(:,1));
        end
        
        [ref_ind] = jihg_unw_setref(S);
        
        win_atmos_dc = win_atmos_dc - S.atmo.ref_mean_dc;
        win_atmos_sm = win_atmos_sm - S.atmo.ref_mean_dc;
        
        output.win_m_atmos = win_m_atmos;
        output.win_atmos_dc = win_atmos_dc;
        output.win_atmos_sm = win_atmos_sm;
        save([output_path,save_name],'-struct','output');
        
    
    else
        output = load([output_path,save_name]);
    end
    
    
end