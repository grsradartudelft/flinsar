function [S,output] = jihg_rcr_v2(S,input)
% Author: FMG HEUFF - 06/2020.
% added and changed code to accommodate spatially varying regressors
    
%% If output exists, load it instead.

    s               = dbstack;    
    fun_name        = s(1).name;
    output_path     = S.options.output_path;
    iter_step       = input.iter_step;
    save_name       = [fun_name,'_output',num2str(iter_step),'.mat'];  
    if S.sim_flag == 1
        save_name       = ['Simulation/',fun_name,'_sim_output',num2str(iter_step),'.mat'];
    end

    input.overwrite = 0;
    if ~(exist(fullfile(output_path,save_name),'file')==2) ||  input.overwrite
    %% otherwise run function    
        
    
        tsr_unw         = input.tsr_unw;
        gamma_pta       = input.gamma_pta;
        CRB             = input.CRB_sm;
        win_f           = input.win_f_joint;
        EP1_avg         = input.EP1_avg;
        EP2_avg         = input.EP2_avg;
        DM1             = input.DM1;
        peat_regressor  = input.regressors;
%         EP1             = input.EP1;
%         EP2             = input.EP2;
        
        nwin            = S.win.n;
        rcr_types       = S.options.rcr_types;
        
        default         = 1;
        
        %% Remove Step
      

        
        % peat - spatially varying model
        % per window for loop
        % estimated time-series for the two models
        
        % wrap the original unwrap timeseries and estimed tsr
        tsr_w       = exp(1j*(tsr_unw-tsr_unw(:,1))); %
        tsr_w_all   = zeros(size(tsr_w));
        est_EP1     = zeros(size(tsr_w));
        est_EP2     = zeros(size(tsr_w));
        for i = 1:nwin
            
  
            if strcmp(rcr_types,'all') % I never used this and there's a bug, which I haven't solved yet.
                
                est_EP1(i,:) = EP1_avg(i,:)*DM1';
                
                regressor = peat_regressor(:,i);
                DM2 = [ones(size(regressor)) regressor];
                est_EP2(i,:) = EP2_avg(i,:)*DM2';
                est_EP2(i,:) = est_EP2(i,:) - est_EP2(i,1); %start at 0 to be consistent across windows. 
        
                est_EP2_w   = exp(1j*est_EP2(i,:));
                % remove estimate
                tsr_w_all(i,:)    = tsr_w(i,:).*conj(est_EP2_w).*conj(est_EP1_w); % Something is missing, although I am not sure where it got lost... :-P
            else
                
                regressor = peat_regressor(:,i);
                DM2 = [ones(size(regressor)) regressor];
                est_EP2(i,:) = EP2_avg(i,:)*DM2';
                est_EP2(i,:) = est_EP2(i,:) - est_EP2(i,1);
                
                % we can't simply subtract the model, we need to first
                % wrap the original unwrapped timeseries and estimed tsr
                % and use complex multiplication to remove the model.
     
                est_EP2_w   = exp(1j*est_EP2(i,:));

                % remove estimate            
                tsr_w_all(i,:)    = tsr_w(i,:).*conj(est_EP2_w);
            end           
        end
        
        
        %% Compute
        % create interferogram to be unwrapped
        
        input.ifgs          = angle(tsr_w_all); % in tsr_joint_rcr the model used per window has already been set.
        input.coh           = repmat(gamma_pta,1,S.stackinfo.nslcs-1); % this is the TEMPORAL coherence.
        input.iter_step     = iter_step;
        input.win_f_joint   = win_f;
        
        %% function
        
        [S,output]          = jihg_unwrap_rcr(S,input); 
        
        %% Restore        
        tsr_unw_EP         = output.ifgs_uw;
%         
        tsr_rcr            = tsr_unw_EP + est_EP1 + est_EP2;
        
        %% est       
        % correct any unwrapping mistakes and estimate through improved
        % unwrapped results
        tsr_new     = zeros(size(tsr_rcr));
        est_EP1_new = zeros(size(tsr_rcr));           
        est_EP2_new = zeros(size(tsr_rcr)); 
        EP1_new     = zeros(size(EP1_avg));
        EP2_new     = zeros(size(EP2_avg));
        
        v_res       = zeros(size(EP1_avg,1),1);
        v_est       = zeros(size(tsr_rcr));
        
        dates_num       = S.stackinfo.dates_num(2:end);
        years           = year(datetime(datestr(dates_num)));
        winter          = any(month(datetime(datestr(dates_num))) == [1 2],2) ;
        ref_period      = S.options.ref_period;   
        ref_datenum     = datenum(ref_period{1},'yyyymmdd'):datenum(ref_period{2},'yyyymmdd');
        ref             = ismember(dates_num,ref_datenum);
        tic
        
        %% first do all windows with standard model
        % we can still keep this the same as we have not changed the DM1 method
        for i = 1:nwin             
            
            tsr_win = tsr_rcr(i,:); 
            tsr_win = tsr_win - mean(tsr_win(ref));
            
            tsr_orig = tsr_unw(i,:); 
            tsr_orig = tsr_orig - mean(tsr_orig(ref));
                
            if win_f(i) == 1
                % unweighted least squares for PS
                [est_param1,~,ee1] = lscov(DM1,tsr_win');
                [est_param2,~,ee2] = lscov(DM1,tsr_orig'); % trials with best fit to select model, however, currently just using the RCR result. 
                if 1 %ee1 < ee2
                    est = DM1*est_param1;  
                else
                    est = DM1*est_param2; 
                end
                    
                % correct unwrapping errors
                tsr_tmp = jihg_solve_ambiguity(tsr_win',est,2*pi);
                EP1_new(i,:) = lscov(DM1,tsr_tmp); 
            else
                if win_f(i) > -1
                    if strcmp(CRB,'full') && sum(used) > 0
                        Q = zeros(nifgs);
                        Q_in = load([S.options.CRB_path,'CRBQ_win',num2str(i)],'CRBQ'); Q_in = Q_in.CRBQ;
                        Q(used,used) = Q_in;
                        Q(diag(diag(Q) == 0)) = 9999;
                        [~,p] = chol(Q);
                        if p > 0
                            % default using just variances
                            Q = 1./CRB(i,:).';
                            fprintf('Not positive definite\n')
                        end   
                    else
                        % default using just variances
                        Q = 1./CRB(i,:);
                    end                

                    [est_param1,~,ee1] = lscov(DM1,tsr_win');
                    [est_param2,~,ee2] = lscov(DM1,tsr_orig'); 
                    
                    if 1 %ee1 < ee2
                        est = DM1*est_param1;  
                    % correct unwrapping errors
                        tsr_tmp = jihg_solve_ambiguity(tsr_win',est,2*pi);
                        EP1_new(i,:) = lscov(DM1,tsr_tmp,Q); 
                    else
                        est = DM1*est_param2; 
                        % correct unwrapping errors
                        tsr_tmp = jihg_solve_ambiguity(tsr_orig',est,2*pi);
                        EP1_new(i,:) = lscov(DM1,tsr_tmp,Q); 
                    end

                end
            end


             
             % correct original tsr with new estimate
             est_EP1_new(i,:)   = DM1*EP1_new(i,:)';
             tsr_new(i,:)       = jihg_solve_ambiguity(tsr_unw(i,:)',est_EP1_new(i,:)',2*pi);  
             
             
            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
            end
        end
        
        %% now model 2 for polygons 
        % needs to be changed to with the varying regressor
        % for visualization purposes
        EP_final = [EP1_new zeros(nwin,1)];

        for i = 1:nwin  
            
            DM_final        = [DM1 peat_regressor(:,i)];

            tsr_win = tsr_rcr(i,:); 
            tsr_win = tsr_win - mean(tsr_win(ref));
            
            tsr_orig = tsr_unw(i,:); 
            tsr_orig = tsr_orig - mean(tsr_orig(ref));               
                
            if win_f(i) == 3
                
                if rank(DM_final) < size(DM_final,2)
                    test_zeros = all(DM_final == 0,1);
                    if any(test_zeros)
                        continue
                    end
                    
                end
                
                
                if strcmp(CRB,'full') && sum(used) > 0
                    Q = zeros(nifgs);
                    Q_in = load([S.options.CRB_path,'CRBQ_win',num2str(i)],'CRBQ'); Q_in = Q_in.CRBQ;
                    Q(used,used) = Q_in;
                    Q(diag(diag(Q) == 0)) = 9999;
                    [~,p] = chol(Q);
                    if p > 0
                        % default using just variances
                        Q = 1./CRB(i,:).';
                        fprintf('Not positive definite\n')
                    end   
                else
                    % default using just variances
                    Q = 1./CRB(i,:);
                end            
                
    
                [est_param1,~,ee1] = lscov(DM_final,tsr_win',Q);
                [est_param2,~,ee2] = lscov(DM1,tsr_orig',Q); 

                if 1% ee1 < ee2
                    est = DM_final*est_param1;  
                % correct unwrapping errors
                    tsr_tmp = jihg_solve_ambiguity(tsr_win',est,2*pi);
                    temp = lscov(DM_final,tsr_tmp,Q);
                    EP2_new(i,:) = temp([1,3]); 
                    
                    v_res(i,1) = temp(2);
                    v_est(i,:) = DM_final(:,1:2)*temp([1,2]);
                

                % correct original tsr with new estimate
                    regressor = peat_regressor(:,i);
                    DM2 = [ones(size(regressor)) regressor];

                    est_EP2_new(i,:)   = DM2*EP2_new(i,:)';
    %                 tsr_new(i,:)       = jihg_solve_ambiguity(tsr_unw(i,:)',est_EP2_new(i,:)',2*pi);
                    tsr_new(i,:) = jihg_solve_ambiguity(tsr_unw(i,:)',DM_final*temp,2*pi);
                    
                    EP_final(i,:) = lscov(DM_final,tsr_new(i,:)',Q);


                
%                 figure(1); plot(tsr_unw(i,:)); 
%                 hold on; plot(tsr_new(i,:)); 
%                 plot(DM_final*temp);
%                 hold off
%                 
%                 figure(2); plot(tsr_rcr(i,:)); 
%                 hold on; plot(est_EP2(i,:)); 
%                 plot(DM_final*temp);
%                 hold off
%                 
                
                
                
                else
                    est = DM1*est_param2; 
                    % correct unwrapping errors
                    tsr_tmp = jihg_solve_ambiguity(tsr_orig',est,2*pi);
                    temp = lscov(DM1,tsr_tmp,Q);
                    EP2_new(i,:) = temp;

                    v_res(i,1) = temp(2);
                    v_est(i,:) = DM1*temp([1,2]);
                    
                    est_EP2_new(i,:)   = DM1*EP2_new(i,:)';
                    tsr_new(i,:)       = jihg_solve_ambiguity(tsr_unw(i,:)',est_EP2_new(i,:)',2*pi); 
                end
%                 
%                 
                
                
            end

            if mod(i,round(nwin/10)) == 0
                fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)      
            end
        end
        
        


        output.tsr_unw_rc = tsr_unw_EP;
        output.tsr_unw = tsr_new;
        output.EP1 = EP1_new; % used for RCR
        output.EP2 = EP2_new;
        output.v_res = v_res;
        output.v_est = v_est;
        
        output.est_EP1 = est_EP1_new;
        output.est_EP2 = est_EP2_new;
        output.DM_final = [DM1 peat_regressor]; %lazy programming
        
        output.EP_final = EP_final;
   
        
        save([output_path,save_name],'-struct','output');
    else
        output = load([output_path,save_name]);
    end


end