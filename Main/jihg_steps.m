function jihg_steps
%JIHG_STEPS Calls the jihg functions that form one large processing step. 
% TODO: The order does not make a whole lot of sense yet. Certain functions
% call other functions. Some of the functions here might fit better as part
% of a group of functions.

% Author: FMG HEUFF - Feb 2020.

% S is continually updated between processing steps. It contains
% configuration settings, variables that can easily be kept in memory and
% stored processing output that is relevant for further steps.
% NOTE: I realized later on that this is not the ideal way to program this,
% so I started deviating a little bit. 

% try
%      S.options = load('jihg_S');
% catch
%      S.options = load('jihg_settings');
% end
%

S.options = load('jihg_S');

t = tic;
fprintf('#### Creating folders. \n');
[S]             = jihg_init(S); 
                save([S.options.output_path,'S'],'-struct','S');
                fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));
                
%%
fprintf('#### Reading stack information. \n');
[S]             = jihg_read_stackinfo(S); 
                save([S.options.output_path,'S'],'-struct','S');
                fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));                
               
%%
fprintf('#### Calibrating amplitude.  \n');
[S]             = jihg_amp_fun(S);
                save([S.options.output_path,'S'],'-struct','S');
                fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));
                
%%
fprintf('#### Labeling pixels. \n');
[S]             = jihg_id_pixels(S);
                save([S.options.output_path,'S'],'-struct','S');
                fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));
                
%% 
fprintf('#### Alloting pixels to windows. \n');
[S,output_win]      = jihg_allot_win(S);
                save([S.options.output_path,'S'],'-struct','S');
                fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));  
                
%%                
fprintf('#### Creating interferograms. \n');
[S,output_grid]      = jihg_cpxcoh(S,output_win);
                save([S.options.output_path,'S'],'-struct','S');
                fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));

%%                
fprintf('#### Unwrapping interferograms. \n'); 
[S,output]      = jihg_unwrap(S,output_grid);
                save([S.options.output_path,'S'],'-struct','S');            
                
%%               
fprintf('#### Estimating timeseries and parameters. \n'); 
[S,output]      = jihg_est_tsr(S,output);
                save([S.options.output_path,'S'],'-struct','S');
                fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t)); 
                delete jihg_S.mat
             
end

