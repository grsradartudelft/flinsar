function [S,output] = jihg_sel_shps_poly(S,input_poly,input_amp)
%JIHG_SEL_SHPS selects per window (statistically) homogenous pixels.
% These pixels are later used for multi-looking. Pixels not belonging to
% the main group are not used in further processing.

% Author: FMG HEUFF - 02/2020.
    
%% If output exists, load it instead.
    output_path     = S.options.output_path;    
    s               = dbstack;
    fun_name        = s(1).name;
    
    if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    %% Otherwise run function.
        % Read input
        ps_flag         = S.options.ps_flag;
        mean_test       = S.options.mean_test;
        alpha           = S.options.alpha;
        nslcs           = S.stackinfo.nslcs;

        pix_PS          = S.pix_id.ps;
        pix_in_p        = S.pix_id.in_p;
        nwin            = S.win.npoly; 
        
        
        win_ixs         = input_poly.win_ixs;
        amp_mean        = input_amp.amp_mean;
        
        % Preallocate cells and arrays
        win_shp         = cell(nwin,1);
        nshp            = zeros(nwin,1);        
        win_f           = zeros(nwin,1); % check for type of windows 1 = PS, 2 = PS in field, 3 = field, 0 = other.

        min_shps = 25;
        if ps_flag ==1
        %% %% %% %% %% %% %% %% %% %% %% %%
            % first check for PS processing, good results
            for i = 1:nwin
                ix          = win_ixs{i};
               
                is_ps       = pix_PS(ix);    % PS
                is_in_p     = pix_in_p(ix);  % In polygons                   
                    
                 
                if isempty(ix)
                    %% Window outside slc
                    win_shp{i} = [];
                    win_f(i) = -2;
                    nshp(i)  = 0;
                elseif any(is_ps) && ~ any(is_in_p)
                    %% Window with PS
                    
                    win_shp{i}=ix(is_ps);
                    nshp(i)=sum(is_ps);
                    win_f(i) = 1;
                
                elseif length(ix) < min_shps 
                    %% Window with too few pixels
                    win_shp{i} = [];
                    win_f(i) = -1;
                    nshp(i)  = 0;
                
                elseif any(is_in_p)
                    %% Window with pixels of interest
                    % remove PS (if present)
                    sel = is_in_p & ~is_ps;
                    ix_p = ix(sel);
                    
                    if mean_test == 1
                        
                        [nbs] = jihg_mean_test(double(amp_mean(ix_p)),nslcs,alpha);
                        
                        win_shp{i}  = ix_p(nbs);
                        win_f(i)    = 3;
                        nshp(i)     = sum(nbs);
                    else
                        win_shp{i}  = ix_p;
                        win_f(i)    = 3;
                        nshp(i)     = length(ix_p);
                    end                    
                else
                    %% select everything
                    win_shp{i} = ix;
                    win_f(i) = 0;
                    nshp(i) = length(ix);
                end       
                
                % check done afterwards, for clarity
                if nshp(i) < min_shps 
                    if win_f(i) == 3
                        % too few, select all instead
                        win_shp{i} = ix;
                        win_f(i) = 2;
                        nshp(i) = length(ix);
                    elseif win_f(i) == 0
                        % should not be possible
                        win_shp{i} = [];                       
                        win_f(i) = -1;
                        nshp(i)  = 0;
                    end
                end
                

                if mod(i,round(nwin/10)) == 0
                    fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)                
                end
                
            end % end for loop

            
        else 
        %% %% %% %% %% %% %% %% %% %% %% %%
        % default
        % less trust in PS, different criteria
            
            for i = 1:nwin
                ix          = win_ixs{i};
               
                is_ps       = pix_PS(ix);    % PS
                is_in_p     = pix_in_p(ix);  % In polygons      


                
                if isempty(ix)
                    %% Window outside slc
                    win_shp{i} = [];
                    win_f(i) = -2;
                    nshp(i)  = 0;
                elseif length(ix) < min_shps 
                    %% Window with too few pixels
                    win_shp{i} = [];
                    win_f(i) = -1;
                    nshp(i)  = 0;                    
                
                elseif any(is_in_p)
                    %% Window with pixels of interest
                    % remove PS (if present)
                    sel = is_in_p & ~is_ps;
                    ix_p = ix(sel);
                    
                    if mean_test == 1
                        
                        [nbs] = jihg_mean_test(double(amp_mean(ix_p)),nslcs,alpha);
                        
                        win_shp{i}  = ix_p(nbs);
                        win_f(i)    = 3;
                        nshp(i)     = sum(nbs);
                    else
                        win_shp{i}  = ix_p;
                        win_f(i)    = 3;
                        nshp(i)     = length(ix_p);
                    end   
                elseif any(is_ps)
                    %% Window with PS
                    nps_win     = sum(is_ps);
                    ntot_win    = length(ix);
                    
                    if nps_win > 0.1*ntot_win
                        %% Select based on PS                        
                        win_shp{i}=ix(is_ps);
                        nshp(i)=sum(is_ps);
                        win_f(i) = 1;
                    else
                        %% Select all
                        win_shp{i} = ix;
                        win_f(i) = 0;
                        nshp(i) = length(ix);
                    end
                
                else
                    %% select everything
                    win_shp{i} = ix;
                    win_f(i) = 0;
                    nshp(i) = length(ix);
                end       
                
                % check done afterwards, for clarity
                if nshp(i) < min_shps 
                    if win_f(i) == 3
                        % too few
                        win_shp{i} = ix;
                        win_f(i) = 2;
                        nshp(i) = length(ix);
                    elseif win_f(i) == 0
                        % should not be possible
                        win_shp{i} = [];                       
                        win_f(i) = -1;
                        nshp(i)  = 0;
                    end
                end
                

                if mod(i,round(nwin/10)) == 0
                    fprintf('Finished %i of %i windows. [%i%% - %0.2f s] \n',i,nwin,round(i/nwin*100),toc)                
                end
                
            end % end for loop
             
        end
        
        %% output
        output.win_shp          = win_shp;
        output.nshp             = nshp;
        output.win_f            = win_f;
        
        save([output_path,fun_name,'_output'],'-struct','output');
    else
        output = load([output_path,fun_name,'_output.mat']);
    end
    
    S.win.win_f_poly = output.win_f;
    S.win.nshp_poly  = output.nshp;
        

end