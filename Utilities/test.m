clc; clear all; close;

fid = fopen('test.kml','w');

fprintf(fid,'<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid,'<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">');
fprintf(fid,'<Document>\n');
fprintf(fid,'<Style id="1Style">\n');
fprintf(fid,'\t<IconStyle>\n');
fprintf(fid,'\t\t<color>ff00008f</color>\n');
fprintf(fid,'\t\t<scale>0.5</scale>\n');
fprintf(fid,'\t\t<Icon>\n');
fprintf(fid,'\t\t\t<href>http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png</href>\n');
fprintf(fid,'\t\t</Icon>\n');
fprintf(fid,'\t</IconStyle>\n');
fprintf(fid,'</Style>\n');


fprintf(fid,'<Placemark> \n');
fprintf(fid,'<styleUrl>#1Style</styleUrl> \n');
fprintf(fid,'<description> \n');
fprintf(fid,' \n');
fprintf(fid,' \n');
fprintf(fid,' \n');
fprintf(fid,' \n');
       
		
		



       <br/>ID: <b>34</b>       <br/>Height [m]: <b>-100.0</b>, Height St Dev [m]: <b>3.1</b> 
       <br/>Height relative to Ground [m]: <b>-99.9</b> 
       <br/>Velocity [mm/year]: <b>-34.5</b>, Velocity St Dev [mm/year]: <b>1.54</b> 
       <br/>Displ. to Temper. Ratio [mm/degC]: <b>0.00</b>, Cumulative Displacement [mm]: <b>-65.7</b> 
       <br/>Temporal Coherence: <b>0.39</b>, Sample: <b>196</b>, Line: <b>34</b>
       <br/>Std Dev. [mm]: <b>6.1</b> Data Nr.: <b> 28</b> <br/>
               <![CDATA[
       		<script type="text/javascript" src="dygraph-combined.js"></script>
       		<div id="graphdiv"></div>
       		<script type="text/javascript">
       		g = new Dygraph(
               document.getElementById("graphdiv"),
               "Date,Displacement\n" +
       		"2017-01-24,0.0\n" + 
       		"2017-02-17,-5.1\n" + 
       		"2017-03-13,-5.3\n" + 
       		"2017-04-06,-15.1\n" + 
       		"2017-04-30,-4.4\n" + 
       		"2017-05-24,-14.0\n" + 
       		"2017-06-17,-23.2\n" + 
       		"2017-07-11,-17.4\n" + 
       		"2017-08-04,-30.5\n" + 
       		"2017-08-28,-18.7\n" + 
       		"2017-09-21,-27.5\n" + 
       		"2017-10-15,-30.6\n" + 
       		"2017-11-08,-18.8\n" + 
       		"2017-12-02,-36.3\n" + 
       		"2017-12-26,-34.0\n" + 
       		"2018-01-19,-37.5\n" + 
       		"2018-04-01,-43.9\n" + 
       		"2018-04-25,-51.2\n" + 
       		"2018-05-19,-47.8\n" + 
       		"2018-06-12,-38.2\n" + 
       		"2018-07-06,-64.9\n" + 
       		"2018-07-30,-58.1\n" + 
       		"2018-08-23,-66.2\n" + 
       		"2018-09-16,-59.6\n" + 
       		"2018-10-10,-72.1\n" + 
       		"2018-11-03,-57.3\n" + 
       		"2018-11-27,-63.0\n" + 
       		"2018-12-21,-6.5e+01\n", 
       		{
       		valueRange: [-100,100],
       		ylabel: "[mm]",
       		}
       		);
       		</script>
       	]]>
       <br/> by SARPROZ (c) 
   </description>
   <Point>
       <altitudeMode>clampToGround</altitudeMode>
       <coordinates>4.247314269065446,51.98354245227845,0.000000</coordinates>
   </Point>
   

</Placemark>           
		
	


fclose(fid);