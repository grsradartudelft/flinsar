function g = lightgray(m)
%LIGHTGRAY   Linear gray-scale color map
%   LIGHTGRAY(M) returns an M-by-3 matrix containing a gray-scale colormap.
%   LIGHTGRAY, by itself, is the same length as the current figure's
%   colormap. If no figure exists, MATLAB uses the length of the
%   default colormap. The colormap is an adjust version of the colormap
%   GRAY.
%
%   For example, to reset the colormap of the current figure:
%
%             colormap(graywhite)
%
%   See also HSV, HOT, COOL, BONE, COPPER, PINK, FLAG, 
%   COLORMAP, RGBPLOT.

%   Copyright 1984-2015 The MathWorks, Inc.

if nargin < 1
   f = get(groot,'CurrentFigure');
   if isempty(f)
      m = size(get(groot,'DefaultFigureColormap'),1);
   else
      m = size(f.Colormap,1);
   end
end

g = (0:m-1)'/max(m-1,1);
g = (g+1)/2;
g = [g g g];
