function cov_mx=cohmx2vcmx(coh_mx,baseline_arcs,ps_ds,method,L,info)
%  cohmx2vcmx -- compute phase covariance matrix for pixel interferogram
%  phasaes
% cohmx2vcmx(coh_mx,baseline_arcs,ps_ds,method,L)  returns the covariance matrix
% of vector of interferometric phases.
%
%    Inputs:
%         coh_mx: coherence matrix ==> NxN matrix where N=Nslcs
%         baseline_arcs: baseline configuration ==> Mx2 vector where M is Nifgs
%         ps_ds: 'ps' for point scatterers or 'ds' for distributed scatterers
%         method: 'analytical'  'Monte_Carlo' or  'table' or 'pdf_integral'
%
%         LL number of looks
%          info: 'var' only variance    'full': full matrix
%    Outputs:
%         cov_mx: covariance matrix ==> MxM matrix where M is Nifgs
%                 cov_mx elements are in [rad^2]
%
%
% -------------------------------------------------------------------------
% File............: cohmx2vcmx
% Version & Date..: 1.1.1.1, 25-MAY-2012
% Authors.........: Sami Samiei Esfahany
%                   Delft Institute of Earth Observation and Space Systems
%                   Delft University of Technology
% -------------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2009 Delft University of Technology, The Netherlands


global cov_LUT_3_L1 cov_LUT_3_L10 cov_LUT_3_L20 cov_LUT_3_L50 ...
    cov_LUT_6_L1 cov_LUT_6_L10 cov_LUT_6_L20 cov_LUT_6_L50
%method: pdf_integral or table
% required files:
%   covariance_table_V23April.mat,cov_from_coh3_ml.m,cov_from_coh6_ml.m,
%   coh2std.m

%%%L=1; %now only for single look

Nifgs=length(baseline_arcs);
Nslc=size(coh_mx,1);
coh_ifgs=diag(coh_mx(baseline_arcs(:,1),baseline_arcs(:,2)));

i_unit=sqrt(-1);
if strcmp(method,'Monte_Carlo')
    
    switch info
        case 'full'
            [V,D] = eig(coh_mx);
            Np=10000;
            Xc =(V*sqrt(D))*randn(Nslc,L*Np);
            Xs =(V*sqrt(D))*randn(Nslc,L*Np);
            cpx=(Xc+i_unit*Xs)';
            cpx_ifg=cpx(:,baseline_arcs(:,1)).*conj(cpx(:,baseline_arcs(:,2)));
            
            tempc=mean(reshape(real(cpx_ifg),L,Nifgs*Np),1);
            temps=mean(reshape(imag(cpx_ifg),L,Nifgs*Np),1);
            
            Xcc =reshape(tempc,Np,Nifgs);
            Xss =reshape(temps,Np,Nifgs);
            cpx_ifg=Xcc+i_unit*Xss;
            cov_mx=cov(angle(cpx_ifg));
           % cov_mx=cov_mx.*(ones(Nifgs)-eye(Nifgs))+diag((deg2rad(coh2std(coh_ifgs,ps_ds,L))).^2);
        case 'var'
            cov_mx=diag((deg2rad(coh2std(coh_ifgs,ps_ds,L))).^2);    
    end
    
elseif strcmp(method,'pdf_integral') | strcmp(method,'table')
   
    switch info
        case 'var'
            cov_mx=diag((deg2rad(coh2std(coh_ifgs,ps_ds,L))).^2);
        case 'full'
            cov_mx=diag((deg2rad(coh2std(coh_ifgs,ps_ds,L))).^2);
            if strcmp(ps_ds,'ds')
                if strcmp(method,'table') & isempty(cov_LUT_3_L1)
                    %%%disp('load')
                    PPP=mfilename('fullpath');
                    load([PPP(1:end-10) 'covariance_table_V23April.mat']);
                end
                
                I=[1:Nifgs];
                J=[1:Nifgs];
                [II,JJ]=meshgrid(I,J);
                A(:,1)=(II(:));
                A(:,2)=(JJ(:));
                A=A(A(:,1)<A(:,2),:);
                A(:,3:4)=baseline_arcs(A(:,1),:);
                A(:,5:6)=baseline_arcs(A(:,2),:);
                A(:,7)=A(:,3)-A(:,5)==0;   %ifgs with the same master
                A(:,8)=A(:,4)-A(:,6)==0;   %ifgs with the same slave
                A(:,9)=A(:,3)-A(:,6)==0;   % ifgs with master-slave in common
                A(:,10)=A(:,4)-A(:,5)==0;  % ifgs with slave-master in common
                A(:,11)=sum(A(:,7:10),2);
                
                A(and(A(:,11)==1,A(:,7)==1),12:13)=A(and(A(:,11)==1,A(:,7)==1),[4 6]);
                A(and(A(:,11)==1,A(:,8)==1),12:13)=A(and(A(:,11)==1,A(:,8)==1),[3 5]);
                A(and(A(:,11)==1,A(:,9)==1),12:13)=A(and(A(:,11)==1,A(:,9)==1),[4 5]);
                A(and(A(:,11)==1,A(:,10)==1),12:13)=A(and(A(:,11)==1,A(:,10)==1),[3 6]);
                A(:,14)=coh_mx(sub2ind(size(coh_mx),A(:,3),A(:,4)));
                A(:,15)=coh_mx(sub2ind(size(coh_mx),A(:,5),A(:,6)));
                A(A(:,11)==1,16)=coh_mx(sub2ind(size(coh_mx),A(A(:,11)==1,12),A(A(:,11)==1,13)));
                
                B=A(A(:,11)==0,[1 2 3 4 5 6 14 15]);
                B(:,12)=B(:,8);
                B(:,8)=coh_mx(sub2ind(size(coh_mx),B(:,3),B(:,5)));
                B(:,9)=coh_mx(sub2ind(size(coh_mx),B(:,3),B(:,6)));
                B(:,10)=coh_mx(sub2ind(size(coh_mx),B(:,4),B(:,5)));
                B(:,11)=coh_mx(sub2ind(size(coh_mx),B(:,4),B(:,6)));
                B_temp=B;
                B(:,7:12)=floor(B(:,7:12)*10);
                B(B==0)=B(B==0)+1;
                A=A(A(:,11)==1,[1 2 14 15 16]);
                %A(:,3:5)=round(A(:,3:5)*10)+1;
                A(:,3:5)=round(A(:,3:5)*10);
                A(A==0)=A(A==0)+1;
                
                switch method
                    case 'pdf_integral'
                        for i=1:Nifgs-1
                            for j=i+1:Nifgs
                                if ~isempty(intersect(baseline_arcs(i,:),baseline_arcs(j,:)))
                                    s_ind=setxor(baseline_arcs(i,:),baseline_arcs(j,:));
                                    g12=coh_ifgs(i);
                                    g13=coh_ifgs(j);
                                    g23=coh_mx(s_ind(1),s_ind(2));
                                    if L==1
                                        func = @(x,y)  x.*y.*phase_joint_pdf(x,y,g12,g13,g23);
                                        covtemp=simp2D(func,-pi,pi,-pi,pi,60,60);
                                    else
                                        covtemp=cov_from_coh3_ml([g12,g13,g23],L);
                                    end
                                    if covtemp>0 & isreal(covtemp)==1
                                        cov_mx(i,j)=covtemp;
                                        cov_mx(j,i)=covtemp;
                                    elseif covtemp<0
                                        cov_mx(i,j)=0;
                                        cov_mx(j,i)=0;
                                    elseif isreal(covtemp)==0
                                        cov_mx(i,j)=0;
                                        cov_mx(j,i)=0;
                                    end
                                end
                            end
                        end
                        %B
                        %B_temp
                        for i=1:size(B,1);
                            cov_mx(sub2ind(size(cov_mx),B(:,1),B(:,2)))=cov_from_coh6_ml(B_temp(i,7:12),L);
                            cov_mx(sub2ind(size(cov_mx),B(:,2),B(:,1)))=cov_mx(sub2ind(size(cov_mx),B(:,1),B(:,2)));
                            %cov_mx(sub2ind(size(cov_mx),B(:,2),B(:,1)))=cov_from_coh6_ml(B_temp(i,7:12),L);
                        end
                    case 'table'
                        %B
                        %B_temp
                        if L<=5
                            cov_mx(sub2ind(size(cov_mx),A(:,1),A(:,2)))= ...
                                cov_LUT_3_L1(sub2ind(size(cov_LUT_3_L1),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),A(:,2),A(:,1)))= ...
                                cov_LUT_3_L1(sub2ind(size(cov_LUT_3_L1),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),B(:,1),B(:,2)))= ...
                                cov_LUT_6_L1(sub2ind(size(cov_LUT_6_L1),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                            cov_mx(sub2ind(size(cov_mx),B(:,2),B(:,1)))= ...
                                cov_LUT_6_L1(sub2ind(size(cov_LUT_6_L1),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                        elseif L>5 && L<15
                            cov_mx(sub2ind(size(cov_mx),A(:,1),A(:,2)))= ...
                                cov_LUT_3_L10(sub2ind(size(cov_LUT_3_L10),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),A(:,2),A(:,1)))= ...
                                cov_LUT_3_L10(sub2ind(size(cov_LUT_3_L10),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),B(:,1),B(:,2)))= ...
                                cov_LUT_6_L10(sub2ind(size(cov_LUT_6_L10),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                            cov_mx(sub2ind(size(cov_mx),B(:,2),B(:,1)))= ...
                                cov_LUT_6_L10(sub2ind(size(cov_LUT_6_L10),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                        elseif L>=15 && L<30
                            cov_mx(sub2ind(size(cov_mx),A(:,1),A(:,2)))= ...
                                cov_LUT_3_L20(sub2ind(size(cov_LUT_3_L20),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),A(:,2),A(:,1)))= ...
                                cov_LUT_3_L20(sub2ind(size(cov_LUT_3_L20),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),B(:,1),B(:,2)))= ...
                                cov_LUT_6_L20(sub2ind(size(cov_LUT_6_L20),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                            cov_mx(sub2ind(size(cov_mx),B(:,2),B(:,1)))= ...
                                cov_LUT_6_L20(sub2ind(size(cov_LUT_6_L20),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                        else
                            cov_mx(sub2ind(size(cov_mx),A(:,1),A(:,2)))= ...
                                cov_LUT_3_L50(sub2ind(size(cov_LUT_3_L50),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),A(:,2),A(:,1)))= ...
                                cov_LUT_3_L50(sub2ind(size(cov_LUT_3_L50),A(:,3),A(:,4),A(:,5)));
                            cov_mx(sub2ind(size(cov_mx),B(:,1),B(:,2)))= ...
                                cov_LUT_6_L50(sub2ind(size(cov_LUT_6_L50),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                            cov_mx(sub2ind(size(cov_mx),B(:,2),B(:,1)))= ...
                                cov_LUT_6_L50(sub2ind(size(cov_LUT_6_L50),B(:,7),B(:,8),B(:,9),B(:,10),B(:,11),B(:,12)));
                            
                        end
                end
            end
    end
elseif strcmp(method,'analytical')
%    Nifgs=size(baseline_arcs,1);
%    %C1=repmat(baseline_arcs(:,1),1,Nifgs);
%    %C2=repmat(baseline_arcs(:,2),1,Nifgs);
%    %C3=repmat(baseline_arcs(:,1)',Nifgs,1);
%    %C4=repmat(baseline_arcs(:,2)',Nifgs,1);
%    C1=baseline_arcs(:,1);
%    C2=baseline_arcs(:,2);
%    C3=baseline_arcs(:,1);
%    C4=baseline_arcs(:,2);
%   
%    cov_mx=(1/(2*L))*(coh_mx(C1,C3).*coh_mx(C2,C4) - coh_mx(C1,C4).*coh_mx(C2,C3))./(coh_mx(C1,C2).*coh_mx(C3,C4));
%    cov_mx=triu(cov_mx)+triu(cov_mx,1)';
   
Nifgsall=size(baseline_arcs,1);
Q1=repmat(baseline_arcs(:,1),1,Nifgsall);
Q2=repmat(baseline_arcs(:,2),1,Nifgsall);
Q3=Q1';
Q4=Q2';
C12=coh_mx(sub2ind(size(coh_mx),Q1,Q2));
C34=coh_mx(sub2ind(size(coh_mx),Q3,Q4));
C13=coh_mx(sub2ind(size(coh_mx),Q1,Q3));
C24=coh_mx(sub2ind(size(coh_mx),Q2,Q4));
C14=coh_mx(sub2ind(size(coh_mx),Q1,Q4));
C23=coh_mx(sub2ind(size(coh_mx),Q2,Q3));
cov_mx=1/2/L*((C13.*C24-C14.*C23)./(C12.*C34));
cov_mx=triu(cov_mx)+triu(cov_mx,1)';
   
else 
    error('the selected method is not available!!!')
end
cov_mx(abs(cov_mx)<1000*eps)=0; %avoid small negative values


