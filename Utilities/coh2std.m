function [std_phase,ph_pdf_out] = coh2std (coherence,ps_ds,L,pha_0);
% COH2STD -- Phase standard deviation for single look distributed scatterers.
%
%   COH2STD(COHERENCE) returns the standard deviation for the phase
%   based on the coherence. This should be valid for distributed 
%   scatterers. COHERENCE is a (complex) number with absolute value
%   between 0 and 1. If COHERENCE is a vector, a vector is returned.
%   Note that this is not the standard deviation of a normal distribution.
%
%   COH2STD(COHERENCE,L) returns std for multilookfactor L. 
%
%   Output is in degrees.
%
%   General equation for std is (expection E{.}):
%     var = int_{-pi}^{+pi} [phi-E{phi}].^2 .* pdf(phi) dphi
%   For single look this can be written in closed form as (coherence G):
%     var = 1/3*pi^2 - pi*arcsin(G) + arcsin^2(G) - 0.5*Li(G^2)
%   where Li(G)=sum_1^infty G^2k./k^2 (Euler's dilogarithm). 
%   For miltilooked phase it computes the numerical integral of the phase PDF
%
%   Example:
%     coh=0:0.01:1;
%     plot(coh,coh2std(coh,1),'k',coh,coh2std(coh,10),'b',...
%          coh,coh2std(coh,20),'r');
%     xlabel('coherence');ylabel('std');
%     title('phase standard deviation vs. coherence'); grid on;
%
%     (Hanssen 2001, eq. (4.2.28), p. 94)
%    
%   See also: PDF_PHASE, 
%
% -------------------------------------------------------------------------
% File............: coh2std
% Version & Date..: 1.1.1.1, 30-AUG-2011
% Authors.........: Sami Samiei Esfahany (extended for multilooked phase)
%                   using the original function "std_phase" from the InSAR 
%                   toolbox for single look phase written by Bert Kampes
%                   (18-Apr-2001)
%                   Delft Institute of Earth Observation and Space Systems
%                   Delft University of Technology
% -------------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2009 Delft University of Technology, The Netherlands



%%% Check input.
exitwithhelp=0;
switch nargin
  case 1
    ps_ds='ds';
    L=1;
    pha_0=0;
  case 2
    L=1;
   pha_0=0;
  case 3
    L=L;
    pha_0=0;
  case 4
    L=L;
    
  otherwise
    coherence=0.5; L=1;% (dummies)
    exitwithhelp=1;
end
if (~strcmp(ps_ds,'ps') & ~strcmp(ps_ds,'ds'))
error('pixel should either ps or ds');
end
if (~isreal(coherence))
    coherence=abs(coherence);
end;

if (min(coherence)<0 | max(coherence)>1) 
  warning('coherence not in [0,1]');
  exitwithhelp=1;
end;

if (exitwithhelp==1) helphelp; return; end;


%%% Compute std.

coh_size=size(coherence);
coherence=lying(coherence);

if  strcmp(ps_ds,'ps')
   var_phase=(1-coherence.^2)./(2*coherence.^2);
else

switch (L)
  case 1
    var_phase = (pi.^2)./3 - pi.*asin(coherence) + ...
                asin(coherence).^2 - Li2(coherence)./2;
    otherwise
        var_phase=NaN(size(coherence));
        for coh_ind=1:length(coherence)
        ph=[-pi:0.01:pi];
        %pha_0=0;
        g=coherence(coh_ind);
        %ph_pdf=pdf_phase(ph,g,L,pha_0);
        %alternative is to use the following function for high number of looks
        
%         ph_pdf=((gamma(L+0.5)) * (1-g^2)^L * g * cos(ph) ./ (2* sqrt(pi) * gamma(L) * (1-g^2.*(cos(ph)).^2).^(L+0.5) )) ...
%             +  (1-g^2)^L./(2.*pi) * hypergeometric2f1(L,1,0.5,(g^2.*cos(ph).^2),100); 
        %%
%         gamma_part = exp(gammaln(L+0.5) - gammaln(L));
%         cos_part = (1-g^2)^L * g * cos(ph) ./ (2* sqrt(pi) * (1-g^2.*(cos(ph)).^2).^(L+0.5) );
%         hyper_part1 = (1-g^2)^L./(2.*pi);
%         hyper_part2 = hypergeometric2f1(L,1,0.5,(g^2.*cos(ph).^2),100);

        
        ph_pdf= (exp(gammaln(L+0.5) - gammaln(L)) * (1-g^2)^L * g * cos(ph) ./ (2* sqrt(pi) * (1-g^2.*(cos(ph)).^2).^(L+0.5) )) ...
            +  (1-g^2)^L./(2.*pi) * hypergeometric2f1(L,1,0.5,(g^2.*cos(ph).^2),100); 

%         figure(1); plot(gamma_part.*cos_part); hold on; plot(hyper_part1.*hyper_part2); hold off
%         figure(2); plot(ph_pdf); hold on; plot(gamma_part.*cos_part + hyper_part1.*hyper_part2); hold off
        %%
        ph_pdf(ph_pdf < 0) = 0;
        ph_pdf(isnan(ph_pdf)) = 0;
        ph_pdf_out = ph_pdf;
        ph_pdf=(ph).^2.*(ph_pdf);
        
        %
        %ph_pdf=((gamma(L+0.5)) * (1-g^2)^L * g * cos(ph-pha_0) ./ (2* sqrt(pi) * gamma(L) * (1-g^2.*(cos(ph-pha_0)).^2).^(L+0.5) )) ...
        %    +  (1-g^2)^L./(2.*pi) * hypergeometric2f1(L,1,0.5,(g^2.*cos(ph-pha_0).^2),100); 
        %ph_pdf_M=trapz(ph,ph.*ph_pdf);
        %ph_pdf=(ph-ph_pdf_M).^2.*(ph_pdf);
        
        
       
        var_phase(coh_ind)=trapz(ph,ph_pdf);
        
     %   phPDF = @(ph) ph.^2.*(((gamma(L+0.5)) * (1-g^2)^L * g * cos(ph) ./ (2* sqrt(pi) * gamma(L) * (1-g^2.*(cos(ph)).^2).^(L+0.5) )) ...
     %       +  (1-g^2)^L./(2.*pi) * hypergeometric2f1(L,1,0.5,(g^2.*cos(ph).^2),100));
     %   var_phase(coh_ind)=quad(phPDF,-pi,pi);
        
        end
end;

end %%ps_ds
%%% what to return?
std_phase = rad2deg(sqrt(var_phase));
std_phase=reshape(std_phase,coh_size); 
if imag(std_phase)~=0
    test = 0;
end
std_phase(imag(std_phase)~=0)=0.5;



%%% EOF.

%---------------------------------------------------------------
% SUBFUNCTION LI2
%---------------------------------------------------------------
function Li2 = Li2 (coherence)
% Li2 = Li2 (coherence, kmax)
%   Calculate Euler's dilogarithm for use in calculation of
%   phase variance.
%   Note that actually the argument of Euler's dilogarithm should be
%   the squared absolute coherence. This function however has the
%   absolute coherence as parameter.
%   (Hanssen 2001, eq. (4.2.29), p. 95)
%   Li(G)=sum_1^infty G^2k./k^2 (Euler's dilogarithm). 
%
%   Implementation with loop cut of at 100, which is ok, 
%   even for coherence=0.99 (BK).
%   If input is a vector, slow implemented? vector returned.
%   coherence should be lying...
%

% Rens Swart * 17 april 2001
%if (nargin~=1) error('only 1 argument in subfunction Li2.'); end;
%if (coherence<0 | coherence>1) error('coherence not in [0,1]'); end;
%%% check input is vector/scalar

coherence = lying(coherence);
KMAX = 100;
k    = standing(1:KMAX);
G    = (ones(KMAX,1)*coherence).^(2.*k*ones(1,length(coherence)));
K    = k.^2*ones(1,length(coherence));
Li2  = sum(G./K,1);

%%% TEST
%for ii=1:length(coherence)
%  g    = coherence(ii);
%  Li2a = sum((g.^(2.*k)) ./ (k.^2))
%end
%%% EOF.

%---------------------------------------------------------------
% SUBFUNCTION hypergeometric2f1
%---------------------------------------------------------------
function z=hypergeometric2f1(a,b,c,x,n)
% HYPERGEOMETRIC2F1 Computes the hypergeometric function 
% using a series expansion:
%
%    f(a,b;c;x)=
%
%    1 + [ab/1!c]x + [a(a+1)b(b+1)/2!c(c+1)]x^2 +
%    [a(a+1)(a+2)b(b+1)(b+2)/3!c(c+1)(c+2)]x^3 + ...
%
% The series is expanded to n terms
%
% This function solves the Gaussian Hypergeometric Differential Equation:
%
%     x(1-x)y'' + {c-(a+b+1)x}y' - aby = 0
%
% The Hypergeometric function converges only for:
% |x| < 1
% c != 0, -1, -2, -3, ...
%
%
% Comments to:
% Diego Garcia   - d.garcia@ieee.org
% Chuck Mongiovi - mongiovi@fast.net
% June 14, 2002

if nargin ~= 5
    error('Usage: hypergeometric2f1(a,b,c,x,n) --> Wrong number of arguments')
end

if (n <= 0 | n ~= floor(n))
    error('Usage: hypergeometric2f1(a,b,c,x,n) --> n has to be a positive integer')
end

if (abs(x) > 1)
    error('Usage: hypergeometric2f1(a,b,c,x,n) --> |x| has to be less than 1')
end

if (c <= 0 & c == floor(c))
    error('Usage: hypergeometric2f1(a,b,c,x,n) --> c != 0, -1, -2, -3, ...')
end

z = 0;
m = 0;
while (m<n)
    if (m == 0)
        delta = 1;
    else
        delta = delta .* x .* (a + (m - 1)) .* (b + (m-1)) ./ m ./ (c + (m-1));
   end
   z = z + delta;
   m = m + 1;
end
%%% EOF.