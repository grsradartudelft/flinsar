from pyproj import Transformer

def wsg2rd(lat,lon):

    transformer = Transformer.from_crs("epsg:4326", "epsg:7415")
    out = transformer.transform(lat,lon)

    return out

def rd2wsg(x,y):

    transformer = Transformer.from_crs("epsg:7415","epsg:4326")
    out = transformer.transform(x,y)

    return out

if __name__ == "__main__":

    test = wsg2rd(52,4.5)

    print(test)
